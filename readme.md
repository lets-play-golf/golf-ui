# Lets play golf - golf-ui

Lets-play-golf is a web application where a community of players can challenge each other playing [golf](<https://en.wikipedia.org/wiki/Golf_(card_game)>), a popular card game.

[Join now and start playing!](https://www.lets-play-golf.com/)

This is the client UI project for [lets-play-golf](https://www.lets-play-golf.com/)
See [golf-ui](https://gitlab.com/lets-play-golf) for the NodeJs API server project.

## Product overview

This client application aims at delighting and helping users to:

- [Register and log in](https://lets-play-golf.com/login)
- Create and play a game - with a friend or with a bot
- Check their ranking on the [leaderboard](https://lets-play-golf.com/leaderboard)
- Send private messages to their challenger
- [Check and update their account details](https://lets-play-golf.com/account)

## Technical overview

This is a simple ReactJS application using npm ecosystem packages, including:

- Storybook: to craft beautiful components and spark delight
- Styled-components and React Bootstrap: For presentation components and styling.
- Reach-router: For routing requests to client page.
- Apollo-client: For communication with server.
- Subscriptions-transport-ws: For live subscription data transmission
- ReactJS: coordination, component rendering and update.

## Technical principles driving folder organisation

Folders breakdown:

- `app`: Functional/transversal components used across all pages.
- `components`: Business domain components, organised around domain objects: chats, sessions, users, games, players. Associated with a CRUD verb, it further organises some of the pages, e.g.:
  - `CreateUser` to register
  - `CreateSession` to log in
  - `UpdateUser`: to update account details, etc.
- `shared`: atomic presentation components, free of business logic and here to handle content organisation.
- `stories`: storybook stories to support the crafting of delightful ui components.
- `pages`: mostly static content pages.
- `docs`: readme files served in static pages.

## Software developer experience

- Lint and prettify: config to support code linting a prettifying on save with Vs Code.
- Pre-commit hook: configured to prettify and lint the code on commit

## Scripts

Getting started

### Install

```shell
yarn
```

### Start server

The development server will start up against port 4000

Note: This project points at a local instance of golf-api, serving data on port 4001

```shell
yarn start
```

### Lint

```shell
yarn fix-lint
```

### Build

To run the production build:

```shell
yarn build
```

### Storybook

Storybook allows for UI components to be developed in isolation by developing stories files.

To start up storybook:

```shell
npm run storybook
```

Check `**.stories.js` files to see the components' stories, props and documentation.

## Automation testing

Automation testing is here to ensure users can use the API and UI to fulfill their goal - create an account, log in, etc.

First start up the API and the UI

```shell
yarn start
```

Then run automation:

```shell
yarn e2e
```

The test suite is run with `jest` and `puppeteer` handle browser automation - open the home page, clicks on links, fill in forms, etc.
