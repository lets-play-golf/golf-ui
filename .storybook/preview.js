import React from 'react'
import GlobalStyle from 'shared/GlobalStyle'

import { Preview } from '@storybook/react'

import { ThemeProvider } from 'styled-components'
import themes from 'shared/theme'
import styled from 'styled-components'

/**
 * This is to simulate the screen with a dark background
 */
const Screen = styled.div`
  background-color: ${(props) => props.theme.background100};
  padding: 2rem;
`
const preview: Preview = {
  decorators: [
    (Story) => (
      <ThemeProvider theme={themes.darkBlue}>
        <Screen>
          <GlobalStyle />
          {/* 👇 Decorators in Storybook also accept a function. Replace <Story/> with Story() to enable it  */}

          <Story />
        </Screen>
      </ThemeProvider>
    ),
  ],
}

export default preview
