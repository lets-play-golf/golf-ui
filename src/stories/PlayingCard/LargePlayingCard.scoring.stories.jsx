import LargePlayingCard from 'components/play/PlayingCards/LargePlayingCard'
import { Template } from './Template'

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'DarkMode/play/PlayingCards/LargePlayingCard/scoring',
  component: LargePlayingCard,
  // // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  // argTypes: {

  // },
}

export const ScoringAceOfHearts = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
ScoringAceOfHearts.args = {
  id: 0,
  score: 1,
}

export const ScoringQueenOfDiamonds = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
ScoringQueenOfDiamonds.args = {
  id: 24,
  score: 10,
}

export const ScoringTenOfSpades = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
ScoringTenOfSpades.args = {
  id: 35,
  score: 10,
}

export const ScoringThreeOfClubs = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
ScoringThreeOfClubs.args = {
  id: 41,
  score: 3,
}

export const ScoringZero = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
ScoringZero.args = {
  id: 41,
  score: 0,
}

export const ScoringMinus10 = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
ScoringMinus10.args = {
  id: 41,
  score: -10,
}
