import React from 'react'

import { PlayingCardFlipper } from 'components/play/PlayingCards/PlayingCardFlipper'
import LargePlayingCard from 'components/play/PlayingCards/LargePlayingCard'

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'DarkMode/play/PlayingCardFlipper',
  component: PlayingCardFlipper,
  // // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  // argTypes: {

  // },
}

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template = (args) => <PlayingCardFlipper {...args} />

export const Flips = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Flips.args = {
  from: <LargePlayingCard />,
  to: <LargePlayingCard id={1} />,
}
