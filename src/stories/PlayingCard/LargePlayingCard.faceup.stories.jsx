import LargePlayingCard from 'components/play/PlayingCards/LargePlayingCard'
import { Template } from './Template'

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'DarkMode/play/PlayingCards/LargePlayingCard/faceup',
  component: LargePlayingCard,
  // // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  // argTypes: {

  // },
}

export const AceOfHearts = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
AceOfHearts.args = {
  id: 0,
}

export const QueenOfDiamonds = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
QueenOfDiamonds.args = {
  id: 24,
}

export const TenOfSpades = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
TenOfSpades.args = {
  id: 35,
}

export const ThreeOfClubs = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
ThreeOfClubs.args = {
  id: 41,
}
