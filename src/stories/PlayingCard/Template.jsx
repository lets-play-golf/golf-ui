import Card from 'shared/Card'
import LargePlayingCard from 'components/play/PlayingCards/LargePlayingCard'

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
export const Template = (args) => (
  <Card style={{ padding: '1rem' }}>
    <LargePlayingCard {...args} />
  </Card>
)
