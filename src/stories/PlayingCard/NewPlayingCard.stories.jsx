import styled from 'styled-components'
import Card from 'shared/Card'
import React from 'react'
import { getCard } from 'components/play/PlayingCards/cardHelpers'

export const Template = (args) => (
  <Card style={{ padding: '1rem' }}>
    <NewPlayingCard {...args} />
  </Card>
)

const NewPlayingCard = ({ id }) => {
  const { rank, suitCharacter, color } = getCard(id)
  const gradient = !rank ? 'neutralGradient' : color === 'light' ? 'redGradient' : 'blackGradient'
  return (
    <svg width="3.3rem" height="4.3rem">
      <defs>
        <linearGradient id="redGradient" x1="100%" y1="0%" x2="0%" y2="100%">
          <stop offset="0%" stop-color="hsl(12.8, 100%, 61.4%)" />
          <stop offset="50%" stop-color="hsl(347.1, 100%, 62.5%)" />
          <stop offset="100%" stop-color="hsl(306.2, 96.1%, 60.2%)" />
        </linearGradient>
        <linearGradient id="blackGradient" x1="0%" y1="100%" x2="100%" y2="0%">
          <stop offset="0%" stop-color="#6961ff" />
          <stop offset="80%" stop-color="#5ff6fe" />
        </linearGradient>linear-gradient(to bottom left, hsla(101, 100%, 68%, 1) 0%, hsla(182, 100%, 58%, 1) 100%)
        <linearGradient id="neutralGradient" x1="100%" y1="0%" x2="0%" y2="100%">
          <stop offset="0%" stop-color="hsla(101, 100%, 68%, 1)" />
          <stop offset="100%" stop-color="hsla(182, 100%, 58%, 1)" />
        </linearGradient>
        <clipPath id="text-1">
          <text
            font-family="Arial"
            x="78%"
            y="20%"
            dominant-baseline="middle"
            text-anchor="middle"
            font-size="1rem"
            dangerouslySetInnerHTML={{ __html: suitCharacter }}
          />
          <text
            font-family="'Calistoga',cursive"
            x="50%"
            y="53%"
            dominant-baseline="middle"
            text-anchor="middle"
            font-size="2.2rem"
          >
            {rank}
          </text>
          <text
            font-family="Arial"
            x="22%"
            y="80%"

            dominant-baseline="middle"
            text-anchor="middle"
            font-size="1rem"
            dangerouslySetInnerHTML={{ __html: suitCharacter }}
          />
        </clipPath>
      </defs>

      <rect
        x="2"
        y="2"
        width="3rem"
        height="4rem"
        stroke={`url(#${gradient})`}
        fill="#00000000"
        rx=".4rem"
        strokeWidth="3"
      />
      <g clip-path="url(#text-1)">
        <rect
          x="2"
          y="2"
          width="3rem"
          height="4rem"
          fill={`url(#${gradient})`}
          strokeWidth="3"
        />
      </g>
    </svg>
  )
}

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'DarkMode/play/PlayingCards/NewLargeCard',
  component: NewPlayingCard,
  // // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  // argTypes: {

  // },
}

export const AceOfHearts = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
AceOfHearts.args = {
  id: 0,
}

export const QueenOfDiamonds = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
QueenOfDiamonds.args = {
  id: 24,
}

export const TenOfSpades = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
TenOfSpades.args = {
  id: 35,
}

export const ThreeOfClubs = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
ThreeOfClubs.args = {
  id: 41,
}

export const FaceDown = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
FaceDown.args = {
}


const Wrapper = styled.button`
  height: 3rem;
  width: 4rem;
`
