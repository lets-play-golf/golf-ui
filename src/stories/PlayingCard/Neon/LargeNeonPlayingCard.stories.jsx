import NeonLargePlayingCard from 'components/play/PlayingCards/NeonLargePlayingCard'

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'DarkMode/play/PlayingCards/Neon/Large/faceup',
  component: NeonLargePlayingCard,
}

export const AceOfHearts = NeonLargePlayingCard.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
AceOfHearts.args = {
  id: 0,
}

export const QueenOfDiamonds = NeonLargePlayingCard.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
QueenOfDiamonds.args = {
  id: 24,
}

export const TenOfSpades = NeonLargePlayingCard.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
TenOfSpades.args = {
  id: 35,
}

export const ThreeOfClubs = NeonLargePlayingCard.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
ThreeOfClubs.args = {
  id: 41,
}
