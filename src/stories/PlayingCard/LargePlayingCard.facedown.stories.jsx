import LargePlayingCard from 'components/play/PlayingCards/LargePlayingCard'
import { Template } from './Template'

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'DarkMode/play/PlayingCards/LargePlayingCard/facedown',
  component: LargePlayingCard,
  // // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  // argTypes: {

  // },
}

export const FaceDown = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
FaceDown.args = {}

export const SelectedFaceDown = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
SelectedFaceDown.args = {
  isSelected: true,
}

export const Loading = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Loading.args = {
  isLoading: true,
}

export const ActiveCard = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
ActiveCard.args = {
  label: `Click here to discard the card you just picked up`,
  onClick: () => alert('onClick event'),
}
