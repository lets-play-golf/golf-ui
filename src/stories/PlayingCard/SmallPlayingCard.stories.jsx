import SmallPlayingCard from 'components/play/PlayingCards/SmallPlayingCard.jsx'
import Card from 'shared/Card'

const Template = (args) => (
  <Card style={{ padding: '1rem' }}>
    <SmallPlayingCard {...args} />
  </Card>
)

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'DarkMode/play/PlayingCards/SmallPlayingCard',
  component: SmallPlayingCard,
  // // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  // argTypes: {

  // },
}

export const FaceDown = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
FaceDown.args = {}

export const Red = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Red.args = {
  id: 23,
}

export const Black = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Black.args = {
  id: 49,
}

export const Scoring10 = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Scoring10.args = {
  id: 23,
  score: 10,
}

export const Scoring0 = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Scoring0.args = {
  id: 23,
  score: 0,
}

export const ScoringMinus10 = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
ScoringMinus10.args = {
  id: 23,
  score: -10,
}
