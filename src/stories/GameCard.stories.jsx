import React from 'react'

import GameCard from 'components/games/GameCard'

const game = {
  id: '5fb2d56f550fde55a8ea20ed',
  numberOfRounds: 5,
  players: [
    {
      user: {
        id: '5fb281e61026422da18402d7',
        username: 'ali',
        __typename: 'User',
      },
      score: [14, 14, 16, 17, 30],
      isOnline: false,
      __typename: 'GamePlayer',
    },
    {
      user: {
        id: '5fbeb4ef3c33f5440527b6c8',
        username: 'bruno',
        __typename: 'User',
      },
      score: [8, 6, 23, 23, 3],
      isOnline: false,
      __typename: 'GamePlayer',
    },
  ],
  status: 'finished',
  createdAt: new Date('2020-11-16T19:39:27.468Z'),
  createdBy: {
    id: '5fb281e61026422da18402d7',
    firstname: 'Ali',
    username: 'ali',
    __typename: 'User',
  },
  __typename: 'Game',
}

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'DarkMode/games/GameCard',
  component: GameCard,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    hide: { control: 'color' },
    isPrimary: { control: 'boolean' },
  },
}

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template = (args) => <GameCard {...args} />

export const WithActions = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
WithActions.args = {
  game: game,
  hideActions: false,
}

export const WithoutActions = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
WithoutActions.args = {
  game: game,
  hideActions: true,
}
