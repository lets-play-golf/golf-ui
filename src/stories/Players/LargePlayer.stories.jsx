import React from 'react'
import Player from 'components/play/Players/LargePlayer'
import LargePlayingCard from 'components/play/PlayingCards/LargePlayingCard'

const player = {
  id: '5fb281e61026422da18402d7',
  isGameWinner: 0,
  isGameLoser: 0,
  visibleCards: [45, 24, 1, undefined],
  pickedUpCard: null,
  username: 'fatoumata',
  totalScore: 0,
  score: [],
  actions: [],
}

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'DarkMode/play/Players/LargePlayer',
  component: Player,
  // // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  // argTypes: {

  // },
}

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template = (args) => (
  <Player {...args}>
    {args.player.visibleCards.map((id) => (
      <LargePlayingCard id={id} key={id} />
    ))}
  </Player>
)

export const LargePlayer = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
LargePlayer.args = {
  player,
}
