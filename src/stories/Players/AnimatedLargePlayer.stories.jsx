import { AnimatedLargePlayer } from 'shared/AnimatedLargePlayer'

const player = {
  id: '5fb281e61026422da18402d7',
  isGameWinner: 0,
  isGameLoser: 0,
  pickedUpCard: null,
  username: 'you',
  totalScore: 0,
  score: [],
  actions: [],
}

export const AnimatedLargePlayerComp = AnimatedLargePlayer
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'DarkMode/play/Players/AnimatedLargePlayer',
  component: AnimatedLargePlayer,
}

// More on args: https://storybook.js.org/docs/react/writing-stories/args
AnimatedLargePlayer.args = {
  player,
}
