import React from 'react'
import Player from 'components/play/Players/SmallPlayer'
import SmallPlayingCard from 'components/play/PlayingCards/SmallPlayingCard'

const player = {
  id: '5fb281e61026422da18402d7',
  isGameWinner: 0,
  isGameLoser: 0,
  visibleCards: [45, 24, 1, undefined],
  pickedUpCard: null,
  username: 'fatoumata',
  totalScore: 36,
  score: [],
  actions: [],
}

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'DarkMode/play/Players/SmallPlayer',
  component: Player,
  // // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  // argTypes: {

  // },
}

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template = (args) => (
  <Player {...args}>
    {args.player.visibleCards.map((id) => (
      <SmallPlayingCard id={id} key={id} />
    ))}
  </Player>
)

export const SmallPlayer = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
SmallPlayer.args = {
  player,
}

export const TODOCurrentTurnPlayer = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
TODOCurrentTurnPlayer.args = {
  player,
}
