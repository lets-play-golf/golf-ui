import React from 'react'

import SmallDecksComponent from 'components/play/Decks/SmallDecks'

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'DarkMode/play/SmallDecks',
  component: SmallDecksComponent,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {},
}

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template = (args) => <SmallDecksComponent {...args} />

export const SmallDecks = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
SmallDecks.args = {
  discardCard: 12,
}
