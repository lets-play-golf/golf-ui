import React from 'react'
import { DecksWrapper } from 'components/play/Decks/LargeDecks'
import LargePlayingCard from 'components/play/PlayingCards/LargePlayingCard'

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'DarkMode/play/LargeDecks',
  component: DecksWrapper,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {},
}

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template = (args) => <DecksWrapper {...args} />

export const DealerDecksStory = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
DealerDecksStory.args = {
  children: [<LargePlayingCard />, <LargePlayingCard id={22} />],
}

export const TODOYouPickedUpACard = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
TODOYouPickedUpACard.args = {
  children: [<LargePlayingCard />, <LargePlayingCard id={22} />],
}
