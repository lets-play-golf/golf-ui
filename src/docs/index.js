import React from 'react'
import MainLayout from 'app/MainLayout'
import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'
import { useMdFile, sitemap } from './pages'
import rehypeHighlight from 'rehype-highlight'
import 'highlight.js/styles/gradient-dark.css'
import styled from 'styled-components'
import Breadcrumbs from 'app/Breadcrumbs'

const DocsHome = (props) => {
  const { md, breadcrumbs } = useMdFile(props.location.pathname)

  const siteMap = sitemap()
  return (
    <MainLayout>
      <DocsStyleWrapper>
        <div className="float-right" style={{ fontSize: '.9rem' }}>
          you are here:&nbsp;&nbsp;&nbsp;&nbsp;
          <Breadcrumbs.Crumbs crumbs={breadcrumbs} isForward={false} />
        </div>
        <ReactMarkdown
          children={md}
          remarkPlugins={[remarkGfm]}
          rehypePlugins={[rehypeHighlight]}
          components={{
            img: Image,
            code: Code,
          }}
        />
        <SiteMap siteMap={siteMap} />
      </DocsStyleWrapper>
    </MainLayout>
  )
}

const SiteMap = ({ siteMap }) => (
  <div style={{ fontSize: '.9rem' }}>
    <p style={{ marginTop: '5rem' }}>Related topics:</p>
    {siteMap.map((crumbs) => (
      <div>
        <Breadcrumbs.Crumbs crumbs={crumbs} />
      </div>
    ))}
  </div>
)

const DocsStyleWrapper = styled.div`
  h1 {
    padding-top: 1.5rem;
  }

  h2 {
    font-size: 1.8rem;
    padding-top: 1rem;
  }
`

/**
 * Custom handler applying max-width styling for different images based on alt-text
 * 'iphone-screenshot' =>  maxWidth: 375px
 */
const Image = (props) => {
  const alt = props.alt || ''
  const isIphoneScreenshot = alt.includes('iphone-screenshot')
  return (
    <img
      {...props}
      style={{ width: '100%', maxWidth: isIphoneScreenshot ? '375px' : '100%' }}
    />
  )
}

const Code = styled.code`
  border-radius: 0.4rem;
  border: 1px solid #ffffff;
  padding: 0.8rem;
`

export default DocsHome
