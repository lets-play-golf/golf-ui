import React from 'react'
import publicPages from './public-home'
import privateFiles from './private-home'
const allFilesMap = {
  // public pages
  ...publicPages,

  // private pages
  '/docs/custom-md': privateFiles.customMd,
}

const RENAME_BREADCRUMB = {
  docs: 'docs home',
}

const useBreadcrumb = (requestedFile) => {
  const { breadcrumbs } = requestedFile
    .split('/')
    .slice(1)
    .reduce(
      (acc, path) => ({
        breadcrumbs: acc.breadcrumbs.concat({
          title: `${RENAME_BREADCRUMB[path] || path}`,
          link: `${acc.cumulPath}/${path}`,
        }),
        cumulPath: `${acc.cumulPath}/${path}`,
      }),
      { breadcrumbs: [], cumulPath: '' }
    )
  return breadcrumbs
}

const useMdFile = (pathname) => {
  const [md, setMd] = React.useState('')

  const breadcrumbs = useBreadcrumb(pathname)
  const file = allFilesMap[pathname] || publicPages.default

  fetch(file)
    .then((response) => response.text())
    .then((text) => setMd(text))

  return { md, breadcrumbs }
}

const sitemap = () => Object.keys(publicPages).map(useBreadcrumb)

export { useMdFile, sitemap }
