# Custom MD user guide

## Images

Use this typical notation to insert an image that is loaded in the assets folder.

```md
![alt-text](/assets/your-image.png)
```

### Iphone screenshot resizing

Use this alt text notation to resize image so it has a max-width of 375px:

```md
![iphone-screenshot: play game](/assets/its-your-turn.png)
```

a js example

```javascript
const hello = 10
console.log({ hello })
```
