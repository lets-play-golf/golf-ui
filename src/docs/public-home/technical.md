# Technical

Lets-play-golf is a full stack JS project developed entirely by
[Benoît Perrier](mailto:bpjperrier@gmail.com) for friends to
enjoy.

It was intended to demonstrate technical skills working with:

- Node.Js with Express server; and
- GraphQL gateway middleware on the back end.
- React.Js single page app in the front end.
- Unit testing with global coverage using Jest
- End-to-End testing using Puppeteer

## Code

Check out the code on [gitlab](http://gitlab.com/lets-play-golf)

Detailed technical information is available on each project's README file:

- [gol-api](http://gitlab.com/lets-play-golf/golf-api): for the Node.Js Express server with GraphQL gateway.
- [gol-ui](http://gitlab.com/lets-play-golf/golf-ui): for the React.Js single page app.

## UI

Example of a resized iphone screenshot:

![iphone-screenshot: play game](/assets/its-your-turn.png)
