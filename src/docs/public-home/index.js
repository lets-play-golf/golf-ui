import docsHome from './docs-home.md'
import technical from './technical.md'
import about from './about.md'
import productOverview from './product-overview.md'
import pageNotFound from './page-not-found.md'
import contact from './contact.md'

const publicPages = {
  '/docs': docsHome,
  '/docs/technical': technical,
  '/docs/about': about,
  '/docs/contact': contact,
  '/docs/product-overview': productOverview,
  default: pageNotFound,
}

export default publicPages
