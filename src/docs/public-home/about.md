# About

```javascript
console.log('Hello, world!')
```

Lets-play-golf is a web application project created by Benoît Perrier.\
It is an online version of Pub Golf, a popular cards game.

## About me

My name is Benoît, I am full stack developer living and working in London.
You can consult my [CV on google drive](https://docs.google.com/document/d/1uigQUf7oXPT_ItRa8KqGbetpkW4JcAG48BztB3JvdBQ).

## About this app

I started this project during the pandemic to keep my friends entertained and develop coding skills.
Over time, it turned out to be more focused on product, feature development and design.
\

To explore further:

- [Product overview](/docs/product-overview)
- [Technical overview](/docs/technical) for More information and code repositories.

![iphone-screenshot: play game](/assets/its-your-turn.png)
