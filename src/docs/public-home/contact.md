# Contact

[Get in touch](https://docs.google.com/forms/d/e/1FAIpQLScoxoV4zShGz9vd06js-WKcl8mJPuHl0ItBbOcYe7hwv_eaQw/viewform")
if you are seeing errors or have any other query related to the website, like:

- Something is not working as expected
- Something confuses you
- You'd like to request a feature

It only takes a minute to fill in the [contact form](https://docs.google.com/forms/d/e/1FAIpQLScoxoV4zShGz9vd06js-WKcl8mJPuHl0ItBbOcYe7hwv_eaQw/viewform).

# Email

For anything else, please send me an [email](mailto:bpjperrier@gmail.com)
