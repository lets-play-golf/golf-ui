# Product overview

Lets-play-golf is a web application where a community of players can challenge each other playing [golf](<https://en.wikipedia.org/wiki/Golf_(card_game)>), a popular card game.

## Features

In a nutshell, users can:

- Play the game with a bot
- Play the game with a friend & live chat.
- Check rating and ranking on the leaderboard

## Key engagement metrics

The golf cards game is an exercise of memory, maths, strategy and fast-paced decision making.

- To be successful at this game, users need to think strategically and make decisions based on their memory of played cards and simple mathematical calculations.
- The main driver for engagement is to exercise cognitive and strategic skills: applying a strategy and seeing the result of it in order to improve.

## User profile 1: The gamer

- The gamer engages in his or her spare time and play golf with the bot as a way of killing time.
- They might play on their way to work while waiting for the bus or in front of the TV.
- Because their time is limited, they are looking to complete a game within a few minutes and see the result of their progress at the end of it.

## User profile 2: The socialiser

- The socialiser plays this game with his friends in real life.
- The socialiser practices with the bot in their spare time on the one hand, and secretely wants to use their improved skills to win over their friends.
- Every now and then, to settle a competition or to win a bet, they challenge their friend on the app and play live with an opponent.
- They don't mind playing the original longer game of 18 rounds, because victory is more meaningful.
