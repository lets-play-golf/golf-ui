import gql from 'graphql-tag'
import { useLoggedInQuery, useLoggedInMutation } from 'app/useGraphql'

const TYPE_GAME = `{
  id
  numberOfRounds
  players {
    user {
      id
      username
    }
    score
    isOnline
  }
  status
  createdAt
  createdBy {
    id
    firstname
    username
  }
}`

const GAME_QUERY = gql`
  query game ($id: ID!) {
    game (id: $id) ${TYPE_GAME}
  }
`
export const useGameQuery = (variables) =>
  useLoggedInQuery(GAME_QUERY, { variables, fetchPolicy: 'network-only' })

const PLAYERS_GAMES_QUERY = gql`
  query games {
    gamesUntyped {
      games
      otherPlayers {
        id
        username
        activeGameSubscriptions
      }
    }
  }
`
export const useGamesQuery = (variables) =>
  useLoggedInQuery(PLAYERS_GAMES_QUERY, {
    variables,
    fetchPolicy: 'network-only',
  })

const CREATE_GAME = gql`
  mutation createGame($numberOfRounds: Int!) {
    createdGame: createGame (
      numberOfRounds: $numberOfRounds
    ) ${TYPE_GAME}
  }
`
export const useCreateGame = (options) =>
  useLoggedInMutation(CREATE_GAME, options)

const JOIN_GAME_MUTATION = gql`
  mutation joinGame($id: ID!) {
    joinGame(id: $id) {
      id
    }
  }
`
export const useJoinGame = (variables) =>
  useLoggedInMutation(JOIN_GAME_MUTATION, { variables })
