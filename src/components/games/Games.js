import React from 'react'
import styled from 'styled-components'
import { useGamesQuery } from './queries'
import { PrimaryButton } from 'shared/Buttons'
import { Link } from '@reach/router'
import GameCard from './GameCard'
import Row from 'react-bootstrap/Row'
import { useSessionContext } from 'components/sessions/sessionContext'
import Stack from 'shared/content-control/Stack'
import { GolfFlagIcon } from 'app/icons'

const Games = () => {
  const { session } = useSessionContext()
  const { data } = useGamesQuery({ player: session.user.id })
  if (!data?.gamesUntyped) return <></>

  if (!data?.gamesUntyped?.games?.length) return <NoGames />

  const {
    gamesUntyped: { games, otherPlayers },
  } = data

  const resolveOtherPlayers = (game) => ({
    ...game,
    players: game.players.map((player) => {
      const otherPlayer = otherPlayers.find(
        (otherPlayer) => otherPlayer.id === player.user
      )
      return {
        ...player,
        user: otherPlayer,
        isOnline: otherPlayer.activeGameSubscriptions.includes(game.id),
      }
    }),
  })
  const {
    created = [],
    started = [],
    finished = [],
  } = games.map(resolveOtherPlayers).reduce(
    (acc, itm) => ({
      ...acc,
      [itm.status]: acc[itm.status] ? [...acc[itm.status], itm] : [itm],
    }),
    {}
  )

  console.log({})
  // Maximum amount of games open is 5
  const canOpenNewGame = [...created, ...started]?.length < 6

  return (
    <>
      <h1>Your Games</h1>

      {!!created.length && <GamesList title="Open">{created}</GamesList>}

      {!!started.length && <GamesList title="Started">{started}</GamesList>}

      {canOpenNewGame && <CreateGameButton className="mt-3 mb-3" />}

      {!!finished.length && <GamesList title="Done">{finished}</GamesList>}
    </>
  )
}

const GamesList = ({ title, children }) => {
  return (
    <>
      <h3>{title}</h3>
      <Row className="m-0">
        {children.map((game) => (
          <GameCard key={game.id} game={game} />
        ))}
      </Row>
    </>
  )
}

const IconDottedWrapper = styled.div`
  border: .2rem dashed white;
  padding 1rem;
  border-radius: 1rem;
  width: 9rem;
  height: 9rem;
  margin: 1rem 0;
`

const NoGames = () => (
  <Stack space={'3'}>
    <h1>Your Games</h1>
    <IconDottedWrapper>
      <GolfFlagIcon />
    </IconDottedWrapper>
    <p>Your games will appear here - Start your first game now!</p>
    <CreateGameButton className="mt-3 mb-3" title={'New Game'} />
  </Stack>
)

const CreateGameButton = (props) => (
  <Link to="/create" data-testid="create-game-link">
    <PrimaryButton {...props}>New Game</PrimaryButton>
  </Link>
)
export default Games
