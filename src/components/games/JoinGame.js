import React from 'react'
import styled from 'styled-components'
import { useSessionContext } from 'components/sessions/sessionContext'
import { useGameQuery, useJoinGame } from './queries'
import { Redirect, navigate } from '@reach/router'
import MainLayout from 'app/MainLayout'
import GameCard from './GameCard'
import { PrimaryButton } from 'shared/Buttons'

const JoinGame = ({ id }) => {
  const [joinGame] = useJoinGame({ id })
  const { session } = useSessionContext()
  const { data } = useGameQuery({ id })
  const [showErrorMessage, setShowErrorMessage] = React.useState(false)

  if (!data?.game) return <></>

  const { status, players, createdBy } = data.game
  const hasUserAlreadyJoined = players.some(
    ({ user }) => user.id === session.user.id
  )
  if (hasUserAlreadyJoined) return <Redirect to={`/play/${id}`} noThrow />
  if (status !== 'created') return <GameAlreadyStarted />

  const onClick = async () => {
    try {
      const { data } = await joinGame()
      if (data) navigate(`/play/${id}`)
    } catch (e) {
      setShowErrorMessage(true)
    }
  }

  return (
    <MainLayout>
      <h1>Join {createdBy.firstname}</h1>
      <GameCard game={data.game} hideActions />
      <p>
        {`Friendly note: Please play all ${data.game.numberOfRounds} rounds for your credit
        and other players' to be released.`}
      </p>
      {showErrorMessage && (
        <ErrorMessage>
          This game is now closed. You may join another game or create a new
          one.
        </ErrorMessage>
      )}
      <PrimaryButton onClick={onClick}>
        Join game for {data.game.numberOfRounds} rounds
      </PrimaryButton>
    </MainLayout>
  )
}

const GameAlreadyStarted = () => (
  <MainLayout>
    <h1>This game already started</h1>
    <p>You may create a new game or join another existing one.</p>
  </MainLayout>
)

const ErrorMessage = styled.div`
  color: ${(props) => props.theme.accent2};
`

export default JoinGame
