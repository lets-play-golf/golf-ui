import React from 'react'
import styled from 'styled-components'
import MainLayout from 'app/MainLayout'
import Form from 'react-bootstrap/Form'
import { useForm } from 'react-hook-form'
import { PrimaryButton } from 'shared/Buttons'
import { useCreateGame } from './queries'
import { navigate } from '@reach/router'

const CreateGame = () => {
  const [createGame] = useCreateGame()
  const {
    formState: { isSubmitting },
    handleSubmit,
    register,
  } = useForm({ submitFocusError: true })

  const [showErrorMessage, setShowErrorMessage] = React.useState(false)
  const onSubmit = handleSubmit(async ({ numberOfRounds }) => {
    try {
      const { data } = await createGame({
        variables: { numberOfRounds: +numberOfRounds },
      })
      if (data) navigate(`/play/${data.createdGame.id}`)
    } catch (e) {
      // Error occurred
      setShowErrorMessage(true)
    }
  })

  return (
    <MainLayout>
      <h1>New Game</h1>

      <Form onSubmit={onSubmit}>
        <Form.Group controlId="numberOfRounds">
          <Form.Label>
            How many rounds would you like to play? (Count 2 minutes per round)
          </Form.Label>
          <Form.Control name="numberOfRounds" as="select" ref={register()}>
            <option value="5">5</option>
            <option value="9">9</option>
            <option value="14">14</option>
            <option value="18">18</option>
          </Form.Control>
        </Form.Group>

        {showErrorMessage && (
          <ErrorMessage>
            Whoops! There was an error creating your game. Please try again in a
            few moments.
          </ErrorMessage>
        )}
        <PrimaryButton
          disabled={isSubmitting}
          type="submit"
          title="Create new Game"
        >
          Create new game
        </PrimaryButton>
      </Form>
    </MainLayout>
  )
}

const ErrorMessage = styled.div`
  color: ${(props) => props.theme.accent2};
`

export default CreateGame
