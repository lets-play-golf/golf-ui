import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Card from 'shared/Card'
import { PrimaryButton } from 'shared/Buttons'
import { timeAgo } from 'app/timeAgo'
import { Link } from '@reach/router'
import PlayersList from './PlayersList'
import { GolfHole } from 'app/icons'

const GameCardWrapper = styled(Card)`
  margin: 0 1rem 1rem 0;
  width: 18rem;
`

const GameCard = ({ game, hideActions, ...props }) => {
  const { id, status, numberOfRounds, players, createdAt } = game
  const currentRound = players[0].score.length + 1
  const cardTitle =
    status === 'started'
      ? `${currentRound} / ${numberOfRounds} rounds`
      : `${numberOfRounds} rounds`

  return (
    <GameCardWrapper id={`game-${id}`} {...props}>
      <Card.Body>
        <Card.Title>
          <GolfHole /> {cardTitle}
        </Card.Title>
        <Card.Text>
          <span>
            {status}{' '}
            {timeAgo.format(Date.now() - (new Date() - new Date(createdAt)))}
          </span>
          <br />
          {players && <PlayersList players={players} />}
        </Card.Text>
        {!hideActions && (
          <Link to={`/play/${id}`}>
            <PrimaryButton>
              {['created', 'finished'].includes(status) ? 'Open' : 'Play'}
            </PrimaryButton>
          </Link>
        )}
      </Card.Body>
    </GameCardWrapper>
  )
}

export default GameCard

const PlayerShape = PropTypes.shape({
  user: PropTypes.shape({
    id: PropTypes.string,
    username: PropTypes.string,
    __typename: PropTypes.string,
  }),
  score: PropTypes.arrayOf(PropTypes.number),
  isOnline: PropTypes.bool,
  __typename: PropTypes.string,
})

GameCard.propTypes = {
  game: PropTypes.shape({
    id: PropTypes.string,
    numberOfRounds: PropTypes.number,
    players: PropTypes.arrayOf(PlayerShape),
    status: PropTypes.oneOf(['finished', 'created', 'started']),
    createdAt: PropTypes.string,
    createdBy: PlayerShape,
  }),
  hideActions: PropTypes.bool,
}

GameCard.defaultProps = {
  game: {},
  hideActions: false,
}
