import React, { useState, useRef } from 'react'
import Overlay from 'react-bootstrap/Overlay'
import Tooltip from 'react-bootstrap/Tooltip'
import { GolfPlayer, Lightning } from 'app/icons'

const PlayersList = ({ players }) => {
  return (
    <>
      {players.map((player) => (
        <Player player={player} key={player.user.id} />
      ))}
    </>
  )
}

const Player = ({ player }) => {
  const target = useRef(null)
  const [show, setShow] = useState(false)
  return (
    <>
      <span>
        <GolfPlayer />
        {player.user.username}
        {player.isOnline && (
          <>
            <span ref={target} onClick={() => setShow(!show)}>
              {' '}
              <Lightning />
            </span>
            <Overlay target={target.current} show={show} placement="right">
              {(props) => (
                <Tooltip id="overlay-example" {...props}>
                  {player.user.username} is online
                </Tooltip>
              )}
            </Overlay>
          </>
        )}
      </span>
      <br />
    </>
  )
}

export default PlayersList
