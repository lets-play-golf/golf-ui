import { useMutation, useQuery } from '@apollo/react-hooks'
import gql from 'graphql-tag'

const CREATE_USER = gql`
  mutation createUser(
    $email: String!
    $username: String!
    $firstname: String!
    $password: String!
    $emailWhenInvitationAccepted: Boolean!
    $emailWhenMessageReceivedOffline: Boolean!
  ) {
    createUser(
      email: $email
      username: $username
      firstname: $firstname
      password: $password
      emailWhenInvitationAccepted: $emailWhenInvitationAccepted
      emailWhenMessageReceivedOffline: $emailWhenMessageReceivedOffline
    ) {
      ... on Session {
        user {
          id
          email
          username
          firstname
        }
      }
      ... on UserInputError {
        error {
          key
          message
          fields
          data
        }
      }
    }
  }
`
export const useCreateUser = () => useMutation(CREATE_USER)

const UPDATE_USER = gql`
  mutation updateUser($update: UserUpdate!) {
    updateUser(update: $update) {
      ... on User {
        id
        email
        username
        firstname
        preferences {
          emailWhenInvitationAccepted
          emailWhenMessageReceivedOffline
        }
      }
      ... on UserInputError {
        error {
          key
          fields
          message
        }
      }
    }
  }
`
export const useUpdateUser = () => useMutation(UPDATE_USER)

const USER = gql`
  query me {
    me {
      username
      email
      firstname
      preferences {
        emailWhenInvitationAccepted
        emailWhenMessageReceivedOffline
      }
    }
  }
`
export const useUserQuery = (variables) =>
  useQuery(USER, { variables, fetchPolicy: 'network-only' })

const REQUEST_PASSWORD_RESET = gql`
  mutation requestPasswordReset($email: String!) {
    requestPasswordReset(email: $email)
  }
`
export const useRequestPasswordReset = () => useMutation(REQUEST_PASSWORD_RESET)

const RESET_PASSWORD = gql`
  mutation resetPassword($password: String!, $resetHash: String!) {
    resetPassword(password: $password, resetHash: $resetHash)
  }
`
export const useResetPassword = () => useMutation(RESET_PASSWORD)

const DELETE_USER = gql`
  mutation {
    deleteUser
  }
`
export const useDeleteUser = () => useMutation(DELETE_USER)
