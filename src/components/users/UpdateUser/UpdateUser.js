import React from 'react'
import { useSessionContext } from '../../sessions/sessionContext'
import MainLayout from '../../../app/MainLayout'
import { useUserQuery } from '../queries'
import { Redirect, Link } from '@reach/router'
import ChangePreferencesForm from '../forms/ChangePreferencesForm'
import ChangeTextForm from '../forms/ChangeTextForm'
import ChangePasswordForm from '../forms/ChangePasswordForm'
import { PrimaryButton } from 'shared/Buttons'

const UpdateUser = () => {
  const { session, handleExpiredSession } = useSessionContext()
  const { data, error } = useUserQuery()
  if (!session) return <Redirect to="/login" noThrow />
  if (error) handleExpiredSession(error)
  if (!data) return <></>

  const { username, email, firstname, preferences } = data.me

  return (
    <MainLayout>
      <h1>Update Details</h1>
      <ChangeTextForm {...{ firstname }} />
      <ChangeTextForm {...{ username }} />
      <ChangeTextForm {...{ email }} />
      <p>password: ********</p>
      <ChangePasswordForm />
      <ChangePreferencesForm {...{ preferences }} />
      <br />
      <Link to="/delete-account">
        <PrimaryButton variant="danger">Delete your account</PrimaryButton>
      </Link>
    </MainLayout>
  )
}
export default UpdateUser
