import { usernameRegex, emailRegex, passwordRegex } from 'app/constants'
import { getError } from 'components/errors'

const formElements = {
  firstname: {
    label: 'first name',
    type: 'text',
    register: {
      validate: (value) =>
        !value ? 'required' : value.length < 2 ? 'Name is too short' : true,
    },
  },
  username: {
    label: 'username',
    type: 'text',
    register: {
      required: true,
      pattern: {
        value: usernameRegex,
        message: getError({ key: 'INVALID_USERNAME' }).message,
      },
    },
  },
  email: {
    label: 'email',
    type: 'email',
    register: {
      required: true,
      pattern: {
        value: emailRegex,
        message: getError({ key: 'INVALID_EMAIL' }).message,
      },
    },
  },
  password: {
    label: 'password',
    type: 'password',
    register: {
      required: true,
      pattern: {
        value: passwordRegex,
        message: getError({ key: 'WEAK_PASSWORD' }).message,
      },
    },
  },
  passwordConfirm: {
    label: 'password confirm',
    type: 'password',
    register: {
      validate: (value) =>
        !value ? 'required' : value.length < 6 ? 'Password is too short' : true,
    },
  },
}

export default formElements
