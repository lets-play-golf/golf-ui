import React from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { useUpdateUser } from '../../queries'
import { PrimaryButton } from 'shared/Buttons'
import { useForm } from 'react-hook-form'
import Form from 'react-bootstrap/Form'
import { resolveBackendError } from '../../../errors'

const ChangePreferencesForm = ({
  preferences: { emailWhenInvitationAccepted, emailWhenMessageReceivedOffline },
}) => {
  const [updateUser] = useUpdateUser()
  const {
    formState: { isSubmitting },
    handleSubmit,
    register,
    setError,
  } = useForm({
    defaultValues: {
      emailWhenInvitationAccepted,
      emailWhenMessageReceivedOffline,
    },
  })

  const onSubmit = handleSubmit(
    async ({
      emailWhenInvitationAccepted,
      emailWhenMessageReceivedOffline,
    }) => {
      const update = {
        emailWhenInvitationAccepted,
        emailWhenMessageReceivedOffline,
      }
      try {
        const {
          data: {
            updateUser: { error },
          },
        } = await updateUser({ variables: { update } })
        if (!error) return window.location.reload()
        const { field, key, message } = resolveBackendError(error)
        setError(field, key, message)
      } catch (e) {
        console.error(`CreateUser submit error: ${e}`)
        console.log('error', e)
      }
    }
  )
  return (
    <>
      <p>Email me when:</p>
      <Form onSubmit={onSubmit}>
        <Form.Group controlId="validation">
          <Row>
            <Col>
              <Form.Check
                name="emailWhenInvitationAccepted"
                ref={register()}
                type="checkbox"
                label="Someone joins my game"
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Check
                name="emailWhenMessageReceivedOffline"
                ref={register()}
                type="checkbox"
                label="Someone messages me and I'm offline"
              />
            </Col>
          </Row>
          <Row className="mt-3">
            <Col>
              <PrimaryButton disabled={isSubmitting} type="submit">
                Change
              </PrimaryButton>
            </Col>
          </Row>
        </Form.Group>
      </Form>
    </>
  )
}
export default ChangePreferencesForm
