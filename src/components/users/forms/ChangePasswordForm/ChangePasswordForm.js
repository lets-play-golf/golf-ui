import React from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { useUpdateUser } from '../../queries'
import { PrimaryButton } from 'shared/Buttons'
import { useForm } from 'react-hook-form'
import Form from 'react-bootstrap/Form'
import { resolveBackendError, getError } from '../../../errors'
import { passwordRegex } from 'app/constants'

const ChangePasswordForm = () => {
  const [updateUser] = useUpdateUser()
  const {
    formState: { isSubmitting },
    handleSubmit,
    register,
    errors,
    setError,
  } = useForm({ defaultValues: {} })

  const onSubmit = handleSubmit(async ({ password, passwordConfirm }) => {
    if (password !== passwordConfirm)
      return setError(
        'passwordConfirm',
        'PASSWORDS_DONT_MATCH',
        "Passwords don't match"
      )
    const update = { password }
    try {
      const {
        data: {
          updateUser: { error },
        },
      } = await updateUser({ variables: { update } })
      if (!error) return window.location.reload()
      const { field, key, message } = resolveBackendError(error)
      setError(field, key, message)
    } catch (e) {
      console.error(`CreateUser submit error: ${e}`)
      console.log('error', e)
    }
  })

  return (
    <>
      <Form onSubmit={onSubmit}>
        <Form.Group controlId="validation">
          <Row>
            <Col>
              <Form.Control
                name="password"
                type="password"
                placeholder="New password"
                disabled={isSubmitting}
                isInvalid={!!errors.password}
                ref={register({
                  required: true,
                  pattern: {
                    value: passwordRegex,
                    message: getError({ key: 'WEAK_PASSWORD' }).message,
                  },
                })}
              />

              <Form.Control
                name="passwordConfirm"
                type="password"
                placeholder="Confirm password"
                disabled={isSubmitting}
                isInvalid={!!errors.passwordConfirm}
                ref={register({
                  validate: (value) =>
                    !value
                      ? 'required'
                      : value.length < 6
                      ? 'Password is too short'
                      : true,
                })}
              />
              <Form.Control.Feedback type="invalid">
                {errors.password
                  ? errors.password.message
                  : errors.passwordConfirm
                  ? errors.passwordConfirm.message
                  : ''}
              </Form.Control.Feedback>
            </Col>
            <Col xs="auto" lg="auto">
              <PrimaryButton disabled={isSubmitting} type="submit">
                Change
              </PrimaryButton>
            </Col>
          </Row>
        </Form.Group>
      </Form>
      <br />
    </>
  )
}
export default ChangePasswordForm
