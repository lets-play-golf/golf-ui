import React from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { useUpdateUser } from '../../queries'
import { useForm } from 'react-hook-form'
import Form from 'react-bootstrap/Form'
import { resolveBackendError } from '../../../errors'
import { PrimaryButton } from 'shared/Buttons'
import formElements from '../formElements'

const ChangeTextForm = (props) => {
  const [detail, currentValue] = Object.entries(props)[0]
  const [updateUser] = useUpdateUser()
  const {
    formState: { isSubmitting },
    handleSubmit,
    register,
    errors,
    setError,
  } = useForm({ defaultValues: {} })

  const onSubmit = handleSubmit(async (formParams) => {
    const update = { [detail]: formParams[detail] }
    try {
      const {
        data: {
          updateUser: { error },
        },
      } = await updateUser({ variables: { update } })
      if (!error) return window.location.reload()
      const { field, key, message } = resolveBackendError(error)
      setError(field, key, message)
    } catch (e) {
      console.error(`CreateUser submit error: ${e}`)
      console.log('error', e)
    }
  })

  const element = formElements[detail]

  return (
    <>
      <p>
        {detail}: {currentValue}
      </p>
      <Form onSubmit={onSubmit}>
        <Form.Group>
          <Row>
            <Col>
              <Form.Control
                name={detail}
                type={element.type}
                placeholder={`New ${element.label}`}
                disabled={isSubmitting}
                isInvalid={!!errors[detail]}
                ref={register(element.register)}
              />
              <Form.Control.Feedback type="invalid">
                {errors[detail] && errors[detail].message}
              </Form.Control.Feedback>
            </Col>
            <Col xs="auto" lg="auto">
              <PrimaryButton disabled={isSubmitting} type="submit">
                Change
              </PrimaryButton>
            </Col>
          </Row>
        </Form.Group>
      </Form>
      <br />
    </>
  )
}
export default ChangeTextForm
