import React from 'react'
import { useForm } from 'react-hook-form'
import { PrimaryButton } from 'shared/Buttons'
import { resolveBackendError } from '../../errors'
import FormControl from 'shared/FormControl'
import { Link, navigate } from '@reach/router'
import { useCreateUser } from '../queries'
import { useSessionContext } from 'components/sessions/sessionContext'
import MainLayout from 'app/MainLayout'
import Form from 'react-bootstrap/Form'
import formElements from '../forms/formElements'

const CreateUser = () => {
  const {
    formState: { isSubmitting },
    handleSubmit,
    register,
    errors,
    setError,
  } = useForm({
    defaultValues: {
      emailWhenInvitationAccepted: true,
      emailWhenMessageReceivedOffline: true,
    },
  })

  const [createUser] = useCreateUser()
  const { persistSession } = useSessionContext()
  const onSubmit = handleSubmit(async (formParams) => {
    const {
      firstname,
      email,
      username,
      password,
      password_confirm: passwordConfirm,
      emailWhenInvitationAccepted,
      emailWhenMessageReceivedOffline,
    } = formParams
    if (password !== passwordConfirm)
      return setError(
        'password_confirm',
        'PASSWORDS_DONT_MATCH',
        "Passwords don't match"
      )
    try {
      const {
        data: {
          createUser: { error, ...session },
        },
      } = await createUser({
        variables: {
          firstname,
          email,
          password,
          username,
          emailWhenInvitationAccepted,
          emailWhenMessageReceivedOffline,
        },
      })
      if (!error) {
        persistSession(session)
        return navigate('/')
      }
      const { field, key, message } = resolveBackendError(error)
      setError(field, key, message)
    } catch (e) {
      console.error(`CreateUser submit error: ${e}`)
    }
  })

  const { email, username, firstname, password, passwordConfirm } = formElements

  return (
    <MainLayout>
      <h1>Register</h1>
      <p>
        Got an account? <Link to="/login">login</Link>
      </p>
      <Form onSubmit={onSubmit}>
        <Form.Group controlId="validationFirstname">
          <Form.Label>Your name</Form.Label>
          <FormControl
            name="firstname"
            type="text"
            placeholder="Enter your name"
            disabled={isSubmitting}
            isInvalid={!!errors.firstname}
            ref={register(firstname.register)}
          />
          <Form.Control.Feedback type="invalid">
            {errors.firstname && errors.firstname.message}
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group controlId="validationUsername">
          <Form.Label>Username</Form.Label>
          <FormControl
            name="username"
            type="text"
            placeholder="Try your first name"
            disabled={isSubmitting}
            isInvalid={!!errors.username}
            ref={register(username.register)}
          />
          <Form.Control.Feedback type="invalid">
            {errors.username && errors.username.message}
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group controlId="validationEmail">
          <Form.Label>Email</Form.Label>
          <FormControl
            name="email"
            type="email"
            placeholder="Your email"
            disabled={isSubmitting}
            isInvalid={!!errors.email}
            ref={register(email.register)}
          />
          <Form.Control.Feedback type="invalid">
            {errors.email && errors.email.message}
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group controlId="validationPassword">
          <Form.Label>Password</Form.Label>
          <FormControl
            name="password"
            type="password"
            placeholder="Choose a password"
            disabled={isSubmitting}
            isInvalid={!!errors.password}
            ref={register(password.register)}
          />
          <Form.Control.Feedback type="invalid">
            {errors.password && errors.password.message}
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group controlId="validationConfirmPassword">
          <Form.Label>Confirm password</Form.Label>
          <FormControl
            name="password_confirm"
            type="password"
            placeholder="Confirm password"
            disabled={isSubmitting}
            isInvalid={!!errors.password_confirm}
            ref={register(passwordConfirm.register)}
          />
          <Form.Control.Feedback type="invalid">
            {errors.password_confirm && errors.password_confirm.message}
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group controlId="emailNotificationsCheckbox">
          <Form.Label>Email me when:</Form.Label>
          <Form.Check
            name="emailWhenInvitationAccepted"
            ref={register()}
            type="checkbox"
            label="Someone joins my game"
          />
          <Form.Check
            name="emailWhenMessageReceivedOffline"
            ref={register()}
            type="checkbox"
            label="Someone messages me and I'm offline"
          />
        </Form.Group>

        {errors.form && (
          <Form.Text className="text-muted">{errors.form.message}</Form.Text>
        )}

        <PrimaryButton disabled={isSubmitting} type="submit">
          Register
        </PrimaryButton>
      </Form>
    </MainLayout>
  )
}

export default CreateUser
