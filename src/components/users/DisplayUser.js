import React from 'react'
import { useSessionContext } from 'components/sessions/sessionContext'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { useUserQuery } from './queries'
import { Link, Redirect } from '@reach/router'
import { PrimaryButton } from 'shared/Buttons'

const DisplayUser = () => {
  const { session, handleExpiredSession } = useSessionContext()
  const { data, error } = useUserQuery()
  if (!session) return <Redirect to="/login" noThrow />
  if (error) handleExpiredSession(error)
  if (!data) return <></>

  const {
    firstname,
    username,
    email,
    preferences: {
      emailWhenInvitationAccepted,
      emailWhenMessageReceivedOffline,
    },
  } = data.me

  return (
    <>
      <Row>
        <Col xs="5">Username:</Col>
        <Col>{username}</Col>
      </Row>
      <Row>
        <Col xs="5">Email: </Col>
        <Col>{email}</Col>
      </Row>
      <Row>
        <Col xs="5">Name: </Col>
        <Col>{firstname}</Col>
      </Row>
      <Row>
        <Col xs="5">Password: </Col>
        <Col>********</Col>
      </Row>
      <div className="mt-3">Email me when:</div>
      <ul>
        <li>
          {`I receive a message and I'm offline:`}{' '}
          {emailWhenMessageReceivedOffline ? 'yes' : 'no'}
        </li>
        <li>
          A user accepts my invitation:{' '}
          {emailWhenInvitationAccepted ? 'yes' : 'no'}
        </li>
      </ul>

      <Link to="/update-account">
        <PrimaryButton>Update your account details</PrimaryButton>
      </Link>
    </>
  )
}
export default DisplayUser
