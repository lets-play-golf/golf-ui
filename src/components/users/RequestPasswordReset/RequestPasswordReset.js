import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { PrimaryButton } from 'shared/Buttons'
import FormControl from 'shared/FormControl'
import { useRequestPasswordReset } from '../queries'
import MainLayout from 'app/MainLayout'
import Form from 'react-bootstrap/Form'
import { Link } from '@reach/router'
import formElements from '../forms/formElements'

const RequestPasswordReset = () => {
  const emailState = useState(false)
  const [emailSent] = emailState
  return (
    <MainLayout>
      {emailSent ? (
        <ResetPasswordSuccessful email={emailSent} />
      ) : (
        <ResetPasswordForm emailState={emailState} />
      )}
    </MainLayout>
  )
}

const ResetPasswordForm = ({ emailState }) => {
  const setEmailSent = emailState[1]
  const {
    formState: { isSubmitting },
    handleSubmit,
    register,
    errors,
    setError,
  } = useForm({
    defaultValues: {
      emailWhenInvitationAccepted: true,
      emailWhenMessageReceivedOffline: true,
    },
  })

  const [requestPasswordReset] = useRequestPasswordReset()
  const onSubmit = handleSubmit(async ({ email }) => {
    try {
      const { data, error } = await requestPasswordReset({
        variables: { email },
      })
      if (error || !data.requestPasswordReset)
        setError(
          'email',
          'INTERNAL_SERVER_ERROR',
          'We could not send you an email at this time. Please try again in a short while.'
        )
      if (data.requestPasswordReset) return setEmailSent(email)
    } catch (e) {
      console.error(`CreateUser submit error: ${e}`)
    }
  })
  return (
    <>
      <h1>Resetting your password</h1>
      <p>
        {
          "Type in your email address. We'll send you an email with a password reset link."
        }
      </p>
      <Form onSubmit={onSubmit}>
        <Form.Group controlId="validationEmail">
          <Form.Label>Email</Form.Label>
          <FormControl
            name="email"
            type="email"
            placeholder="Enter email"
            disabled={isSubmitting}
            isInvalid={!!errors.email}
            ref={register(formElements.email.register)}
          />
          <Form.Control.Feedback type="invalid">
            {errors.email && errors.email.message}
          </Form.Control.Feedback>
        </Form.Group>

        <PrimaryButton disabled={isSubmitting} type="submit">
          Request Password Reset
        </PrimaryButton>
      </Form>
    </>
  )
}

const ResetPasswordSuccessful = ({ email }) => (
  <>
    <h1>Check your email</h1>
    <p>An email was sent to {email} with a reset link.</p>
    <p>Notice: The password reset link will expire within 15 minutes.</p>
    <Link to="/login">
      <PrimaryButton>Go to Login page</PrimaryButton>
    </Link>
  </>
)
export default RequestPasswordReset
