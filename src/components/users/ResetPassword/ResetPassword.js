import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { PrimaryButton } from 'shared/Buttons'
import FormControl from 'shared/FormControl'
import { useResetPassword } from '../queries'
import MainLayout from 'app/MainLayout'
import Form from 'react-bootstrap/Form'
import { Link } from '@reach/router'
import formElements from '../forms/formElements'

const ResetPassword = ({ resetHash }) => {
  const passwordResetState = useState(null)
  const [isPasswordReset] = passwordResetState
  return (
    <MainLayout>
      {isPasswordReset !== null ? (
        <ResetPasswordRequested passwordResetState={passwordResetState} />
      ) : isPasswordReset == null ? (
        <ResetPasswordForm
          resetHash={resetHash}
          passwordResetState={passwordResetState}
        />
      ) : (
        <ResetPasswordForm />
      )}
    </MainLayout>
  )
}

const ResetPasswordForm = ({ resetHash, passwordResetState }) => {
  const setPasswordReset = passwordResetState[1]
  const {
    formState: { isSubmitting },
    handleSubmit,
    register,
    errors,
    setError,
  } = useForm({
    defaultValues: {
      emailWhenInvitationAccepted: true,
      emailWhenMessageReceivedOffline: true,
    },
  })

  const [resetPassword] = useResetPassword()
  const onSubmit = handleSubmit(async ({ password, passwordConfirm }) => {
    try {
      if (password !== passwordConfirm)
        return setError(
          'passwordConfirm',
          'PASSWORDS_DONT_MATCH',
          "Passwords don't match"
        )
      const { data, error } = await resetPassword({
        variables: { password, resetHash },
      })
      if (error)
        setError(
          'password',
          'INTERNAL_SERVER_ERROR',
          'We could not send you an email at this time. Please try again in a short while.'
        )
      if (data) return setPasswordReset(data.resetPassword)
    } catch (e) {
      console.error(`CreateUser submit error: ${e}`)
    }
  })
  return (
    <>
      <h1>Your new password</h1>
      <p>Choose and confirm your new password</p>
      <Form onSubmit={onSubmit}>
        <Form.Group controlId="validationPassword">
          <Form.Label>Password</Form.Label>
          <FormControl
            name="password"
            type="password"
            placeholder="New password"
            disabled={isSubmitting}
            isInvalid={!!errors.password}
            ref={register(formElements.password.register)}
          />
          <Form.Control.Feedback type="invalid">
            {errors.password && errors.password.message}
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group controlId="validationPasswordConfirm">
          <Form.Label>Confirm password</Form.Label>
          <FormControl
            name="passwordConfirm"
            type="password"
            placeholder="Confirm password"
            disabled={isSubmitting}
            isInvalid={!!errors.passwordConfirm}
            ref={register(formElements.passwordConfirm.register)}
          />
          <Form.Control.Feedback type="invalid">
            {errors.passwordConfirm && errors.passwordConfirm.message}
          </Form.Control.Feedback>
        </Form.Group>

        <PrimaryButton disabled={isSubmitting} type="submit">
          Save new password
        </PrimaryButton>
      </Form>
    </>
  )
}

const ResetPasswordRequested = ({ passwordResetState }) => {
  const isSuccessful = passwordResetState[0]

  return isSuccessful ? (
    <>
      <h1>Your password was reset</h1>
      <p>You can now log in using your new password</p>
      <Link to="/login">
        <PrimaryButton>Go to Login page</PrimaryButton>
      </Link>
    </>
  ) : (
    <>
      <h1>Huh-Hoh</h1>
      <p>We could not reset your password, this may be because: </p>
      <ul>
        <li>The reset link has already been used.</li>
        <li>
          The reset link expired - it has to be used within 15 minutes of
          requesting a password reset.
        </li>
        <li>
          Another link was requested after this one, in this case you need to
          use the latest reset link.
        </li>
      </ul>
      <Link to="/password-reset">
        <PrimaryButton>Request a new Password Reset Link</PrimaryButton>
      </Link>
    </>
  )
}
export default ResetPassword
