import React from 'react'
import { useSessionContext } from '../../sessions/sessionContext'
import MainLayout from '../../../app/MainLayout'
import { Redirect, navigate } from '@reach/router'
import { PrimaryButton } from 'shared/Buttons'
import { useDeleteUser } from '../queries'

const DeleteUser = () => {
  const { session, deleteSession } = useSessionContext()
  const [deleteUser] = useDeleteUser()
  const [isSure, setIsSure] = React.useState()

  if (!session) return <Redirect to="/login" noThrow />

  const onClick = async () => {
    const {
      data: { deleteUser: isDeleted },
    } = await deleteUser()
    if (!isDeleted) return window.location.reload()
    deleteSession()
    return navigate('/')
  }

  return (
    <MainLayout>
      <h1>Delete Account</h1>
      {!isSure ? (
        <>
          <h2>Are you sure?</h2>
          <ul>
            <li>Your score and personal information will be lost.</li>
            <li>This is this irreversible.</li>
          </ul>
          <PrimaryButton variant="danger" onClick={() => setIsSure(true)}>
            {`Yes, I'm sure!`}
          </PrimaryButton>
        </>
      ) : (
        <>
          <h2>Ok then!</h2>
          <ul>
            <li>Your score and personal information will be lost.</li>
            <li>This is this irreversible.</li>
          </ul>
          <PrimaryButton variant="danger" onClick={onClick}>
            Delete My Account Now
          </PrimaryButton>
        </>
      )}
    </MainLayout>
  )
}
export default DeleteUser
