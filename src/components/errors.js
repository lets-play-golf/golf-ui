const BACKEND_ERRORS = [
  {
    key: 'WRONG_CREDENTIALS',
    field: 'form',
    message: () => 'Incorrect email or password',
  },
  {
    key: 'USER_EMAIL_ALREADY_EXISTS',
    field: 'email',
    message: () => 'An account already exists for this email',
  },
  {
    key: 'USERNAME_ALREADY_EXISTS',
    field: 'username',
    message: () => 'This username is already used',
  },
  {
    key: 'WEAK_PASSWORD',
    field: 'password',
    message: () =>
      'Please enter at least 6 characters and a mix of upper case, lower case and numbers.',
  },
  {
    key: 'INVALID_USERNAME',
    field: 'username',
    message: () =>
      'Your username can only contain between 3-8 lower case letters, numbers or dash-sign',
  },
  {
    key: 'INVALID_EMAIL',
    field: 'email',
    message: () => 'Please enter a valid email address',
  },
  {
    key: 'ACCOUNT_LOCKED',
    field: 'form',
    message: ({ time }) =>
      `You have entered your password wrong too many times, your account is now locked for ${time} minute${
        time === 1 ? '' : 's'
      }.`,
  },
]

export const resolveBackendError = ({ key, message, field, data }) => {
  const error = getError({ key, data })
  if (error) {
    if (typeof error.message === 'function')
      error.message =
        typeof error.message === 'function'
          ? error.message(data)
          : error.message
    return error
  }
  return { key, message, field }
}

export const getError = ({ key, data }) => {
  const error = BACKEND_ERRORS.find((itm) => itm.key === key)
  return { ...error, message: error.message(data) }
}
