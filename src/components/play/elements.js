import styled from 'styled-components'
import RowRB from 'react-bootstrap/Row'

export const Row = styled(RowRB)`
  align-items: center;
  height: 6rem;
  justify-content: center;
`

export const Panel = styled.div`
  position: relative;
  text-align: center;
`
