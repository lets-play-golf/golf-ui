import React from 'react'
import GameContainer from '../GameContainer'
import { PlayGameMenu } from '../PlayGameMenu'
import { usePlayGameContext } from '../playGameContext'
import LargeDecks from '../Decks/LargeDecks'
import { Away } from '../Players/PlayersRows'
import PlayGamePlayer from '../Players/PlayGamePlayer'
import PickCardsPlayer from '../Players/PickCardsPlayer'
import StartRoundActionPanel from './StartRoundActionPanel'

const StartedGame = () => {
  const {
    game,
    startedGame,
    startedGame: {
      players: { home },
      nextTurn: { actions },
    },
  } = usePlayGameContext()

  const isRoundStarted = !actions.some((itm) =>
    ['start', 'pick_cards'].includes(itm)
  )

  return (
    <GameContainer>
      <PlayGameMenu game={game} />
      <Away startedGame={startedGame} />
      {isRoundStarted ? (
        <LargeDecks startedGame={startedGame} />
      ) : (
        <StartRoundActionPanel />
      )}
      {home.actions.includes('pick_cards') ? (
        <PickCardsPlayer player={home} />
      ) : (
        <PlayGamePlayer player={home} />
      )}
    </GameContainer>
  )
}

export default StartedGame
