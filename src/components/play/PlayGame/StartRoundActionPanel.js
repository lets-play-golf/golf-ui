import React from 'react'
import styled from 'styled-components'
import { PrimaryButton } from 'shared/Buttons'
import { useStart } from '../queries'
import ReactAnimatedEllipsis from 'react-animated-ellipsis'
import { usePlayGameContext } from '../playGameContext'
import Card from 'shared/Card'
import FlexContentControl from 'shared/FlexContentControl'

const StartRound = () => {
  const {
    startedGame: {
      players: {
        home,
        away: [away],
      },
    },
  } = usePlayGameContext()

  if (home.actions.includes('start')) return <StartNewRound />

  if (home.actions.includes('pick_cards')) return <PickCards />

  // away's turn
  const waitingForNotice = `Waiting for ${away.username} to ${
    away.actions.includes('start') ? 'start the round' : 'pick cards'
  }`

  return (
    <DecksWrapper>
      <p style={{ margin: 0 }}>{waitingForNotice}</p>
      <ReactAnimatedEllipsis />
    </DecksWrapper>
  )
}

const StartNewRound = () => {
  const {
    startedGame: { id },
  } = usePlayGameContext()
  const [start] = useStart({ variables: { game: id } })
  return (
    <DecksWrapper>
      <h1 style={{ marginBottom: 0 }}>Ready?</h1>
      <PrimaryButton onClick={start} style={{ width: '170px', margin: 'auto' }}>
        Start a new round
      </PrimaryButton>
    </DecksWrapper>
  )
}

const PickCards = () => {
  return (
    <DecksWrapper style={{ maxWidth: '280px' }}>
      <h1 style={{ marginBottom: 0 }}>Pick 2 cards you'd like to see</h1>
    </DecksWrapper>
  )
}

const Wrapper = styled(Card)`
  padding: 1rem;
  margin: 0 auto;
  text-align: center;
`

export const DecksWrapper = ({ children, ...props }) => {
  return (
    <Wrapper {...props}>
      <FlexContentControl
        flow="column"
        style={{ gap: '1rem', justifyContent: 'center' }}
      >
        {children}
      </FlexContentControl>
    </Wrapper>
  )
}
export default StartRound
