import React from 'react'
import GameContainer from '../GameContainer'
import Card from 'shared/Card'
import { Trophy } from 'app/icons'
import { Link } from '@reach/router'
import { PrimaryButton, SecondaryButton } from 'shared/Buttons'
import { usePlayGameContext } from '../playGameContext'
import { PlayGameMenu } from '../PlayGameMenu'
import PlayGamePlayer from '../Players/PlayGamePlayer'
import { Panel } from '../elements'
import Stack from 'shared/content-control/Stack'

const FinishedGame = () => {
  const {
    game,
    startedGame: {
      players: { home, away },
    },
  } = usePlayGameContext()

  const orderedPlayers = [...away, home].sort(
    (a, b) => a.totalScore > b.totalScore
  )

  return (
    <GameContainer>
      <PlayGameMenu game={game} />
      <Panel centered style={{ minHeight: '19rem' }}>
        <h1>
          Game over:{' '}
          {home.isGameWinner
            ? 'You win'
            : home.isGameLoser
            ? 'you lose'
            : `It's a tie!`}
        </h1>
        <Card>
          <Card.Body>
            <Stack space="2">
              {orderedPlayers.map((itm) => (
                <Player
                  key={itm.id}
                  player={itm}
                  total={itm.totalScore}
                  isWinning={itm.isGameWinner}
                />
              ))}
              <Link to="/create">
                <PrimaryButton>Play again</PrimaryButton>
              </Link>
              your rating was updated
              <Link to="/leaderboard">
                <SecondaryButton>Check the Leaderboard</SecondaryButton>
              </Link>
            </Stack>
          </Card.Body>
        </Card>
      </Panel>
      <PlayGamePlayer player={home} />
    </GameContainer>
  )
}

const Player = ({ player, isWinning, total }) => (
  <div className="mb-">
    <span>
      {player.username}: {total} {isWinning ? <Trophy /> : ''}
    </span>
  </div>
)

export default FinishedGame
