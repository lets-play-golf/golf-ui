import React from 'react'
import styled from 'styled-components'
import { Panel } from '../elements'
import GameCard from 'components/games/GameCard'
import { Button, PrimaryButton } from 'shared/Buttons'
import { HTTP_URL } from 'config'
import useCopyToClipboard from 'app/useCopyToClipboard'
import TooltipTrigger from 'shared/TooltipTrigger'
import GameContainer from '../GameContainer'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Stack from 'shared/content-control/Stack'
import { Robot, Friends } from 'app/icons'
import EvenlySpaced from 'shared/content-control/EvenlySpaced'
import { useAddABot, useStartGame } from '../queries'
import { navigate } from '@reach/router'
import * as Menu from 'components/play/PlayGameMenu'
import { MAX_NUMBER_OF_PLAYERS } from 'app/constants'
import Card from 'shared/Card'

const PANELS = {
  DEFAULT: 'DEFAULT',
  ADD_PARTICIPANT: 'ADD_PARTICIPANT',
}

/**
 * Entry point for a game that has been created
 */
const CreatedGame = ({ game }) => {
  const panelState = React.useState(PANELS.DEFAULT)
  return (
    <GameContainer>
      <CreatedGameMenu {...{ panelState, game }} />
      {(() => {
        if (panelState[0] === PANELS.DEFAULT)
          return <DefaultPanel game={game} panelState={panelState} />
        if (panelState[0] === PANELS.ADD_PARTICIPANT)
          return <AddParticipantPanel game={game} panelState={panelState} />
        return <DefaultPanel game={game} panelState={panelState} />
      })()}
      <div style={{ height: '3rem' }}></div>
    </GameContainer>
  )
}

const DefaultPanel = ({ game: { players }, panelState, game }) => {
  const AddPlayerButton = ({ panelState: [_, setPanel] }) => {
    return (
      <PrimaryButton onClick={() => setPanel(PANELS.ADD_PARTICIPANT)}>
        Add a player
      </PrimaryButton>
    )
  }

  return (
    <Panel centered style={{ minHeight: '23rem' }}>
      <Stack space={3}>
        <h1>Game Open</h1>
        <GameCard game={game} hideActions style={{ margin: 'auto' }} />
        {game.players.length < 2 ? (
          <Stack space={3}>
            <div>
              {players.length < 2
                ? `Add 1 more player to start the game`
                : `Add more players or start the game`}
            </div>
            <AddPlayerButton panelState={panelState} />
          </Stack>
        ) : (
          <Stack space={3}>
            <StartGameButton game={game} />
            {players.length < MAX_NUMBER_OF_PLAYERS && (
              <CopyLinkAndShareButton game={game} isPrimary={false} />
            )}
          </Stack>
        )}
      </Stack>
    </Panel>
  )
}

const CreatedGameMenu = ({ panelState, game: { players } }) => {
  const [panel, setPanel] = panelState

  const onBack =
    panel === PANELS.DEFAULT
      ? () => navigate('/')
      : () => setPanel(PANELS.DEFAULT)
  return (
    <Menu.Wrapper>
      <Menu.BackMenuItem onClick={onBack} />
      {players.length > 1 && <Menu.ChatMenuItem />}
    </Menu.Wrapper>
  )
}

const StartGameButton = ({ game: { id } }) => {
  const [startGame] = useStartGame({ variables: { id } })
  return <PrimaryButton onClick={startGame}>Start game now!</PrimaryButton>
}

const AddParticipantPanel = ({ game, game: { id, players }, panelState }) => {
  const [isPlayWithBotSelected, setIsPlayWithBotSelected] = React.useState(null)

  const SelectAnOption = () => (
    <Stack space={3}>
      <div>Pick an option</div>
    </Stack>
  )

  const AddABot = ({ game: { id } }) => {
    const [addRandobot] = useAddABot({
      variables: { id, bot: '6318b28aea4bd4003c0b7e91' },
    })
    const handleOnClick = async () => {
      await addRandobot()
      panelState[1](PANELS.DEFAULT)
    }
    return (
      <Stack space={3}>
        <div>Select a bot to play with</div>
        <PrimaryButton onClick={handleOnClick}>
          Play with randobot
        </PrimaryButton>
      </Stack>
    )
  }

  const AddFriends = () => (
    <Stack space={3}>
      <Card style={{ textAlign: 'left' }} className={'p-3'}>
        <ul className={'p-0 m-0 ml-3'}>
          <li>Share the link with friends</li>
          <li>Go back and wait for them to join</li>
        </ul>
      </Card>
      <CopyLinkAndShareButton game={game} />
    </Stack>
  )

  return (
    <Panel centered style={{ minHeight: '20rem' }}>
      <Stack>
        <h1>Add a Player</h1>
        <ButtonGroup aria-label="Login mean" className="mb-3">
          <Button
            isPrimary={isPlayWithBotSelected}
            onClick={() => setIsPlayWithBotSelected(true)}
          >
            <IconWrapper>
              <Robot />
            </IconWrapper>
            Play with a bot
          </Button>
          <Button
            isPrimary={!isPlayWithBotSelected && isPlayWithBotSelected != null}
            onClick={() => setIsPlayWithBotSelected(false)}
          >
            <IconWrapper>
              <Friends />
            </IconWrapper>
            Play with friends
          </Button>
        </ButtonGroup>
        {(() => {
          if (isPlayWithBotSelected == null) return <SelectAnOption />
          if (isPlayWithBotSelected === true) return <AddABot game={game} />
          if (isPlayWithBotSelected === false) return <AddFriends />
        })()}
      </Stack>
    </Panel>
  )
}

const CopyLinkAndShareButton = ({ game: { id }, isPrimary = true }) => {
  const [copy] = useCopyToClipboard(`${HTTP_URL}/play/${id}`)

  return (
    <TooltipTrigger text="Link copied!" placement={'bottom'}>
      <Button onClick={copy} mt={'3'} isPrimary={isPrimary}>
        Copy link and invite friends
      </Button>
    </TooltipTrigger>
  )
}

const IconWrapper = styled(EvenlySpaced.Row)`
  height: 3rem;
  margin: 1rem 0;
`

export default CreatedGame
