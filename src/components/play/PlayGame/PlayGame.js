import React from 'react'
import { Redirect } from '@reach/router'
import { useGameQuery, useSubscribeGame } from '../queries'
import { useSessionContext } from '../../sessions/sessionContext'
import FinishedGame from './FinishedGame'
import { useModalContext } from 'app/modal'
import { useSubscribeChat } from '../../chats/queries'
import {
  ChatContextProvider,
  useChatContext,
} from 'components/chats/chatContext'
import { PlayGameContextProvider, usePlayGameContext } from '../playGameContext'
import CreatedGame from './CreatedGame'
import PeekCardsModal from '../modals/PeekCardsModal'
import StartedGame from './StartedGame'
import ScoreBoardModal from '../modals/ScoreBoardModal'
import StartedGameTransitionModal from '../modals/StartedGameTransitionModal'

/**
 * Entry point for PlayGame UI
 * - Reads game and chat and set them is their context for downstream components
 * - Subscribe game and chat updates and refresh context and components.
 * @param {string} options.id: required - game id to subscribe to
 */

const PlayGame = ({ id }) => {
  const { session } = useSessionContext()
  const { data, loading } = useGameQuery({ id })
  if (loading) return null
  if (!loading && !data?.game) return <Redirect to={`/`} noThrow />
  const { game } = data

  const hasUserJoined = () =>
    game.players.some(({ user: { id } }) => session.user.id === id)

  if (!hasUserJoined()) return <Redirect to={`/join/${id}`} noThrow />

  return (
    <PlayGameContextProvider value={{ game }}>
      <ChatContextProvider value={{ chat: game.chat }}>
        <SubscribeGame />
      </ChatContextProvider>
    </PlayGameContextProvider>
  )
}

const SubscribeGame = () => {
  const { openModal } = useModalContext()
  const {
    game,
    game: { id, status },
    startedGame,
    setGame,
  } = usePlayGameContext()
  const { chat, onMessageReceived } = useChatContext()

  const skipFetchingUser = ['started', 'finished'].includes(status)

  /**
   * When `skipFetchingUser` is on, gameUpdate won't have user details, so we'll update it with the game's user details.
   * @param {*} game: original game as passed to this component
   * @param {*} gameUpdate: gameUpdate from the subscription
   * @returns gameUpdate with the users' details populated
   */
  const appendUsersToGame = (game, gameUpdate) => ({
    ...gameUpdate,
    players: gameUpdate.players.map((player) => ({
      ...player,
      user: game.players.find((itm) => itm.userId === player.userId).user,
    })),
  })

  useSubscribeGame({
    variables: { id, skipFetchingUser },
    onSubscriptionData: async ({
      subscriptionData: {
        data: { gameUpdate },
      },
    }) => {
      if (gameUpdate.__typename === 'PeekCards')
        return openModal(<PeekCardsModal spots={gameUpdate.spots} />, 8000)
      if (game.status === 'created' && gameUpdate.status === 'started')
        openModal(<StartedGameTransitionModal />, 8000)
      if (skipFetchingUser) gameUpdate = appendUsersToGame(game, gameUpdate)

      // If the round is complete, show the ScoreBoardModal
      const showScoreBoard =
        !isRoundComplete(game) && isRoundComplete(gameUpdate)
      if (showScoreBoard)
        openModal(
          <ScoreBoardModal
            players={startedGame.players}
            isRoundComplete={true}
          />
        )
      return setGame(gameUpdate)
    },
  })

  useSubscribeChat({
    variables: { id: chat && chat.id },
    skip: !chat,
    onSubscriptionData: onMessageReceived,
  })

  if (status === 'created') return <CreatedGame {...{ game }} />
  if (status === 'finished') return <FinishedGame />
  if (status === 'started') return <StartedGame />

  return <>Not sure what to do</>
}

const isRoundComplete = (game) =>
  game?.golf?.publicView?.players?.every(({ cards }) =>
    cards.every((i) => Number.isInteger(i))
  )

export default PlayGame
