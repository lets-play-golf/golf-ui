/**
 * Layers in the app (z-index values):
 * - Main (0): Main board for playing
 * - Help (500): black transparent overlay greying out main board
 * - Highlighted cards (510): when the help is showing, the clickable cards are highlighted.
 * - PlayGameModal (1000): ScoreBoard and Chat
 */

export const HELP_GREY_LAYER = 500

export const HELP_HIGHLIGHT_LAYER = 510

export const PLAY_GAME_MODAL_LAYER = 1000
