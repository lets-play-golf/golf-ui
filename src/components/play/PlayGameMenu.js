import React from 'react'
import styled from 'styled-components'
import { useModalContext } from 'app/modal'
import { ScoreBoardIcon, ChatIcon, ExitIcon, ReloadIcon } from 'app/icons'
import ScoreBoardModal from './modals/ScoreBoardModal'
import ChatModal from './modals/ChatModal'
import { navigate } from '@reach/router'
import { useChatContext } from 'components/chats/chatContext'
import TooltipTrigger from 'shared/TooltipTrigger'
import FlexContentControl from 'shared/FlexContentControl'

/**
 * Returns the correct menu item by reading the game status
 * @param {number} options.unreadMessages: Number of unread chat messages to be shown as a notification
 * @param {number} options.currentRoundNumber: Number of done rounds
 * @param {object} options.numberOfRounds: total number of rounds
 */
export const PlayGameMenu = ({ game: { numberOfRounds, status, players } }) => {
  const { openModal } = useModalContext()
  const openChatModal = () => openModal(<ChatModal />)
  const { unreadMessages } = useChatContext()

  // Started game menu
  if (status === 'started') {
    const currentRoundNumber = players[0].score.length + 1
    return (
      <Wrapper>
        <BackMenuItem />
        <ScoreBoardMenuItem />
        <MenuItem onClick={() => openChatModal()}>
          <ChatIcon />
          {!!unreadMessages && (
            <MenuItemNotification>{unreadMessages}</MenuItemNotification>
          )}
        </MenuItem>
        <TooltipTrigger
          text={`Round ${currentRoundNumber} of ${numberOfRounds}`}
        >
          <MenuItem>
            <span
              style={{
                letterSpacing: numberOfRounds > 9 ? '0' : '0.1em',
                cursor: 'pointer',
              }}
            >
              {currentRoundNumber}/{numberOfRounds}
            </span>
          </MenuItem>
        </TooltipTrigger>
        <ReloadMenuItem />
      </Wrapper>
    )
  }

  if (status === 'finished') {
    return (
      <Wrapper>
        <BackMenuItem />
        <ScoreBoardMenuItem />
        <MenuItem onClick={() => openChatModal()}>
          <ChatIcon />
          {!!unreadMessages && (
            <MenuItemNotification>{unreadMessages}</MenuItemNotification>
          )}
        </MenuItem>
      </Wrapper>
    )
  }

  return (
    <Wrapper>
      <BackMenuItem />
      <MenuItem onClick={() => openChatModal()}>
        <ChatIcon />
        {!!unreadMessages && (
          <MenuItemNotification>{unreadMessages}</MenuItemNotification>
        )}
      </MenuItem>
    </Wrapper>
  )
}

const ScoreBoardMenuItem = () => {
  const { openModal } = useModalContext()
  return (
    <MenuItem onClick={() => openModal(<ScoreBoardModal />)}>
      <ScoreBoardIcon />
    </MenuItem>
  )
}

export const ChatMenuItem = () => {
  const { unreadMessages } = useChatContext()
  const { openModal } = useModalContext()
  return (
    <MenuItem onClick={() => openModal(<ChatModal />)}>
      <ChatIcon />
      {!!unreadMessages && (
        <MenuItemNotification>{unreadMessages}</MenuItemNotification>
      )}
    </MenuItem>
  )
}

export const BackMenuItem = ({ onClick = () => navigate('/') }) => {
  return (
    <MenuItem onClick={onClick}>
      <ExitIcon />
    </MenuItem>
  )
}

export const ReloadMenuItem = () => {
  return (
    // eslint-disable-next-line no-restricted-globals
    <MenuItem onClick={() => location.reload()}>
      <ReloadIcon />
    </MenuItem>
  )
}

const MenuItem = ({ onClick, children }) => (
  <div style={{ textAlign: 'center', alignItems: 'center' }}>
    <Item onClick={onClick}>{children}</Item>
  </div>
)

export const Wrapper = ({ children }) => (
  <FlexContentControl
    style={{ justifyContent: 'space-around', height: '3rem' }}
  >
    {children}
  </FlexContentControl>
)

const Item = styled.div`
  cursor: ${(props) => (props.onClick ? 'pointer' : 'default')};
  display: inline-block;
  height: 3rem;
  width: 3rem;
  border-radius: 1.5rem;
  line-height: 3rem;
  border: 1px solid ${(props) => props.theme.text};
`

const MenuItemNotification = styled.div`
  position: absolute;
  display: inline-block;
  border-radius: 0.5rem;
  background-color: ${(props) => props.theme.accent2};
  width: 1rem;
  height: 1rem;
  color: white;
  line-height: 1rem;
  font-size: 0.6rem;
`
