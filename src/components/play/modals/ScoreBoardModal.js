import React from 'react'
import styled from 'styled-components'
import { useModalContext } from 'app/modal'
import GameContainer from '../GameContainer'
import { SecondaryButton } from 'shared/Buttons'
import { Chart } from 'react-charts'
import { usePlayGameContext } from '../playGameContext'
import { Trophy } from 'app/icons'
import { Away, Home } from '../Players/PlayersRows'
import { Panel } from '../elements'
import Card from 'shared/Card'
import { ChartIcon, LargeIcon } from 'app/icons'
import Stack from 'shared/content-control/Stack'

/**
 * Shows the ScoreBoard UI
 * @param {boolean} param0.isRoundComplete: This menas the model self opened as a result of the round being completed.
 */
const ScoreBoardModal = ({ isRoundComplete }) => {
  const { closeModal } = useModalContext()
  const {
    startedGame,
    startedGame: {
      players: { home },
      players,
      numberOfRounds,
    },
  } = usePlayGameContext()

  const showChart = home.score.length

  const subtitle = showChart
    ? 'The lowest scoring player wins'
    : 'Complete your first round to see the score board'

  const getTitle = (isRoundComplete) => {
    if (isRoundComplete)
      return `You ${home.isRoundWinner ? 'win' : 'lose'} this round`
    return home.isGameWinner
      ? 'Board of Fame'
      : home.isGameLoser
      ? 'Board of Shame'
      : 'Score board'
  }
  return (
    <GameContainer addModal={false}>
      <Away {...{ startedGame }} />
      <Panel>
        <h1>{getTitle(isRoundComplete)}</h1>
        <Panel style={{ maxWidth: `${numberOfRounds * 4}rem`, margin: 'auto' }}>
          <Card>
            <Card.Body>
              <Stack space={1}>
                {showChart ? (
                  <ScoreChart startedGame={startedGame} />
                ) : (
                  <LargeIcon style={{ margin: 'auto' }}>
                    <ChartIcon />
                  </LargeIcon>
                )}
                <Key players={players} isRoundComplete={isRoundComplete} />
                <div>{subtitle}</div>
              </Stack>
            </Card.Body>
          </Card>
        </Panel>
        <SecondaryButton onClick={closeModal} style={{ marginTop: '1rem' }}>
          ok
        </SecondaryButton>
      </Panel>
      <Home {...{ startedGame }} />
    </GameContainer>
  )
}

const ScoreChart = ({ startedGame: { players, numberOfRounds } }) => {
  // Order players by user alphabetically so username colors match chart colors
  const sortedPlayers = sortPlayers(players)

  const data = React.useMemo(
    () =>
      sortedPlayers.map(({ username, score, isGameWinner }) => {
        let totals = 0
        const label = isGameWinner ? <Trophy>{username}</Trophy> : username
        return {
          label,
          data: [
            [0, 0],
            ...new Array(numberOfRounds).fill().map((itm, index) => {
              if (score[index] === undefined) return [index + 1, null]
              totals = totals + score[index]
              return [index + 1, totals]
            }),
          ],
        }
      }),
    [sortedPlayers, numberOfRounds]
  )

  const axes = React.useMemo(
    () => [
      { primary: true, type: 'ordinal', position: 'bottom' },
      { type: 'linear', position: 'right' },
    ],
    []
  )

  return (
    <ChartWrapper style={{ position: 'relative' }}>
      <Chart data={data} axes={axes} tooltip />
    </ChartWrapper>
  )
}

const sortPlayers = ({ home, away }) =>
  [home, ...away].sort((a, b) => a.id.localeCompare(b.id))

// Shows a colored label to help user identif
const Key = ({ players, isRoundComplete }) => {
  const sortedPlayers = sortPlayers(players)
  return (
    <div>
      {sortedPlayers.map(({ username, score }, index) => {
        const lastScore = score.slice(-1)
        const sign = lastScore > 0 ? '+' : lastScore < 0 ? '-' : ''
        const appendScore = isRoundComplete ? ` ${sign}${lastScore}` : ''
        return (
          <span>
            <KeyLabel style={{ backgroundColor: playersColors[index] }}>
              {username}
              {appendScore}
            </KeyLabel>
            &nbsp;&nbsp;
          </span>
        )
      })}
    </div>
  )
}

const KeyLabel = styled.span`
  border-radius: 0.4rem;
  color: white;
  padding: 0.2rem;
`

const playersColors = [
  '#4ab5eb',
  '#fc6868',
  '#decf3f',
  '#60bd68',
  '#faa43a',
  '#c63b89',
]

const ChartWrapper = styled.div`
  width: 100%;
  height: 10rem;
`

export default ScoreBoardModal
