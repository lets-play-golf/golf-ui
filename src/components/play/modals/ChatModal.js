import React from 'react'
import styled from 'styled-components'
import { useModalContext } from 'app/modal'
import GameContainer from '../GameContainer'
import { usePlayGameContext } from '../playGameContext'
import { Away, Home } from '../Players/PlayersRows'
import { Panel } from '../elements'
import { CloseModal } from 'app/icons'
import ChatPanel from 'components/chats/ChatPanel'

const ChatModal = () => {
  const { closeModal } = useModalContext()
  const { startedGame } = usePlayGameContext()
  const showPlayers = !!startedGame

  return (
    <GameContainer addModal={false}>
      {showPlayers && <Away {...{ startedGame }} />}
      <Panel>
        <h1>Game Chat</h1>
        <CloseModalAction onClick={closeModal} style={{ cursor: 'pointer' }}>
          <CloseModal />
        </CloseModalAction>
        <ChatPanel />
      </Panel>
      {showPlayers && <Home {...{ startedGame }} />}
    </GameContainer>
  )
}

const CloseModalAction = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  font-size: 1.5rem;
`

export default ChatModal
