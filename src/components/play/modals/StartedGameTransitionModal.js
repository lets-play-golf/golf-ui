import React from 'react'
import GameContainer from '../GameContainer'
import { Row, Panel } from '../elements'
import CountDown from 'shared/CountDown'
import Stack from 'shared/content-control/Stack'
import { PrimaryButton } from 'shared/Buttons'
import { useModalContext } from 'app/modal'
import Card from 'shared/Card'

const StartedGameTransitionModal = () => {
  const { closeModal } = useModalContext()

  return (
    <GameContainer addModal={false}>
      <Row />
      <Panel>
        <Card style={{ padding: '1rem' }}>
          <Stack space={3}>
            <h1>Get ready!</h1>
            <img src="/logo192.png" style={{ width: '45px' }} alt="logo" />
            <div>Game is starting!</div>
            <CountDown timeInMs={5000} />
            <PrimaryButton onClick={closeModal}>ok</PrimaryButton>
          </Stack>
        </Card>
      </Panel>
      <Row />
    </GameContainer>
  )
}

export default StartedGameTransitionModal
