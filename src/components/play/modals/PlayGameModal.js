import React from 'react'
import styled from 'styled-components'
import { useModalContext } from 'app/modal'
import useKeyPress from 'app/useKeyPress'
import { PLAY_GAME_MODAL_LAYER } from '../layers'

const ModalOverlay = styled.div`
  position: fixed;
  display: ${(props) => (props.open ? 'block' : 'none')};
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: ${PLAY_GAME_MODAL_LAYER};
`

const ModalContent = styled.div`
  height: 100%;
  width: 100%;
  position: fixed;
  top: 0;
`

const PlayGameModal = () => {
  const { modalOpened, modalProps, closeModal } = useModalContext()

  useKeyPress('Escape') && closeModal()

  return (
    <ModalOverlay open={modalOpened}>
      <ModalContent open={modalOpened} onClose={() => closeModal()}>
        {modalProps.contentElement}
      </ModalContent>
    </ModalOverlay>
  )
}

export default PlayGameModal
