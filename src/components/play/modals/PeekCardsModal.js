import React from 'react'
import styled from 'styled-components'
import PeekCardsPlayer from '../Players/PeekCardsPlayer'
import CountDown from 'shared/CountDown'
import { usePlayGameContext } from '../playGameContext'
import { TakeALook } from 'app/icons'
import { Away } from '../Players/PlayersRows'
import GameContainer from 'components/play/GameContainer'
import FlexContentControl from 'shared/FlexContentControl'
import { useModalContext } from 'app/modal'
import Stack from 'shared/content-control/Stack'
import { PrimaryButton } from 'shared/Buttons'
import Card from 'shared/Card'
import { Panel } from '../elements'

const PeekCardsModal = ({ spots }) => {
  const {
    startedGame,
    startedGame: {
      players: { home },
    },
  } = usePlayGameContext()

  const peekCards = { ...home, visibleCards: spots }
  const { closeModal } = useModalContext()

  return (
    <GameContainer addModal={false}>
      <Away startedGame={startedGame} />
      <Panel>
        <Wrapper>
          <FlexContentControl flow="column">
            <Stack space={'3'}>
              <h1>
                <TakeALook>Remember your cards</TakeALook>
              </h1>
              <CountDown timeInMs={8000} />
              <PrimaryButton onClick={closeModal}>ok</PrimaryButton>
            </Stack>
          </FlexContentControl>
        </Wrapper>
      </Panel>
      <PeekCardsPlayer player={peekCards} />
    </GameContainer>
  )
}

const Wrapper = styled(Card)`
  padding: 1rem;
  margin: 0 auto;
  text-align: center;
`

export default PeekCardsModal
