import React from 'react'
import PlayGameModal from 'components/play/modals/PlayGameModal'
import styled from 'styled-components'
import Container from 'react-bootstrap/Container'
import FlexContentControl from 'shared/FlexContentControl'

const backgrounds = {
  borealis:
    'https://images.pexels.com/photos/3374210/pexels-photo-3374210.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1', // aurora borealis
  cloud:
    'https://images.pexels.com/photos/4737484/pexels-photo-4737484.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1', // blue clouds
  purpleStaryLandscape:
    'https://images.pexels.com/photos/957040/night-photograph-starry-sky-night-sky-star-957040.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  purpleStars:
    'https://images.pexels.com/photos/755726/pexels-photo-755726.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  blueShootingStars:
    'https://images.pexels.com/photos/2786930/pexels-photo-2786930.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
}

const backgroundKeys = Object.keys(backgrounds)

const GameContainerWrapper = styled(Container)`
  height: 100%;
  width: 100%;
  padding: 1rem;
  background-image: url('${backgrounds[
    backgroundKeys[Math.floor(Math.random() * backgroundKeys.length)]
  ]}');
  background-repeat: no-repeat;
  background-size: cover;
`

const GameContainer = ({ children = [], addModal = true }) => (
  <GameContainerWrapper fluid className="noselect">
    {addModal && <PlayGameModal />}
    <FlexContentControl flow="column">{children}</FlexContentControl>
  </GameContainerWrapper>
)

export default GameContainer
