import React from 'react'
import styled from 'styled-components'
import SmallPlayingCard from '../PlayingCards/SmallPlayingCard'
import { roundCardScores } from '../PlayingCards/cardHelpers'
import Card from 'shared/Card'
import EvenlySpaced from 'shared/content-control/EvenlySpaced'

const Wrapper = styled(Card)`
  color: white;
  border-radius: 0.4rem;
  padding: 0.6rem;
  width: 9rem;
  height: 5rem;
`
const SmallPlayer = ({ player: { username, visibleCards, totalScore } }) => {
  // Show card scores if all 4 cards are played
  const showCardScores =
    visibleCards.filter((itm) => Number.isInteger(itm)).length === 4
  const scores = showCardScores
    ? roundCardScores(visibleCards)
    : new Array(4).fill()
  return (
    <Wrapper>
      <EvenlySpaced.Column flow={'column'}>
        <div>
          <span>{username}</span>
          <span className="float-right">{totalScore}</span>
        </div>
        <EvenlySpaced.Row>
          {visibleCards.map((card, index) => (
            <SmallPlayingCard id={card} key={index} score={scores[index]} />
          ))}
        </EvenlySpaced.Row>
      </EvenlySpaced.Column>
    </Wrapper>
  )
}

export default SmallPlayer
