import React from 'react'
import styled from 'styled-components'
import RowRB from 'react-bootstrap/Row'
import SmallPlayer from './SmallPlayer'
import SmallDecks from '../Decks/SmallDecks'
import EvenlySpaced from 'shared/content-control/EvenlySpaced'

// Cross-browser support for hiding scrollbar

export const Row = styled(RowRB)`
  align-items: center;
  justify-content: center;
`

export const Away = ({
  startedGame: {
    players: { away },
  },
}) => {
  return (
    <Row>
      <EvenlySpaced.Row
        style={{
          gap: '1rem',
          justifyContent: 'center',
          flexWrap: 'wrap',
        }}
      >
        {away.map((player) => (
          <SmallPlayer player={player} key={player.id} />
        ))}
      </EvenlySpaced.Row>
    </Row>
  )
}

export const Home = ({
  startedGame: {
    discardCard,
    players: { home },
  },
}) => {
  return (
    <Row>
      <EvenlySpaced.Row style={{ gap: '1rem', justifyContent: 'center' }}>
        <SmallPlayer player={home} />
        <SmallDecks discardCard={discardCard} />
      </EvenlySpaced.Row>
    </Row>
  )
}
