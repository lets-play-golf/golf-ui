import React from 'react'
import LargePlayer from './LargePlayer'
import LargePlayingCard from '../PlayingCards/LargePlayingCard'
import { PlayingCardFlipper } from '../PlayingCards/PlayingCardFlipper'

const PeekCardsPlayer = ({ player: { visibleCards }, player }) => {
  // Controls the cards flipping back at the end of the transition
  const [revealCardsState, setRevealCardsState] = React.useState(false)
  setTimeout(() => setRevealCardsState(true), 7000)

  return (
    <LargePlayer player={player}>
      {visibleCards.map((cardId, spot) => {
        if (!Number.isInteger(cardId)) return <LargePlayingCard key={spot} />
        if (!revealCardsState)
          return (
            <PlayingCardFlipper
              from={<LargePlayingCard />}
              to={<LargePlayingCard id={cardId} />}
              delay={200}
              key={`${revealCardsState}-${spot}`}
            />
          )
        return (
          <PlayingCardFlipper
            from={<LargePlayingCard id={cardId} />}
            to={<LargePlayingCard />}
            delay={500}
            key={`${revealCardsState}-${spot}`}
          />
        )
      })}
    </LargePlayer>
  )
}

export default PeekCardsPlayer
