import React from 'react'
import LargePlayer from './LargePlayer'
import LargePlayingCard from '../PlayingCards/LargePlayingCard'
import { REVEAL, PLACE } from '../queries'
import { usePlayGameContext } from '../playGameContext'
import { useMutation } from '@apollo/react-hooks'
import { roundCardScores } from '../PlayingCards/cardHelpers'

/**
 * Displays a Big Player with their cards and actions to play cards
 */
const PlayerGamePanel = ({ player: { visibleCards, actions }, player }) => {
  const {
    game: { id },
  } = usePlayGameContext()

  // Show card scores if all 4 cards are played
  const showCardScores =
    visibleCards.filter((itm) => Number.isInteger(itm)).length === 4
  const scores = showCardScores
    ? roundCardScores(visibleCards)
    : new Array(4).fill()
  return (
    <LargePlayer player={player}>
      {player.visibleCards.map((cardId, spot) => {
        if (Number.isInteger(cardId))
          return (
            <LargePlayingCard key={spot} id={cardId} score={scores[spot]} />
          )
        if (actions.includes('reveal') || actions.includes('place'))
          return (
            <ClickableCard
              key={spot}
              {...{ actions, variables: { game: id, spot } }}
            />
          )
        return <LargePlayingCard key={spot} />
      })}
    </LargePlayer>
  )
}

const ClickableCard = ({ actions, variables }) => {
  const mutation = actions.includes('reveal') ? REVEAL : PLACE
  const [runMutation, { loading }] = useMutation(mutation, { variables })

  if (loading) return <LargePlayingCard isLoading={loading} />

  return (
    <LargePlayingCard
      onClick={runMutation}
      label="Select this card to place here the card you just picked up. The card facing down will be discarded."
    />
  )
}

export default PlayerGamePanel
