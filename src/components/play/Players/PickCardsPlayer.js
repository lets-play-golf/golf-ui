import React, { useState } from 'react'
import LargePlayingCard from '../PlayingCards/LargePlayingCard'
import { usePickCards } from '../queries'
import { usePlayGameContext } from '../playGameContext'
import LargePlayer from './LargePlayer'

const PickCardsPlayer = ({ player }) => {
  const {
    game: { id },
  } = usePlayGameContext()
  const [pickCards] = usePickCards()
  const [pickedCards, setPickedCards] = useState(new Set())

  const handlePickCard = (spot) => {
    if (pickedCards.has(spot)) return
    if (pickedCards.size < 1)
      return setPickedCards((pickedCards) => new Set(pickedCards).add(spot))
    pickCards({
      variables: {
        game: id,
        spots: Array.from(new Set(pickedCards).add(spot)),
      },
    })
    setPickedCards((pickedCards) => new Set(pickedCards).add(spot))
  }

  const showLabel = pickedCards.size < 2

  return (
    <LargePlayer player={player}>
      {new Array(4).fill().map((itm, index) => (
        <LargePlayingCard
          isSelected={pickedCards.has(index)}
          onClick={() => handlePickCard(index)}
          key={index}
          label={
            showLabel &&
            !pickedCards.has(index) &&
            `Select this card to see it when the game starts`
          }
        />
      ))}
    </LargePlayer>
  )
}

export default PickCardsPlayer
