import React from 'react'
import styled from 'styled-components'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import FlexContentControl from 'shared/FlexContentControl'
import Card from 'shared/Card'

/**
 * Big player for `home` player
 */

const LargePlayer = ({ player, children }) => {
  const { username, totalScore } = player

  return (
    <PlayerBox className="noselect">
      <Col>
        <Row>
          <div style={{ margin: '0 .5rem .5rem 0 ' }}>{username}</div>
          <div>{totalScore}</div>
        </Row>
        <FlexContentControl>{children}</FlexContentControl>
      </Col>
    </PlayerBox>
  )
}

const PlayerBox = styled(Card)`
  padding: 1rem;
`

export default LargePlayer
