import React from 'react'
import PropTypes from 'prop-types'
import { getCard } from 'components/play/PlayingCards/cardHelpers'
import styled, { css } from 'styled-components'
import { glow, pulse } from 'app/animations'
import TooltipTrigger from 'shared/TooltipTrigger'
import FlexContentControl from 'shared/FlexContentControl'

/**
 * Primary UI component for user interaction
 */
const LargePlayingCard = ({
  id,
  isLoading = false,
  isSelected = false,
  score,
  label,
  onClick,
}) => {
  if (!Number.isInteger(id))
    return (
      <PlayingCardWrapper {...{ gradient: neutralGradient, onClick }}>
        {label && <ActionMarker {...{ label }} />}
        <FaceDown
          isLoading={isLoading}
          onClick={onClick}
          isSelected={isSelected}
        >
          {isSelected && <SelectedCardBackground />}
        </FaceDown>
      </PlayingCardWrapper>
    )

  const { rank, suit, suitCharacter } = getCard(id)

  const gradient = ['♥', '♦'].includes(suit) ? redGradient : blackGradient
  return (
    <PlayingCardWrapper {...{ gradient, onClick }}>
      {Number.isInteger(score) && (
        <Badge {...{ score }}>
          {score > 0 && '+'}
          {score}
        </Badge>
      )}
      {label && <ActionMarker {...{ label }} />}
      <FaceUp {...{ gradient, flow: 'column', onClick }}>
        <IconWrapper
          style={{ alignSelf: 'flex-end', marginRight: '.1rem' }}
          dangerouslySetInnerHTML={{ __html: suitCharacter }}
        />
        <RankWrapper>{rank}</RankWrapper>
        <IconWrapper
          style={{ alignSelf: 'flex-start', marginLeft: '.1rem' }}
          dangerouslySetInnerHTML={{ __html: suitCharacter }}
        />
      </FaceUp>
    </PlayingCardWrapper>
  )
}

const SelectedCardBackground = styled.div`
  background: ${(props) => props.theme.background80};
  height: 2.75rem;
`

const PlayingCardWrapper = ({ gradient, children }) => {
  return (
    <Wrapper {...{ gradient }}>
      <WhiteScreen>{children}</WhiteScreen>
    </Wrapper>
  )
}

const redGradient = `linear-gradient(to bottom left,hsl(12.8, 100%, 61.4%) 0%,hsl(347.1, 100%, 62.5%) 51%,hsl(306.2, 96.1%, 60.2%) 100%)`
const blackGradient = `linear-gradient(to top right,#6961ff 0%, #5ff6fe 80%)`
const neutralGradient = `linear-gradient(to bottom left, hsla(101, 100%, 68%, 1) 0%, hsla(182, 100%, 58%, 1) 100%);`

const WhiteScreen = styled.div`
  background: ${(props) => props.theme.background80};
  border-radius: 0.4rem;
  height: 100%;
`

const Wrapper = styled.div`
  ${(props) => props.onClick && 'cursor: pointer;'}
  position: relative;
  width: 3rem;
  height: 4rem;
  border-radius: 0.5rem;
  padding: 2px;
  background: ${(props) => props.gradient};
  -webkit-background-clip: padding-box;
  ${(props) => props.onClick && 'cursor: pointer;'}
`

const FaceDown = styled.div`
  height: 100%;
  width: 100%;
  background: ${neutralGradient};
  border: 0.5rem double ${(props) => props.theme.background80};
  border-radius: 0.4rem;

  ${(props) =>
    props.isLoading &&
    `
    animation: rotate 3s linear infinite;
    @keyframes rotate {
      100% {
        transform: rotate(1turn);
      }
    }
  `}
  ${(props) => props.onClick && 'cursor: pointer;'}
`

const FaceUp = styled(FlexContentControl)`
  background: ${(props) => props.gradient};
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  ${(props) => props.onClick && 'cursor: pointer;'}
`

const RankWrapper = styled.div`
  font-size: 2rem;
  line-height: 1.7rem;
  align-self: center;
  font-family: 'Calistoga', cursive;
`

const IconWrapper = styled.div`
  font-size: 1rem;
  line-height: 1rem;
  font-family: Arial;
`

const Badge = ({
  fontSizeInRem = 0.8,
  paddingInRem = 0.2,
  heightInRem = 1.2,
  score,
  children,
}) => {
  const totalSizeInRem = fontSizeInRem + paddingInRem * 2
  return (
    <BadgeUI
      {...{ fontSizeInRem, paddingInRem, totalSizeInRem, heightInRem, score }}
    >
      {children}
    </BadgeUI>
  )
}

const BadgeUI = styled.div`
  position: absolute;
  text-align: center;
  border-radius: ${(props) => props.totalSizeInRem / 2}rem;
  font-family: Roboto, sans-serif;
  background-color: ${(props) =>
    props.score > 0 ? props.theme.accent2 : props.theme.accent1};
  color: white;
  height: ${(props) => props.heightInRem}rem;
  font-size: ${(props) => props.fontSizeInRem}rem;
  line-height: ${(props) => props.fontSizeInRem}rem;
  top: -${(props) => props.totalSizeInRem / 2}rem;
  right: -${(props) => props.totalSizeInRem / 2}rem;
  padding: ${(props) => props.paddingInRem}rem;
  min-width: ${(props) => props.totalSizeInRem}rem;
`

const ActionMarker = ({ label }) => (
  <TooltipTrigger text={label} trigger={['focus', 'hover']}>
    <ActionMarkerUI />
  </TooltipTrigger>
)

const ActionMarkerUI = styled.div`
  position: absolute;
  top: -0.6rem;
  left: -0.6rem;
  height: 1.2rem;
  width: 1.2rem;
  border-radius: 0.6rem;
  background-color: #50fbbf;

  ${css`
    animation: ${pulse({ minScale: 0.6 })} 2s infinite,
      ${glow({ color: '#50fbbf' })} 2s infinite,
      ${pulse({ minScale: 0.6 })} 2s infinite;
  `}
`

LargePlayingCard.propTypes = {
  id: PropTypes.number,
}

export default LargePlayingCard
