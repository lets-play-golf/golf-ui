import { ClubsIcon, HeartsIcon, SpadesIcon, DiamondsIcon } from 'app/icons'

export const SUITS = ['♥', '♦', '♠', '♣']

export const ICONS = {
  '♥': HeartsIcon,
  '♦': DiamondsIcon,
  '♠': SpadesIcon,
  '♣': ClubsIcon,
}

export const RANKS = ['A', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K']

export const getColor = (suit) => (['♥', '♦'].includes(suit) ? 'light' : 'dark')

export const getCard = (id) => {
  const rank = RANKS[id % RANKS.length]
  const suit = SUITS[(id - (id % RANKS.length)) / 13]
  const Icon = ICONS[suit]
  const color = getColor(suit)
  const suitCharacter = {
    '♥': '&hearts;',
    '♦': '&diams;',
    '♠': '&spades;',
    '♣': '&clubs;',
  }[suit]
  return {
    id,
    suit,
    rank,
    Icon,
    color,
    suitCharacter,
  }
}

/**
 * @param {number|string} rank: the card rank
 * @returns card score for a given rank
 */
export const cardScore = (rank) =>
  rank === 'A' ? 1 : rank === 'K' ? 0 : ['Q', 'J'].includes(rank) ? 10 : +rank

/**
 * @returns the scores of all cards at the end of the round.
 * @param {*} cards: cards in order of their spot
 */
export const roundCardScores = (cards) => {
  cards = cards.map(getCard)
  const counts = cards.reduce((acc, { rank }) => {
    if (acc[rank]) acc[rank] = acc[rank] + 1
    else acc[rank] = 1
    return acc
  }, {})
  return cards.map(({ rank }) => {
    if ([4].includes(counts[rank])) return -5
    if ([3].includes(counts[rank])) {
      counts[rank] = 2
      return cardScore(rank)
    }
    if ([1].includes(counts[rank])) return cardScore(rank)
    return 0
  })
}

export const newDeck = () => new Array(52).fill(0).map((v, i) => i)
export const random = () => Math.random() - 0.5
export const newShuffledDeck = () => newDeck().sort(random).sort(random)
