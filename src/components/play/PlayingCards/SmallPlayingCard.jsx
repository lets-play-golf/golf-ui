import React from 'react'
import PropTypes from 'prop-types'
import { getCard } from 'components/play/PlayingCards/cardHelpers'
import styled from 'styled-components'
import FlexContentControl from 'shared/FlexContentControl'

const SmallPlayingCard = ({ id, score }) => {
  if (!Number.isInteger(id))
    return (
      <PlayingCardWrapper {...{ gradient: neutralGradient }}>
        <FaceDown />
      </PlayingCardWrapper>
    )

  const { rank, suit } = getCard(id)

  const gradient = ['♥', '♦'].includes(suit) ? redGradient : blackGradient
  return (
    <PlayingCardWrapper {...{ gradient }}>
      {Number.isInteger(score) && (
        <Badge {...{ score }}>
          {score > 0 && '+'}
          {score}
        </Badge>
      )}
      <FaceUp {...{ gradient, flow: 'column' }}>
        <RankWrapper>{rank}</RankWrapper>
      </FaceUp>
    </PlayingCardWrapper>
  )
}

const PlayingCardWrapper = ({ gradient, children }) => (
  <Wrapper {...{ gradient }}>
    <WhiteScreen>{children}</WhiteScreen>
  </Wrapper>
)

const softRedGradient = `linear-gradient(to bottom left, hsla(13, 88%, 78%, 1) 0%, hsla(347, 100%, 76%, 1) 51%, hsla(306, 100%, 79%, 1) 100%)`
const softBlackGradient = `linear-gradient(to bottom left, hsla(198, 93%, 71%, 1) 0%, hsla(216, 100%, 75%, 1) 50%, hsla(255, 100%, 81%, 1) 100%)`

const redGradient = `linear-gradient(to bottom left,hsl(12.8, 100%, 61.4%) 0%,hsl(347.1, 100%, 62.5%) 51%,hsl(306.2, 96.1%, 60.2%) 100%)`
const blackGradient = `linear-gradient(to top right,#6961ff 0%, #5ff6fe 80%)`
const neutralGradient = `linear-gradient(to bottom left, hsla(101, 100%, 68%, 1) 0%, hsla(182, 100%, 58%, 1) 100%);`

const WhiteScreen = styled.div`
  background: ${(props) => props.theme.background80};
  border-radius: 0.4rem;
  height: 100%;
`

const Wrapper = styled.div`
  position: relative;
  width: 1.5rem;
  height: 1.5rem;
  border-radius: 0.5rem;
  padding: 2px;
  background: ${(props) => props.gradient};
  -webkit-background-clip: padding-box;
`

const FaceUp = styled(FlexContentControl)`
  background: ${(props) => props.gradient};
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
`

const RankWrapper = styled.div`
  font-size: 1rem;
  line-height: 1rem;
  align-self: center;
  font-family: 'Calistoga', cursive;
`

const FaceDown = styled.div`
  height: 100%;
  width: 100%;
  background: ${neutralGradient};
  border: 0.5rem double ${(props) => props.theme.background80};
  border-radius: 0.4rem;
`

const Badge = ({
  fontSizeInRem = 0.6,
  paddingInRem = 0.1,
  heightInRem = 0.8,
  score,
  children,
}) => {
  const totalSizeInRem = fontSizeInRem + paddingInRem * 2
  return (
    <BadgeUI
      {...{ fontSizeInRem, paddingInRem, totalSizeInRem, heightInRem, score }}
    >
      {children}
    </BadgeUI>
  )
}

const BadgeUI = styled.div`
  position: absolute;
  text-align: center;
  border-radius: ${(props) => props.totalSizeInRem / 2}rem;
  font-family: Roboto, sans-serif;
  background-color: ${(props) =>
    props.score > 0 ? props.theme.accent2 : props.theme.accent1};
  color: white;
  height: ${(props) => props.heightInRem}rem;
  font-size: ${(props) => props.fontSizeInRem}rem;
  line-height: ${(props) => props.fontSizeInRem}rem;
  top: -${(props) => props.totalSizeInRem / 2}rem;
  right: -${(props) => props.totalSizeInRem / 2}rem;
  padding: ${(props) => props.paddingInRem}rem;
  min-width: ${(props) => props.totalSizeInRem}rem;
`

SmallPlayingCard.propTypes = {
  id: PropTypes.number,
}

export default SmallPlayingCard
