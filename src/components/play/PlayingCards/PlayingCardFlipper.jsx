import React from 'react'
import ReactCardFlip from 'react-card-flip'

export const PlayingCardFlipper = ({ from, to, delay = 0 }) => {
  const [isActive, setIsActive] = React.useState(false)

  setTimeout(() => setIsActive(true), delay)

  return (
    <ReactCardFlip isFlipped={isActive} flipDirection="vertical">
      <div>{from}</div>

      <div>{to}</div>
    </ReactCardFlip>
  )
}
