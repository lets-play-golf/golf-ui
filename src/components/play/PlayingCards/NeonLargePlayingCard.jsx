import React from 'react'
import { getCard } from 'components/play/PlayingCards/cardHelpers'
import styled from 'styled-components'

const NeonLargePlayingCard = ({ id }) => {
  if (!Number.isInteger(id)) return <Wrapper></Wrapper>

  const { rank } = getCard(id)

  return (
    <Wrapper>
      <h1 class="neonText">
        <div>{rank}</div>
      </h1>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  
  .neonText {
    color: #fff;
    font-family: "Neonderthaw", sans-serif;
    text-align: center;
    font-size: 6.2rem;
    font-weight: 400;
    background-color: #00000000
    animation: pulsate 1.5s infinite alternate;
    text-shadow:
      0 0 7px #fff,
      0 0 10px #fff,
      0 0 21px #fff,
      0 0 42px #bc13fe,
      0 0 82px #bc13fe,
      0 0 92px #bc13fe,
      0 0 102px #bc13fe,
      0 0 151px #bc13fe;
  }
  
h1 {

  border: 0.2rem solid #fff;
  background-color: #010a01;
  border-radius: 2rem;
  padding: 0.4em;
  box-shadow: 0 0 .2rem #fff,
            0 0 .2rem #fff,
            0 0 2rem #bc13fe,
            0 0 0.8rem #bc13fe,
            0 0 2.8rem #bc13fe,
            inset 0 0 1.3rem #bc13fe; 
}

@keyframes pulsate {
    
  100% {

      text-shadow:
      0 0 4px #fff,
      0 0 11px #fff,
      0 0 19px #fff,
      0 0 40px #bc13fe,
      0 0 80px #bc13fe,
      0 0 90px #bc13fe,
      0 0 100px #bc13fe,
      0 0 150px #bc13fe;
  
  }
  
  0% {

    text-shadow:
    0 0 2px #fff,
    0 0 4px #fff,
    0 0 6px #fff,
    0 0 10px #bc13fe,
    0 0 45px #bc13fe,
    0 0 55px #bc13fe,
    0 0 70px #bc13fe,
    0 0 80px #bc13fe;

}
`

export default NeonLargePlayingCard
