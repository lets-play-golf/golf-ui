import React from 'react'
import styled from 'styled-components'
import SmallPlayingCard from '../PlayingCards/SmallPlayingCard'
import Card from 'shared/Card'
import * as EvenlySpaced from 'shared/content-control/EvenlySpaced'

const Wrapper = styled(Card)`
  color: white;
  border-radius: 0.4rem;
  padding: 0.6rem;
  text-align: center;
`

const SmallDecks = ({ discardCard }) => (
  <Wrapper>
    <EvenlySpaced.Column style={{ gap: '.5rem' }}>
      <EvenlySpaced.Row style={{ gap: '.4rem' }}>
        <SmallPlayingCard />
        <SmallPlayingCard id={discardCard} />
      </EvenlySpaced.Row>
    </EvenlySpaced.Column>
    <div>
      <span>Decks</span>
    </div>
  </Wrapper>
)

export default SmallDecks
