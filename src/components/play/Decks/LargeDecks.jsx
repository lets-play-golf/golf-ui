import React from 'react'
import styled from 'styled-components'
import Row from 'react-bootstrap/Row'
import Card from 'shared/Card'
import LargePlayingCard from '../PlayingCards/LargePlayingCard'
import { usePickup, useDiscard } from '../queries'
import FlexContentControl from 'shared/FlexContentControl'
import { getCard } from 'components/play/PlayingCards/cardHelpers'

const LargeDecks = ({
  startedGame: {
    players: { home },
    discardCard,
    pickedUpCard,
    nextTurn,
  },
  startedGame,
}) => {
  const actionLabels = {
    pickup: 'Pick up a card',
    placediscard: 'Place or discard',
    reveal: 'Reveal a card',
  }

  const userLabel = home.actions.length ? 'your' : `${nextTurn.players[0]}'s`
  const headerLabel = home.actions.length > 0 && `It's ${userLabel} turn!`
  const footerLabel = (() => {
    if (!Number.isInteger(pickedUpCard))
      return home.actions.length
        ? actionLabels[home.actions.join('')]
        : `It's ${userLabel} turn`
    const nextTurnLabel = home.actions.length ? 'you' : nextTurn.players[0]
    const pickedUpCardRank = getCard(pickedUpCard).rank
    return `${nextTurnLabel} picked up a${
      pickedUpCardRank === 'A' ? 'n' : ''
    } ${pickedUpCardRank}`
  })()

  return (
    <DecksWrapper {...{ headerLabel, footerLabel }}>
      {(() => {
        if (home.actions.includes('pickup'))
          return <PickupDecksCards {...{ startedGame, discardCard }} />

        if (Number.isInteger(pickedUpCard))
          return (
            <PickedUpCardDecksCards
              {...{ startedGame, discardCard, pickedUpCard }}
            />
          )

        return (
          <InactiveDecksCards {...{ headerLabel, discardCard, footerLabel }} />
        )
      })()}
    </DecksWrapper>
  )
}

const PickupDecksCards = ({
  startedGame: {
    id: game,
    players: {
      home: { actions },
    },
    discardCard,
  },
}) => {
  const [pickupPileAction] = usePickup({ variables: { game, pile: 'PICKUP' } })
  const [pickupDiscardCardAction] = usePickup({
    variables: { game, pile: 'DISCARD' },
  })

  const card = getCard(discardCard)
  return (
    <>
      <LargePlayingCard
        onClick={pickupPileAction}
        label={
          actions.includes('pickup') &&
          'Select this card to pick up a card from the face down deck'
        }
      />
      <LargePlayingCard
        id={discardCard}
        onClick={pickupDiscardCardAction}
        label={
          actions.includes('pickup') &&
          `Select this card to pick up a ${card.rank} of ${card.suit}`
        }
      />
    </>
  )
}

const PickedUpCardDecksCards = ({
  startedGame: {
    id: game,
    players: {
      home: { actions },
    },
    pickedUpCard,
  },
}) => {
  const [discardCardAction] = useDiscard({ variables: { game } })
  return (
    <>
      <LargePlayingCard id={pickedUpCard} />
      {actions.includes('discard') && (
        <LargePlayingCard
          onClick={discardCardAction}
          label={'Select this card to discard the card you picked up'}
        />
      )}
    </>
  )
}

const InactiveDecksCards = ({ discardCard }) => {
  return (
    <>
      <LargePlayingCard />
      <LargePlayingCard id={discardCard} />
    </>
  )
}

const Wrapper = styled(Card)`
  gap: 1rem;
  padding: 1rem;
  width: 15rem;
  margin: 0 auto;
  text-align: center;
`

export const DecksWrapper = ({ headerLabel, footerLabel, children }) => {
  return (
    <Wrapper>
      <FlexContentControl flow="column" style={{ gap: '1rem' }}>
        {headerLabel && <h1 mb={0}>It's your turn</h1>}
        <FlexContentControl
          flow="row"
          style={{ gap: '1rem', justifyContent: 'center' }}
        >
          {children}
        </FlexContentControl>
        {footerLabel && (
          <Row style={{ justifyContent: 'center' }}>{footerLabel}</Row>
        )}
      </FlexContentControl>
    </Wrapper>
  )
}

export default LargeDecks
