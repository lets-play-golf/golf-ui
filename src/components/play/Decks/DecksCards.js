import React from 'react'
import LargePlayingCard from '../PlayingCards/LargePlayingCard'
import { usePickup, useDiscard } from '../queries'

const DecksCards = ({
  startedGame: {
    id: game,
    players: { home: actions },
    discardCard,
  },
}) => {
  const [pickupPileAction] = usePickup({ variables: { game, pile: 'PICKUP' } })
  const [pickupDiscardCardAction] = usePickup({
    variables: { game, pile: 'DISCARD' },
  })
  const [discardCardAction] = useDiscard({ variables: { game } })
  const discardPileAction = actions.includes('pickup')
    ? pickupDiscardCardAction
    : actions.includes('discard')
    ? discardCardAction
    : null
  return (
    <>
      <LargePlayingCard
        onClick={actions.includes('pickup') ? pickupPileAction : undefined}
      />
      <LargePlayingCard id={discardCard} onClick={discardPileAction} />
    </>
  )
}

export default DecksCards
