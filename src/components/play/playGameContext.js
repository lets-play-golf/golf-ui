import React from 'react'
import { useSessionContext } from 'components/sessions/sessionContext'

export const PlayGameContext = React.createContext()

/**
 * Exposes a context containing:
 * - game: basic game information without current turn and hands
 * - startedGame: game information along with current turn and hands
 */

export function PlayGameContextProvider(props) {
  const [game, setGame] = React.useState(props?.value?.game)
  // Current user
  const { session } = useSessionContext()

  if (!game || !session)
    return <PlayGameContext.Provider {...props} value={{ setGame }} />

  if (['created'].includes(game.status))
    return <PlayGameContext.Provider {...props} value={{ game, setGame }} />

  const homeUser = game.players.find((itm) => itm.userId === session.user.id)
    .user.id
  const players = getPlayers({ game, homeUser })

  // TODO: Destructure game and startedGame so it errors here
  const value = {
    game,
    startedGame: {
      ...game,
      players,
      nextTurn: {
        ...game.golf.nextTurn,
        // Swapping user id for the username so it is printed in the UI
        // TODO: Clean this up
        players: game.golf.nextTurn.players.map(
          (player) =>
            game.players.find(({ user }) => user.id === player).user.username
        ),
      },
      pickedUpCard: getPickedUpCard(players),
      discardCard: game.golf.publicView.discardPileCard,
    },
    setGame,
  }

  return <PlayGameContext.Provider {...props} value={value} />
}

const getPickedUpCard = (players) => {
  const hasPickedUpCard = players.away
    .concat(players.home)
    .find((itm) => !!Number.isInteger(itm.pickedUpCard))
  return hasPickedUpCard ? hasPickedUpCard.pickedUpCard : null
}

/**
 * Splits players between home (the currently logged in player) and away (other users) based on the homeUser
 * @param {object} game: the current game
 * @param {string} homeUser: the user id for the home player
 */
const getPlayers = ({
  game: {
    players,
    golf: { publicView, nextTurn = { players: [], actions: [] } },
  },
  homeUser,
}) => {
  const gameScoresAsc = players
    .map(({ score }) => score.reduce((acc, itm) => itm + acc, 0))
    .sort((a, b) => a - b)
  const { 0: topGameScore, [gameScoresAsc.length - 1]: bottomGameScore } =
    gameScoresAsc

  const roundScoresAsc = players
    .map(({ score }) => score.slice(-1)[0])
    .sort((a, b) => a - b)
  const { 0: topRoundScore } = roundScoresAsc
  return players.reduce(
    (acc, { score, user: { id, username } }) => {
      const totalScore = score.reduce((acc, itm) => acc + itm, 0)
      const isHomeUser = homeUser === id
      const usernameLabel = isHomeUser ? `${username} (you)` : username
      const player = {
        id,
        isGameWinner: totalScore === topGameScore,
        isGameLoser: totalScore === bottomGameScore,
        isRoundWinner: score.slice(-1)[0] === topRoundScore,
        visibleCards: publicView.players.find((itm) => id === itm.id).cards,
        pickedUpCard: publicView.players.find((itm) => id === itm.id)
          .pickedUpCard,
        username: usernameLabel,
        totalScore,
        score,
        actions:
          nextTurn && nextTurn.players.includes(id) ? nextTurn.actions : [],
      }
      if (isHomeUser) return { ...acc, home: player }
      return { ...acc, away: acc.away.concat(player) }
    },
    { away: [] }
  )
}

export const usePlayGameContext = () => {
  return React.useContext(PlayGameContext)
}
