import { useMutation, useSubscription } from '@apollo/react-hooks'
import gql from 'graphql-tag'
import { useLoggedInQuery } from 'app/useGraphql'

const reloadOnError = (error) => {
  console.log(`error handled by useReloadOnErrorQuery:`, error.message)
  window.location.reload()
}

/**
 * Mutation error might occurr in the playGame flow:
 * - When the UI missing a subscription update and the user is playing when it is not their turn
 * - When the user is logged out
 * Reloading the UI will solve both problem
 */
const useReloadOnErrorMutation = (mutation, options) => {
  return useMutation(mutation, {
    ...options,
    onError: reloadOnError,
  })
}

/**
 * TODO: Split Chat, Game and PlayGame queries
 */

const GAME_AND_CHAT = `
  id
  numberOfRounds
  players {
    userId
    user {
      id
      username
    }
    score
    isOnline
  }
  status
  golf {
    publicView {
      players {
        pickedUpCard
        cards
        id
      }
      discardPileCard
    }
    nextTurn {
      players
      actions
    }
  }
  createdAt
  
  chat {
    id
    users {
      id
      username
    }
    messages {
      from
      message
      sentAt
    }
  }
`

const TYPE_GAME_IGNORE_RESPONSE = '{ id }'

const GAME_QUERY = gql`
  query game ($id: ID!) {
    game (id: $id) { ${GAME_AND_CHAT} }
  }
`
export const useGameQuery = (variables) =>
  useLoggedInQuery(GAME_QUERY, { variables, fetchPolicy: 'network-only' })

const SUBSCRIBE_GAME_UPDATES = gql`
  subscription gameUpdated($id: ID!, $skipFetchingUser: Boolean!) {
    gameUpdate: gameUpdated(id: $id) {
      ... on Game {
        id
        numberOfRounds
        players {
          userId
          score
          isOnline
          user @skip(if: $skipFetchingUser) {
            id
            username
          }
        }
        nextTurn {
          players
          actions
        }
        status
        golf {
          nextTurn {
            players
            actions
          }
          publicView {
            players {
              pickedUpCard
              cards
              id
            }
            discardPileCard
          }
        }
        createdAt
      }
      ... on PeekCards {
        spots
      }
    }
  }
`
export const useSubscribeGame = (options) =>
  useSubscription(SUBSCRIBE_GAME_UPDATES, options)

const START = gql`
  mutation start($game: ID!) {
    start(game: $game) ${TYPE_GAME_IGNORE_RESPONSE}
  }
`
export const useStart = (options) => useReloadOnErrorMutation(START, options)

const PICK_CARDS = gql`
  mutation pick_cards($game: ID! $spots: [Int!]!) {
    pick_cards(game: $game spots: $spots) ${TYPE_GAME_IGNORE_RESPONSE}
  }
`
export const usePickCards = (options) =>
  useReloadOnErrorMutation(PICK_CARDS, options)

const PICKUP = gql`
  mutation pickup($game: ID!, $pile: Pile!) {
    pickup(game: $game, pile: $pile) ${TYPE_GAME_IGNORE_RESPONSE}
  }
`
export const usePickup = (options) => useReloadOnErrorMutation(PICKUP, options)

const DISCARD = gql`
  mutation discard($game: ID!) {
    discard(game: $game) ${TYPE_GAME_IGNORE_RESPONSE}
  }
`

export const useDiscard = (options) =>
  useReloadOnErrorMutation(DISCARD, options)

export const REVEAL = gql`
  mutation reveal($game: ID! $spot: Int!) {
    reveal(game: $game spot: $spot) ${TYPE_GAME_IGNORE_RESPONSE}
  }
`

export const PLACE = gql`
  mutation place($game: ID! $spot:Int!){
    place(game:$game spot:$spot) ${TYPE_GAME_IGNORE_RESPONSE}
  }
`

const ADD_A_BOT = gql`
  mutation addABot($id: ID!, $bot: ID!) {
    updatedGame: addABot(id: $id, bot: $bot) {
      players {
        userId
      }
    }
  }
`
export const useAddABot = (options) =>
  useReloadOnErrorMutation(ADD_A_BOT, options)

const START_GAME = gql`
  mutation startGame($id: ID!) {
    startedGame: startGame(id: $id) {
      id
      status
    }
  }
`
export const useStartGame = (options) =>
  useReloadOnErrorMutation(START_GAME, options)
