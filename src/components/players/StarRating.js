import React from 'react'
import styled from 'styled-components'
import Stars from 'react-stars'

const Rating = styled(Stars)`
  display: inline-block;
  line-height: 1rem;
`

const StarRating = ({ rating }) => (
  <Rating edit={false} value={rating} count={5} size={16} />
)

export default StarRating
