import { useQuery } from '@apollo/react-hooks'
import gql from 'graphql-tag'
import { useLoggedInQuery } from 'app/useGraphql'

const GET_PLAYERS = gql`
  query {
    players {
      id
      username
      createdAt
      badges
      stats {
        rating
        averageScore
        numberOfGames
        numberOfRounds
        rank
      }
    }
  }
`
export const useGetPlayers = () =>
  useQuery(GET_PLAYERS, { fetchPolicy: 'network-only' })

const PLAYER = gql`
  query player($player: ID!) {
    player(id: $player) {
      username
      badges
      stats {
        rating
        averageScore
        numberOfGames
        numberOfRounds
        rank
      }
    }
  }
`

export const usePlayerQuery = (variables) =>
  useLoggedInQuery(PLAYER, { variables, fetchPolicy: 'network-only' })
