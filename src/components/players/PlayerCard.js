import React from 'react'
import styled, { css } from 'styled-components'
import Card from 'shared/Card'
import StarRating from './StarRating'
import Overlay from 'react-bootstrap/overlay'
import { pulse, glow } from 'app/animations'
import TooltipTrigger from 'shared/TooltipTrigger'
import {
  FirstPlaceIcon,
  SecondPlaceIcon,
  ThirdPlaceIcon,
  BabyIcon,
  HeartIcon,
} from 'app/icons'

const Wrapper = styled(Card)`
  width: 18rem;

  ${(props) =>
    props.isAnimated &&
    css`
      animation: ${pulse({ minScale: 0.9 })} 2s infinite,
        ${glow({ color: props.theme.background100 })} 2s infinite;
    `}
`

const PlayerCard = ({ ...props }) => {
  const {
    username,
    stats: { rating },
    badges = [],
    isCurrentPlayer,
  } = props
  return (
    <Wrapper
      id={`player-${username}`}
      isAnimated={!!isCurrentPlayer}
      className={'mt-2 p-0'}
    >
      <Card.Body style={{ padding: '.5rem' }}>
        <Card.Text>
          <span>{username}</span>
          <span>
            {' '}
            {badges.map((itm) => (
              <Badge key={itm} name={itm} />
            ))}
          </span>
          <span style={{ float: 'right' }}>
            <StarRating {...{ rating }} /> {rating.toFixed(2)}
          </span>
        </Card.Text>
      </Card.Body>
    </Wrapper>
  )
}

const BadgeWrapper = styled.span`
  margin-right: 5px;
`

const Badge = ({ name }) => {
  const Icon = {
    isFirst: FirstPlaceIcon,
    isSecond: SecondPlaceIcon,
    isThird: ThirdPlaceIcon,
    newestPlayer: BabyIcon,
    largestNumberOfGames: HeartIcon,
  }[name]

  const text = {
    isFirst: 'Best player',
    isSecond: 'Second best player',
    isThird: 'Third best player',
    newestPlayer: 'Newest member',
    largestNumberOfGames: 'Most games played',
  }[name]

  if (!Icon) return <></>
  return (
    <BadgeWrapper>
      <TooltipTrigger text={text} trigger={['focus', 'hover']}>
        <a style={{ cursor: 'pointer' }}>
          <Icon />
        </a>
      </TooltipTrigger>
    </BadgeWrapper>
  )
}

export const TooltipHandler = ({ children }) => {
  const [show, setShow] = React.useState(false)
  const target = React.useRef(null)
  return (
    <span onClick={() => setShow(true)}>
      {children}
      <Overlay target={target.current} show={show} placement="right"></Overlay>
    </span>
  )
}
export default PlayerCard
