import React from 'react'
import { useSessionContext } from 'components/sessions/sessionContext'
import MainLayout from 'app/MainLayout'
import { usePlayerQuery } from './queries'
import { Redirect } from '@reach/router'
import PlayerCard from './PlayerCard'
import DisplayUser from '../users/DisplayUser'
import Card from 'shared/Card'
import Accordion from 'react-bootstrap/Accordion'
import Button from 'react-bootstrap/Button'
import { Link } from '@reach/router'
import { PrimaryButton } from 'shared/Buttons'

const PlayerDisplayPage = () => {
  const { session, handleExpiredSession } = useSessionContext()
  const res = usePlayerQuery({ player: session?.user?.id })
  const { data, error } = res
  if (!session) return <Redirect to="/login" noThrow />
  if (error) handleExpiredSession(error)
  if (!data) return <></>

  const { stats } = data.player
  const { averageScore, numberOfGames, numberOfRounds } = stats

  return (
    <MainLayout>
      <h1>Hello {session.user.firstname}</h1>
      {data?.player?.stats?.rating && <PlayerCard {...{ ...data.player }} />}
      <p className="mt-3">Here is all your account information</p>
      <Accordion>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="0">
              Your Rating
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              {!data?.player?.stats?.rating ? (
                <>
                  To get a rating and appear on the leaderboard, you have to
                  complete 3 games
                </>
              ) : (
                <PlayerCard {...{ ...data.player }} />
              )}
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="1">
              Your Stats
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="1">
            <Card.Body>
              {!data?.player?.stats?.rating ? (
                <>
                  To get stats and appear on the leaderboard, you have to
                  complete 3 games
                </>
              ) : (
                <ul className="mt-0 mb-0">
                  <li>Completed games: {numberOfGames}</li>
                  <li>Completed rounds: {numberOfRounds}</li>
                  <li>Average score per round: {averageScore}</li>
                </ul>
              )}
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="2">
              Your Account Details
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="2">
            <Card.Body>
              <DisplayUser />
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
      <br />
      <Link to="/update-account" className={'mt-3'}>
        <PrimaryButton>Change your account details</PrimaryButton>
      </Link>
    </MainLayout>
  )
}
export default PlayerDisplayPage
