import React from 'react'
import MainLayout from 'app/MainLayout'
import { useGetPlayers } from './queries'
import PlayerCard from './PlayerCard'
import { useSessionContext } from 'components/sessions/sessionContext'

const LeaderboardPage = () => {
  const { session } = useSessionContext()
  const { data, error } = useGetPlayers()
  if (!data) return <></>
  if (error)
    return (
      <>
        Something went wrong, please contact me at bpjperrier@gmail.com to let
        me know
      </>
    )

  const players = data.players.map((itm) => ({
    ...itm,
    createdAt: new Date(itm.createdAt),
  }))

  const currentPlayer = session?.user?.id

  return (
    <MainLayout>
      <h1>Leaderboard</h1>
      <p>Are you on top of it?</p>
      {players.map((player) => (
        <PlayerCard
          key={player.username}
          {...{
            isCurrentPlayer: currentPlayer === player.id,
            ...player,
          }}
        />
      ))}
      <br />
      <p>Not on there? Complete 3 games to get on the board!</p>
    </MainLayout>
  )
}
export default LeaderboardPage
