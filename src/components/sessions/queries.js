import { useMutation, useLazyQuery } from '@apollo/react-hooks'
import gql from 'graphql-tag'

const CREATE_SESSION = gql`
  mutation createSession($email: String!, $password: String!) {
    newSession: createSession(email: $email, password: $password) {
      ... on Session {
        user {
          id
          firstname
          username
          email
        }
      }
      ... on UserInputError {
        error {
          key
          fields
          message
          data
        }
      }
    }
  }
`
export const useCreateSession = (options) =>
  useMutation(CREATE_SESSION, options)

const DELETE_SESSION = gql`
  mutation {
    deleteSession
  }
`
export const useDeleteSession = () => useMutation(DELETE_SESSION)

const REQUEST_MAGIC_LINK = gql`
  mutation requestMagicLink($email: String!) {
    requestMagicLink(email: $email)
  }
`
export const useRequestMagicLink = () => useMutation(REQUEST_MAGIC_LINK)

const CHECK_MAGIC_LINK = gql`
  mutation checkMagicLink($hash: String!) {
    checkMagicLink(hash: $hash)
  }
`
export const useCheckMagicLink = (variables) =>
  useMutation(CHECK_MAGIC_LINK, { variables })

const CONSUME_MAGIC_LINK = gql`
  mutation consumeMagicLink($hash: String!) {
    consumeMagicLink(hash: $hash) {
      ... on Session {
        user {
          id
          firstname
          email
          username
        }
      }
      ... on UserInputError {
        error {
          message
          key
          fields
        }
      }
    }
  }
`
export const useConsumeMagicLink = (variables) =>
  useMutation(CONSUME_MAGIC_LINK, { variables })

const SESSION_QUERY = gql`
  query {
    session {
      user {
        id
        firstname
        username
        email
      }
    }
  }
`
export const useLazySessionQuery = () =>
  useLazyQuery(SESSION_QUERY, { fetchPolicy: 'network-only' })
