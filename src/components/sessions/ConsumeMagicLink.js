import React from 'react'
import { useCheckMagicLink, useConsumeMagicLink } from './queries'
import MainLayout from 'app/MainLayout'
import { PrimaryButton } from 'shared/Buttons'
import { useSessionContext } from './sessionContext'
import { navigate } from '@reach/router'

const ConsumeMagicLink = ({ hash }) => {
  const [checkMagicLink, { loading, error, data }] = useCheckMagicLink({
    hash,
  })

  React.useEffect(() => {
    checkMagicLink()
  }, [checkMagicLink])

  if (loading || !(data || error)) return <></>

  const { checkMagicLink: expiryInMinutes } = data || {}

  return (
    <MainLayout>
      {!error && expiryInMinutes > 0 ? (
        <ValidLink expiryInMinutes={expiryInMinutes} hash={hash} />
      ) : (
        <ExpiredLink />
      )}
    </MainLayout>
  )
}

const ValidLink = ({ expiryInMinutes, hash }) => {
  const [isExpired, setIsExpired] = React.useState()

  const { persistSession } = useSessionContext()
  const [loginWithMagicLink] = useConsumeMagicLink({ hash })

  const onClick = async () => {
    const resp = await loginWithMagicLink()
    const { data, error } = resp
    if (error || data.consumeMagicLink.error) return setIsExpired(true)
    persistSession(data.consumeMagicLink)
    return navigate('/')
  }

  if (isExpired) return <ExpiredLink />

  return (
    <>
      <h1>Log in with a magic link</h1>
      <p className="mt-2">
        Your magic link expires in {expiryInMinutes} minutes.
      </p>
      <p>To use your magic link, click on the button below.</p>
      <PrimaryButton onClick={onClick}>Login to your account now</PrimaryButton>
    </>
  )
}

const ExpiredLink = () => (
  <>
    <h1>Your link has expired</h1>
    <p>
      You can request a new one on our <a href="/login">login page</a>
    </p>
  </>
)
export default ConsumeMagicLink
