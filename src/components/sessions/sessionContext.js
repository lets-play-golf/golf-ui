import React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'

const query = gql`
  query {
    session {
      user {
        id
        firstname
      }
    }
  }
`

export const SessionContext = React.createContext()

export function SessionContextProvider(props) {
  const currentSession = localStorage.getItem('session')
  const [session, setSession] = React.useState(JSON.parse(currentSession))

  const persistSession = (newSession) => {
    localStorage.setItem('session', JSON.stringify(newSession))
    setSession(newSession)
  }

  const deleteSession = () => {
    localStorage.clear()
    setSession(null)
  }

  const handleExpiredSession = (error) => {
    if (
      error?.networkError?.result?.errors[0]?.extensions?.code ===
      'UNAUTHENTICATED'
    )
      deleteSession()
  }

  const useChallengeSession = () => {
    // If session is set, get backend to decode token and respond with current session
    const { error } = useQuery(query, {
      fetchPolicy: 'network-only',
      skip: !currentSession,
    })
    // If backend cannot decode the token, reset the session to null
    if (error) {
      console.log('Token out of date, resetting session')
      deleteSession()
    }
  }

  useChallengeSession()
  const value = {
    session,
    persistSession,
    deleteSession,
    useChallengeSession,
    handleExpiredSession,
  }
  return <SessionContext.Provider {...props} value={value} />
}

export const useSessionContext = () => React.useContext(SessionContext)
