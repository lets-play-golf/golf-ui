import React from 'react'
import { useCreateSession, useRequestMagicLink } from '../queries'
import { useForm } from 'react-hook-form'
import { Link, navigate } from '@reach/router'
import { useSessionContext } from '../sessionContext'
import { Button, PrimaryButton } from 'shared/Buttons'
import { resolveBackendError } from '../../errors'
import MainLayout from '../../../app/MainLayout'
import Form from 'react-bootstrap/Form'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import FormControl from 'shared/FormControl'
import Card from 'shared/Card'

const Login = () => {
  const [withPassword, setWithPassword] = React.useState(true)
  return (
    <MainLayout>
      <h1>Login</h1>
      <p>
        Dont have an account? <Link to="/register">register</Link>
      </p>
      <ButtonGroup aria-label="Login mean" className="mb-3">
        <Button isPrimary={withPassword} onClick={() => setWithPassword(true)}>
          Password
        </Button>
        <Button
          isPrimary={!withPassword}
          onClick={() => setWithPassword(false)}
        >
          Magic link
        </Button>
      </ButtonGroup>
      {withPassword ? <CreateSession /> : <RequestMagicLink />}
    </MainLayout>
  )
}

const CreateSession = () => {
  const { persistSession } = useSessionContext()
  const [createSession] = useCreateSession()
  const {
    formState: { isSubmitting },
    handleSubmit,
    register,
    errors,
    setError,
  } = useForm({ submitFocusError: true })

  const onSubmit = handleSubmit(async ({ email, password }) => {
    try {
      const {
        data: {
          newSession: { error, ...session },
        },
      } = await createSession({ variables: { email, password } })
      if (!error) {
        persistSession(session)
        return navigate('/')
      }
      const { field, key, message } = resolveBackendError(error)
      return setError(field, key, message)
    } catch (e) {
      console.error(`CreateSession submit error: ${e}`)
    }
  })
  return (
    <Form onSubmit={onSubmit}>
      <Form.Group controlId="validationEmail">
        <Form.Label>Email</Form.Label>
        <FormControl
          name="email"
          type="email"
          placeholder="Enter email"
          disabled={isSubmitting}
          isInvalid={!!errors.email}
          ref={register({
            validate: (value) =>
              !value
                ? 'required'
                : value.length < 10
                ? 'Email is too short'
                : true,
          })}
        />
        <Form.Control.Feedback type="invalid">
          {errors.email && errors.email.message}
        </Form.Control.Feedback>
      </Form.Group>

      <Form.Group controlId="validationPassword">
        <Form.Label>
          Password (
          <Link to="/password-reset" tabIndex="-1">
            forgot password?
          </Link>
          )
        </Form.Label>
        <FormControl
          name="password"
          type="password"
          placeholder="Password"
          disabled={isSubmitting}
          isInvalid={!!errors.password}
          ref={register({
            validate: (value) =>
              !value
                ? 'required'
                : value.length < 6
                ? 'Password is too short'
                : true,
          })}
        />
        <Form.Control.Feedback type="invalid">
          {errors.password && errors.password.message}
        </Form.Control.Feedback>
      </Form.Group>

      {errors.form && (
        <Form.Text className="text-muted">{errors.form.message}</Form.Text>
      )}

      <PrimaryButton disabled={isSubmitting} type="submit">
        Log in
      </PrimaryButton>
    </Form>
  )
}

const RequestMagicLink = () => {
  const [isLinkSent, setIsLinkSent] = React.useState(false)
  const [requestMagicLink] = useRequestMagicLink()

  const {
    formState: { isSubmitting },
    handleSubmit,
    register,
    errors,
  } = useForm({ submitFocusError: true })

  if (isLinkSent)
    return (
      <Card>
        <Card.Body>
          <Card.Title>Check your inbox!</Card.Title>
          <Card.Text>
            <ul>
              <li>
                If you are registered, you will receive your magic link by
                email.
              </li>
              <li>Your link expires in 15 minutes</li>
            </ul>
          </Card.Text>
        </Card.Body>
      </Card>
    )

  const onSubmit = handleSubmit(async ({ email }) => {
    const { data } = await requestMagicLink({ variables: { email } })
    if (data.requestMagicLink) setIsLinkSent(true)
  })

  return (
    <Form onSubmit={onSubmit}>
      <Form.Group controlId="validationEmail">
        <Form.Label>
          {`If you are registered, we'll send you a magic link by email`}
        </Form.Label>
        <FormControl
          name="email"
          type="email"
          placeholder="Enter email"
          disabled={isSubmitting}
          isInvalid={!!errors.email}
          ref={register({
            validate: (value) =>
              !value
                ? 'required'
                : value.length < 10
                ? 'Email is too short'
                : true,
          })}
        />
        <Form.Control.Feedback type="invalid">
          {errors.email && errors.email.message}
        </Form.Control.Feedback>
      </Form.Group>
      <PrimaryButton disabled={isSubmitting} type="submit">
        Send me a Magic Link
      </PrimaryButton>
    </Form>
  )
}

export default Login
