import React from 'react'
import { HTTP_URL } from 'config'
import { useSessionContext } from 'components/sessions/sessionContext'

export const ChatContext = React.createContext()

export function ChatContextProvider(props) {
  const [chat, setChat] = React.useState(props?.value?.chat)
  const [unreadMessages, setUnreadMessages] = React.useState(0)
  const { session } = useSessionContext()

  if (!chat || !session) return <ChatContext.Provider {...props} value={{}} />

  const {
    value: {
      chat: { isPublic },
    },
    ...pps
  } = props

  const { user: currentUser } = session

  // Allocate users between currentUser and otherUsers
  const users = isPublic
    ? getPublicChatUsers(chat, currentUser)
    : getPrivateChatUsers(chat, currentUser)

  // Adds a message to message state
  const addReceivedMessage = (newMessage) => {
    // TODO: why unique messages?
    const uniqueMessages = chat.messages
      .concat(newMessage)
      .reduce(
        (acc, itm) => ({ ...acc, [`${itm.from}${itm.sentAt}${itm.msg}`]: itm }),
        {}
      )
    setChat({ ...chat, messages: Object.values(uniqueMessages) })
  }

  /**
   * Processes message received from the subscription
   * @param {object} options.subscriptionData: data received from subscription update
   */
  const onMessageReceived = ({
    subscriptionData: {
      data: { messageSent },
    },
  }) => {
    if (messageSent.from !== currentUser.id) playSoundNotification()
    addReceivedMessage(messageSent)
    setUnreadMessages((unreadMessages) => unreadMessages + 1)
  }

  const value = {
    chat: {
      ...chat,
      users,
    },
    onMessageReceived,
    currentUser,
    unreadMessages,
    setUnreadMessages,
  }
  return <ChatContext.Provider {...pps} value={value} />
}

// Private chat users are registered against chat.users
const getPrivateChatUsers = (chat, currentUser) =>
  chat.users.reduce(
    (acc, user) =>
      user.id === currentUser.id
        ? acc
        : { ...acc, otherUsers: acc.otherUsers.concat(user) },
    { currentUser, otherUsers: [] }
  )

// Public chat users are not registered, their publicName is read from messages
const getPublicChatUsers = (chat, currentUser) =>
  chat.messages.reduce(
    (acc, { from, publicName }) => {
      const user = { id: from, username: publicName }
      return user.id === currentUser
        ? acc
        : { ...acc, otherUsers: acc.otherUsers.concat(user) }
    },
    { currentUser, otherUsers: [] }
  )

const playSoundNotification = async () => {
  try {
    await new Audio(`${HTTP_URL}/stairs.mp3`).play()
  } catch (e) {
    console.log('Not allowed to play sound by user', e)
  }
}

export const useChatContext = () => React.useContext(ChatContext)
