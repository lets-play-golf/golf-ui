import { useSubscription } from '@apollo/react-hooks'
import { useLoggedInMutation, useLoggedInQuery } from 'app/useGraphql'

import gql from 'graphql-tag'

const SEND_MESSAGE = gql`
  mutation sendMessage($id: ID!, $message: String!) {
    sendMessage(id: $id, message: $message) {
      id
      messages {
        from
        publicName
        message
        sentAt
      }
    }
  }
`
export const useSendMessage = (variables) =>
  useLoggedInMutation(SEND_MESSAGE, { variables })

const MESSAGE_SENT = gql`
  subscription messageSent($id: ID!) {
    messageSent(id: $id) {
      message
      from
      publicName
      sentAt
    }
  }
`
export const useSubscribeChat = (options) =>
  useSubscription(MESSAGE_SENT, options)

const GET_CHAT = gql`
  query chat($id: ID!) {
    chat(id: $id) {
      id
      isPublic
      users {
        id
        username
      }
      messages {
        from
        publicName
        message
        sentAt
      }
    }
  }
`
export const useChatQuery = (variables) =>
  useLoggedInQuery(GET_CHAT, { variables, fetchPolicy: 'network-only' })
