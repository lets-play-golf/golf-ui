import React from 'react'
import { PrimaryButton } from 'shared/Buttons'
import { useSendMessage } from '../queries'
import Form from 'react-bootstrap/Form'
import FormControl from 'shared/FormControl'
import { useForm } from 'react-hook-form'

const ChatForm = ({ chat }) => {
  const id = chat && chat.id

  const {
    formState: { isSubmitting },
    handleSubmit,
    register,
    reset,
  } = useForm()
  const [sendMessage] = useSendMessage()

  const onSubmit = handleSubmit(async ({ msg }) => {
    try {
      const message = msg.trim()
      if (message.length) {
        const { data } = await sendMessage({ variables: { id, message } })
        if (data) reset()
      }
    } catch (e) {
      console.log('error occured:', e)
    }
  })

  const isDisabled = !chat || isSubmitting
  return (
    <div style={{ margin: 'auto' }}>
      <Form
        disabled={isDisabled}
        inline
        onSubmit={onSubmit}
        style={{ marginTop: '1rem', display: 'flex', flexFlow: 'row nowrap' }}
      >
        <FormControl
          name="msg"
          ref={register()}
          style={{ flexGrow: 1, marginRight: '1rem' }}
          type="text"
          placeholder="Aa"
          autoFocus
          disabled={isDisabled}
          autoComplete="off"
        />
        <PrimaryButton type="submit" disabled={isDisabled}>
          Send
        </PrimaryButton>
      </Form>
    </div>
  )
}
export default ChatForm
