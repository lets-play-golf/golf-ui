import React from 'react'
import styled from 'styled-components'
import { timeAgo } from 'app/timeAgo'
import { useChatContext } from 'components/chats/chatContext'
import { useSubscribeChat } from '../../chats/queries'
import Linkify from 'linkify-react'

const ChatHistory = () => {
  const chatContext = useChatContext()

  const {
    chat: {
      id,
      messages,
      isPublic,
      users: { currentUser, otherUsers },
    },
    setUnreadMessages,
    onMessageReceived,
  } = chatContext
  // Scroll down to last message on load
  const scrollIntoView = () =>
    childReference.current.scrollIntoView({
      block: 'center',
      inline: 'center',
      behavior: 'smooth',
    })
  const childReference = React.useRef(null)

  React.useEffect(() => {
    scrollIntoView()
    setUnreadMessages(0)
  })

  useSubscribeChat({
    variables: { id },
    onSubscriptionData: onMessageReceived,
  })

  // Get username from message (public chat) or users (private chat)
  const getOtherUser = ({ from, publicName }) => {
    if (isPublic) return publicName
    const otherUser = otherUsers.find(({ id }) => id === from)
    if (otherUser) return otherUser.username
    return publicName
  }

  if (!chatContext.chat) return <>This chat is no longer active</>

  return (
    <>
      {messages.map(({ from, publicName, message, sentAt }, index) => {
        const isCurrentUser = currentUser.id === from
        from = isCurrentUser ? 'you' : getOtherUser({ from, publicName })
        return (
          <Message key={index} {...{ from, message, sentAt, isCurrentUser }} />
        )
      })}
      <div
        style={{ float: 'left', clear: 'both' }}
        ref={childReference}
        onChange={() => scrollIntoView()}
      />
    </>
  )
}

const Message = ({ from, message, sentAt, isCurrentUser }) => (
  <Linkify
    options={{
      target: {
        url: '_blank',
        style: { color: 'white' },
      },
    }}
  >
    <MessageWrapper className="unsetselect" {...{ isCurrentUser }}>
      <MessageHeader>
        {from} ({timeAgo.format(new Date(sentAt))})
      </MessageHeader>
      <MessageText {...{ isCurrentUser }}>{message}</MessageText>
    </MessageWrapper>
  </Linkify>
)

const MessageHeader = styled.div`
  font-size: 0.7rem;
  float: ${(props) => props.theme};
`
const MessageText = styled.div`
  text-align: left;
  text-justify: inter-word;
  padding: 0.2rem 0.5rem;
  ${(props) =>
    props.isCurrentUser
      ? `border-radius: 1rem 1rem 0 1rem;`
      : `border-radius: 1rem 1rem 1rem 0;`}
  ${(props) =>
    props.isCurrentUser && `background-color: ${props.theme.accent1}`};
  ${(props) => props.isCurrentUser && `color:${props.theme.background100}`};
  border: 1px solid ${(props) => props.theme.accent1};
  ${(props) =>
    props.isCurrentUser &&
    `
    a {
      color:white;
    }
  `}
`

const MessageWrapper = styled.div`
  margin-top: 0.5rem;
  max-width: 80%;
  margin-left: ${(props) => (props.isCurrentUser ? '20%' : '0')};
`
export default ChatHistory
