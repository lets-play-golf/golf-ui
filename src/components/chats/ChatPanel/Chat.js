import React from 'react'
import styled from 'styled-components'
import ChatHistory from './ChatHistory'
import ChatForm from './ChatForm'
import { useChatContext } from '../chatContext'

/**
 * Displays the chat UI whether the chat is active or not
 * This component relies on a ChatContext provider to be populated with a chat
 */
const Chat = () => {
  const { chat } = useChatContext()
  return (
    <>
      <ChatHistoryWrapper>
        <ChatHistory {...{ chat }} />
      </ChatHistoryWrapper>
      <ChatForm {...{ chat }} />
    </>
  )
}

const ChatHistoryWrapper = styled.div`
  height: 13rem;
  overflow: scroll;
  scrollbar-width: none;

  -ms-overflow-style: -ms-autohiding-scrollbar;

  ::-webkit-scrollbar {
    /*Chrome, Safari, Edge*/
    display: none;
  }
`
export default Chat
