import React from 'react'
import styled from 'styled-components'

/**
 * Wraps children in a flex box with even columns or rows
 * @param {*} children: The elements to be wrapped
 */

const FlexContentControl = ({ children, ...props }) => {
  const justifyContent =
    React.Children.toArray(children).filter(Boolean).length > 1
      ? 'space-between'
      : 'center'

  return (
    <FlexContentWrapper {...{ justifyContent, ...props }}>
      {children}
    </FlexContentWrapper>
  )
}

const FlexContentWrapper = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-flow: ${(props) => props.flow} nowrap;
  justify-content: ${(props) => props.justifyContent};
`

export default FlexContentControl
