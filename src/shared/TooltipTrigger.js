import React from 'react'
import styled from 'styled-components'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Tooltip from 'react-bootstrap/Tooltip'

const StyledTooltip = styled(Tooltip)`
  .tooltip-inner {
    background-color: #2f4fff;
    box-shadow: 0px 0px 4px black;
    opacity: 1 !important;
  }
  .tooltip.bs-tooltip-right .arrow::before {
    border-right-color: #2f4fff !important;
  }
  .tooltip.bs-tooltip-left .arrow::before {
    border-left-color: #2f4fff !important;
  }
  .tooltip.bs-tooltip-bottom .arrow::before {
    border-bottom-color: #2f4fff !important;
  }
  .tooltip.bs-tooltip-top .arrow::before {
    border-top-color: #2f4fff !important;
  }

  .bs-tooltip-auto[x-placement^='bottom'] .arrow::before,
  .bs-tooltip-bottom .arrow::before {
    border-right-color: #2f4fff !important;
  }
`
/**
 * Wraps an element with a tooltip which triggers onclick
 * @see https://react-bootstrap.github.io/components/overlays/#overlay-trigger
 *
 * @param {string} options.text: Tooltip text to display
 * @param {string} options.hideAfter: defaults to 2000 ms
 * @param {top|left|right|bottom} options.placement: defaults to auto
 * @param {string} options.trigger: defaults to `click`
 */
const TooltipTrigger = ({ children, show = 250, ...props }) => {
  const {
    text,
    hideAfter = 1000000,
    placement = 'auto',
    trigger = 'click',
  } = props

  const renderTooltip = (props) => (
    <StyledTooltip {...props}>{text}</StyledTooltip>
  )

  const hide = (tooltip) =>
    setTimeout(() => {
      tooltip.hidden = true
    }, hideAfter)

  return (
    <OverlayTrigger
      onEnter={hide}
      overlay={renderTooltip}
      delay={{ show }}
      {...{ placement, trigger }}
    >
      {children}
    </OverlayTrigger>
  )
}

export default TooltipTrigger
