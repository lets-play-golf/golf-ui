import React from 'react'
import Player from 'components/play/Players/LargePlayer'
import LargePlayingCard from 'components/play/PlayingCards/LargePlayingCard'
import { PlayingCardFlipper } from 'components/play/PlayingCards/PlayingCardFlipper'
import {
  roundCardScores,
  newShuffledDeck,
  random,
} from 'components/play/PlayingCards/cardHelpers'

const TIMES = {
  START_DELAY: 200,
  REVEAL_INTERVAL: 500,
  SCORES_DELAY: 300,
  SCORES_DISPLAY: 4000,
  CARDS_HIDDEN: 1000,
}

const getTime = (phase) => {
  const SHOW_SCORES =
    TIMES.START_DELAY + 4 * TIMES.REVEAL_INTERVAL + TIMES.SCORES_DELAY
  const HIDE_CARDS = SHOW_SCORES + TIMES.SCORES_DISPLAY
  const RESET_ANIMATION = HIDE_CARDS + TIMES.CARDS_HIDDEN
  if (phase === 'RESET_ANIMATION') return RESET_ANIMATION
  if (phase === 'HIDE_CARDS') return HIDE_CARDS
  if (phase === 'SHOW_SCORES') return SHOW_SCORES
}

const MAX_ITERATIONS = 20

/**
 * AnimatedLargePlayer is a LargePlayer that reveals and hides cards in a sequence.
 */
export const AnimatedLargePlayer = ({
  maxIterations = MAX_ITERATIONS,
  player,
}) => {
  const [visibleCards, setVisibleCards] = React.useState(
    newShuffledDeck().slice(0, 4)
  )

  const [iteration, setIteration] = React.useState(0)

  React.useMemo(() => {
    if (iteration === maxIterations) return
    setTimeout(() => {
      setVisibleCards(newShuffledDeck().slice(0, 4))
      setIteration(iteration + 1)
    }, getTime('RESET_ANIMATION'))
  }, [iteration])

  return (
    <Routine
      {...{ player, visibleCards }}
      // Forces re-render of child component
      key={iteration}
    />
  )
}

/**
 * Routine is the main animation sequence for the LargePlayer.
 */
const Routine = ({ visibleCards, player }) => {
  const [showScores, setShowScores] = React.useState(false)

  const [hideCards, setHideCards] = React.useState(false)

  React.useEffect(() => {
    setTimeout(() => setShowScores(true), getTime('SHOW_SCORES'))
    setTimeout(() => setHideCards(true), getTime('HIDE_CARDS'))
  }, [])

  const scores = roundCardScores(visibleCards)

  const delays = new Array(4)
    .fill()
    .map((_, index) => TIMES.START_DELAY + index * TIMES.REVEAL_INTERVAL)
    .sort(random)

  const cardBack = <LargePlayingCard />
  return (
    <Player {...{ player }}>
      {visibleCards.map((id, index) => {
        const cardFront = (
          <LargePlayingCard
            id={id}
            score={showScores ? scores[index] : undefined}
          />
        )
        const delay = hideCards ? 100 * index : delays[index]

        return (
          <PlayingCardFlipper
            to={hideCards ? cardBack : cardFront}
            from={hideCards ? cardFront : cardBack}
            delay={delay}
            key={index + hideCards * 10}
          />
        )
      })}
    </Player>
  )
}
