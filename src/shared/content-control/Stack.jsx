import React from 'react'

/**
 * Adds the same margin at the bottom of each child component except for the last.
 * @param {Number} options.space: The bootstrap space - i.e. a number between 0 and 3
 */

const Stack = ({ space, children }) => {
  const childrenArray = React.Children.toArray(children).filter((itm) => !!itm)
  return (
    <React.Fragment>
      {childrenArray.map((child, index) => {
        const isLast = index + 1 === childrenArray.length
        return (
          <div key={index} className={`${!isLast && `mb-${space}`}`}>
            {child}
          </div>
        )
      })}
    </React.Fragment>
  )
}

export default Stack
