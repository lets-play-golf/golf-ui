import React from 'react'
import styled from 'styled-components'

/**
 * EvenlySpaced uses FlexBox to distribute the space between children:
 * - Wraps the children in a Flexbox taking 100% of the parent
 * - Works in either direction - column or row
 * - If there is only one child, it will be centered
 * - Accepts flex style props, e.g.: `style={{ gap: '1rem' }}`
 */

const Column = ({ children, ...props }) => {
  const justifyContent =
    React.Children.toArray(children).filter(Boolean).length > 1
      ? 'space-between'
      : 'center'

  return (
    <ColumnWrapper {...{ justifyContent, ...props }}>{children}</ColumnWrapper>
  )
}

const Row = ({ children, ...props }) => {
  const justifyContent =
    React.Children.toArray(children).filter(Boolean).length > 1
      ? 'space-between'
      : 'center'

  return <RowWrapper {...{ justifyContent, ...props }}>{children}</RowWrapper>
}

const ColumnWrapper = styled.div`
  height: 100%;
  display: flex;
  flex-flow: column nowrap;
  justify-content: ${(props) => props.justifyContent};
`

const RowWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-flow: row nowrap;
  justify-content: ${(props) => props.justifyContent};
`

export { Column, Row }
export default { Column, Row }
