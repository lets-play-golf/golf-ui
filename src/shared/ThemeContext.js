import React from 'react'
import { ThemeContext, ThemeProvider } from 'styled-components'
import themeValues from 'shared/theme'

const ThemeContextProvider = (props) => {
  const themeKeys = Object.keys(themeValues)
  const defaultTheme = themeValues[themeKeys[0]]

  const [theme, setTheme] = React.useState(defaultTheme)

  return (
    <ThemeContext.Provider value={{ setTheme, theme }}>
      <ThemeProvider theme={theme}>{props.children}</ThemeProvider>
    </ThemeContext.Provider>
  )
}

export const useThemeContext = () => React.useContext(ThemeContext)

export { ThemeContextProvider }
