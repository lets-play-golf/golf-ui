import React from 'react'
import styled from 'styled-components'
import Button_RB from 'react-bootstrap/Button'

export const PrimaryButton = styled(Button_RB)`
  background-color: ${(props) => props.theme.accent1};
  border-color: ${(props) => props.theme.accent1};
  color: white;
`

export const SecondaryButton = styled(Button_RB)`
  background-color: #00000000;
  border-color: ${(props) => props.theme.accent1};
  color: ${(props) => props.theme.accent1};
`

export const Button = ({ isPrimary, ...props }) =>
  isPrimary ? <PrimaryButton {...props} /> : <SecondaryButton {...props} />
