import Card from 'react-bootstrap/Card'
import styled from 'styled-components'

/**
 * @see https://hype4.academy/tools/glassmorphism-generator
 */
const StyledCard = styled(Card)`
  border-radius: 0.8rem;
  backdrop-filter: blur(8px);
  -webkit-backdrop-filter: blur(8px);
  background: rgba(255, 255, 255, 0.1);
  /* original */
  box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
  /* dark blue */
  box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
  border: 0;
`

export default StyledCard
