import styled from 'styled-components'
import FormControl from 'react-bootstrap/FormControl'

export default styled(FormControl)`
  background-color: #00000000;
  border-color: ${(props) => props.theme.text};
`
