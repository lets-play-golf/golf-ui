/**
 * Smart Home Dark Mode App
 * @see https://dribbble.com/shots/15368753-Smart-Home-Dark-Mode-App
 */
const shPalette = () => {
  const {
    bg100 = '#05103a',
    bg80 = 'rgb(30, 40, 78)',
    color1 = '#00c6fe',
    color2 = '#ec5990',
  } = {}

  return {
    bg100: bg100,
    bg80: bg80,
    text: 'white',
    accent1: color1,
    accent2: color2,
  }
}

const theme = (palette) => {
  const { bg100, bg80, text, accent1, accent2, accent3 } = palette

  return {
    // backgrounds
    background100: bg100,
    // Shade of a card over the navy blue background
    background80: bg80,
    background2: accent3,
    backgroundVariant: accent3,

    // text
    text: text || accent1,

    // buttons
    accent1,

    // notification
    accent2,
  }
}

export default {
  darkBlue: theme(shPalette()),
}
