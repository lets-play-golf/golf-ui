import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import ProgressBarRB from 'react-bootstrap/ProgressBar'

const ProgressBar = styled(ProgressBarRB)`
  .bg-text-color {
    background-color: ${(props) => props.theme.text};
  }
`

const CountDown = ({ timeInMs }) => {
  const [timeLeft, setTimeLeft] = useState(timeInMs)
  useEffect(() => {
    const timer =
      timeLeft > 0 && setInterval(() => setTimeLeft(timeLeft - 1000), 1000)
    return () => clearInterval(timer)
  }, [timeLeft])
  return (
    <div>
      <ProgressBar now={(timeLeft / timeInMs) * 100} variant="textColor" />
    </div>
  )
}
export default CountDown
