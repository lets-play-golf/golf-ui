import React from 'react'
import ReactDOM from 'react-dom'
import * as serviceWorker from './serviceWorker'

import { ApolloProvider } from 'react-apollo'
import Root from 'app/Root'
import initializeClient from 'app/graphqlClient'
import GlobalStyle from 'shared/GlobalStyle'
import { ThemeContextProvider } from 'shared/ThemeContext'
import ReactGA from 'react-ga4'

const TRACKING_ID = 'G-CHML1WSN6L'

ReactGA.initialize(TRACKING_ID)
;(async () => {
  const graphqlClient = await initializeClient()

  ReactDOM.render(
    <ApolloProvider client={graphqlClient}>
      <ThemeContextProvider>
        <GlobalStyle />
        <Root />
      </ThemeContextProvider>
    </ApolloProvider>,
    document.getElementById('root')
  )
})()

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
