const isProduction = !document.location.host.startsWith('localhost')
const baseUrl = `${document.location.host.split(':')[0]}${
  isProduction ? '' : `:${document.location.host.split(':')[1]}`
}`
const isSecure = !!isProduction
const gatewayUrl = `${document.location.host.split(':')[0]}${
  isProduction ? '' : ':4001'
}`
// eslint-disable-next-line no-undef
module.exports = {
  IS_PRODUCTION: isProduction,
  HTTP_URL: `http${isSecure ? 's' : ''}://${baseUrl}`,
  GATEWAY_URI: `${gatewayUrl}/graphql`,
  GQL_CONNECTION_SECURE: isSecure,
}
