import React from 'react'
import MainLayout from 'app/MainLayout'
import { useChatQuery } from 'components/chats/queries'
import { useSessionContext } from '../../components/sessions/sessionContext'
import { Redirect } from '@reach/router'
import ChatPanel from 'components/chats/ChatPanel'
import { ChatContextProvider } from 'components/chats/chatContext'

const FindAPartner = () => {
  const { session, handleExpiredSession } = useSessionContext()
  const { data, error } = useChatQuery({ id: '5efeffe20ed44884e998f047' })
  if (!session) return <Redirect to="/login" noThrow />
  if (!data) return <></>
  if (error) handleExpiredSession(error)
  const { chat } = data
  return (
    <MainLayout>
      <h1>Find a Partner</h1>
      <ChatContextProvider value={{ chat, currentUser: session.user }}>
        <ChatPanel />
      </ChatContextProvider>
    </MainLayout>
  )
}
export default FindAPartner
