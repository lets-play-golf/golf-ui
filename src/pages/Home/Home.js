import React from 'react'
import { useSessionContext } from '../../components/sessions/sessionContext'
import Games from '../../components/games/Games'
import MainLayout from 'app/MainLayout'
import GolfInfo from '../GolfInfo'

const Home = () => {
  const { session } = useSessionContext()
  return (
    <MainLayout>{session ? <Games /> : <GolfInfo isHomePage />}</MainLayout>
  )
}
export default Home
