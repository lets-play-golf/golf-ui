import React from 'react'
import Jumbotron from 'react-bootstrap/Jumbotron'
import { PrimaryButton, SecondaryButton } from 'shared/Buttons'
import Card from 'shared/Card'
import Accordion from 'react-bootstrap/Accordion'
import Button from 'react-bootstrap/Button'
import { TakeALook, YourTurn, Score } from 'app/icons'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import TooltipTrigger from 'shared/TooltipTrigger'
import { AnimatedLargePlayer } from 'shared/AnimatedLargePlayer'

const GolfInfo = ({ isHomePage }) => {
  return (
    <Jumbotron style={{ background: 'none' }}>
      <Row align="center" className="mb-3">
        <Col>
          <h1>{`Let's Play Golf`}</h1>
          <p>4 cards, 2 friends, 1 winner!</p>
        </Col>
      </Row>
      {isHomePage && (
        <Row align="center" className="mb-5">
          <Col>
            <PrimaryButton href="/register">Register</PrimaryButton>
          </Col>
          <Col>
            <SecondaryButton href="/login">Login</SecondaryButton>
          </Col>
        </Row>
      )}
      <AnimatedLargePlayer player={{ username: 'you' }} maxIterations={100} />
      <br />
      <Accordion>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="0">
              What is golf?
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              Golf is a thrilling card game where 2 or more players compete to
              score the least amount of points.
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="1">
              How to play golf?
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="1">
            <Card.Body>
              <div>
                <PrimaryButton
                  href="https://www.youtube.com/watch?v=XZc0VRehFIs"
                  target="_blank"
                >
                  Watch the demo!
                </PrimaryButton>
              </div>
              <div className="mt-4">
                <h5>
                  Setup{' '}
                  <span>
                    <TakeALook />
                  </span>
                </h5>
                <ul>
                  <li>Four cards are dealt to you face down.</li>
                  <li>
                    Before the round starts, choose 2 of these cards and take a
                    look at them.
                  </li>
                  <li>
                    <strong>
                      {`Remember the numbers and where they are. You'll need
                      to pair them up with cards you pick up later.`}
                    </strong>
                  </li>
                </ul>
              </div>
              <div className="mt-4">
                <h5>
                  Scoring <Score />
                </h5>
                <p>At the end of the round, all cards are face up.</p>
                <ul>
                  <li>Pairs cancel each other out and score 0</li>
                  <li>Four of a kind scores -20</li>
                </ul>
                <div>Single cards score:</div>
                <table
                  style={{
                    border: '1px solid',
                    textAlign: 'center',
                    minWidth: '13rem',
                    margin: '.5rem 0',
                  }}
                >
                  <tbody>
                    <tr>
                      <td>Numbered cards</td>
                      <td>2-10</td>
                    </tr>
                    <tr>
                      <td>Queens or Jacks</td>
                      <td>10</td>
                    </tr>
                    <tr>
                      <td>Aces</td>
                      <td>1</td>
                    </tr>
                    <tr>
                      <td>Kings</td>
                      <td>0</td>
                    </tr>
                  </tbody>
                </table>
                <p>
                  <strong>
                    The aim is to score the least amount of points.
                  </strong>
                </p>
              </div>
              <div className="mt-4">
                <h5>
                  On your turn
                  <TooltipTrigger
                    hideAfter={5000}
                    text="It is your turn to play when you see this emoji - your possible actions are shown next to it."
                  >
                    <YourTurn />
                  </TooltipTrigger>
                </h5>
                <ol className="pl-4">
                  <li>
                    Start your turn by picking up a card from the middle piles.
                    There are 2 cards to choose from: a known card from the
                    discard pile up or an unknown one. You may want to pick up
                    the known card if you can match it with one you know or if
                    it scores low.
                  </li>
                  <li>
                    {`If you don't want to keep the card you picked up, you
                    can discard it and reveal a card of your choice from your
                    hand.`}
                  </li>
                  <li>
                    Otherwise pick the card from your hand you want to discard
                    instead. The card you picked up will remain face up in the
                    position you selected.
                  </li>
                  <li>
                    At the end of your turn, one card from your hand is revealed
                    and one card is discarded.
                  </li>
                </ol>
                <p>
                  After each player has had 4 turns, all cards are revealed and
                  the round is scored.
                </p>
                <p>
                  The player scoring the least gets to start the next round.
                </p>
              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="2">
              {`Let's do this!`}
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="2">
            <Card.Body>
              {`It's simple:`}
              <ul>
                <li>Register</li>
                <li>Create a game</li>
                <li>Share link invitation</li>
                <li>When your friend accepts the invitation, hit Play!</li>
              </ul>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </Jumbotron>
  )
}
export default GolfInfo
