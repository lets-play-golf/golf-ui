import React from 'react'
import MainLayout from 'app/MainLayout'
import GolfInfo from '../GolfInfo'

const Rules = () => {
  return (
    <MainLayout>
      <GolfInfo />
    </MainLayout>
  )
}
export default Rules
