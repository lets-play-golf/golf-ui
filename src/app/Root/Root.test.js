import React from 'react'
import Root from './Root'
import { renderApollo } from '../../../test/integration/test-utils.js'
import { act } from 'react-dom/test-utils'

test('renders learn react link', async () => {
  await act(async () => {
    const { getByText } = await renderApollo(<Root />)
    const loginLink = getByText(/LOGIN/i)
    expect(loginLink).toBeInTheDocument()
  })
})
