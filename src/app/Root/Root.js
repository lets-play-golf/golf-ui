import React from 'react'
import styled from 'styled-components'
import Router from '../routes'
import { SessionContextProvider } from '../../components/sessions/sessionContext'
import ErrorBoundary from '../ErrorBoundary'
import { ModalProvider } from '../modal'
import { PlayGameContextProvider } from 'components/play/playGameContext'
import { ChatContextProvider } from 'components/chats/chatContext'

const Wrapper = styled.div`
  box-sizing: border-box;
  display: flex;
  min-height: 100vh;
  width: 100%;
  flex-direction: column;
  #router {
    display: flex;
    min-height: 100vh;
    width: 100%;
    flex-direction: column;
  }
`

const Container = styled.div`
  display: flex;
  flex: 1;
`

const Root = () => {
  return (
    <ErrorBoundary>
      <Wrapper>
        <ModalProvider>
          <SessionContextProvider>
            <PlayGameContextProvider value={null}>
              <ChatContextProvider value={null}>
                <Container>
                  <Router />
                </Container>
              </ChatContextProvider>
            </PlayGameContextProvider>
          </SessionContextProvider>
        </ModalProvider>
      </Wrapper>
    </ErrorBoundary>
  )
}

export default Root
