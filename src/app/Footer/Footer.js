import React from 'react'
import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar'
import { Link } from '@reach/router'
import Nav from 'react-bootstrap/Nav'
import NavLink from 'react-bootstrap/NavLink'
import styled from 'styled-components'

const Footer = () => {
  return (
    <Container>
      <Navbar>
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="Navs mr-auto">
            <FooterLink as={Link} to="/docs/about">
              about
            </FooterLink>
          </Nav>
          <Nav className="Navs m-auto">
            <FooterLink as={Link} to="/docs/technical">
              technical
            </FooterLink>
          </Nav>
          <Nav className="Navs ml-auto">
            <FooterLink as={Link} to="/docs/contact">
              contact
            </FooterLink>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <p style={{ fontSize: '.7rem', marginTop: '1rem' }}>
        Disclaimer: this app runs on a diesel engine. It may take a few seconds
        to warm up when you first hit it.
      </p>
    </Container>
  )
}

const FooterLink = styled(NavLink)`
  color: white;
`
export default Footer
