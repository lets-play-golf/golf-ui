import React from 'react'
import { Link } from '@reach/router'
import {
  SessionContext,
  useSessionContext,
} from '../../components/sessions/sessionContext'
import { useDeleteSession } from 'components/sessions/queries'

import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import NavLink from 'react-bootstrap/NavLink'

const AccountArea = ({ session }) => {
  const [logout] = useDeleteSession()
  const { deleteSession } = useSessionContext()
  const { user } = session || {}
  const handleOnClick = () => {
    logout()
    deleteSession()
  }

  if (!user) {
    return (
      <>
        <NavLink as={Link} to="/login">
          LOGIN
        </NavLink>
        <NavLink as={Link} to="/leaderboard">
          LEADERBOARD
        </NavLink>
      </>
    )
  }

  return (
    <>
      <NavLink as={Link} to="/">
        HOME
      </NavLink>
      <NavLink as={Link} to="/account">
        YOUR ACCOUNT
      </NavLink>
      <NavLink as={Link} to="/leaderboard">
        LEADERBOARD
      </NavLink>
      <NavLink as={Link} to="/find_a_partner">
        FIND A PARTNER
      </NavLink>
      <NavLink as={Link} to="/" id="header_logout" onClick={handleOnClick}>
        LOGOUT
      </NavLink>
    </>
  )
}

const Header = () => {
  return (
    <Navbar collapseOnSelect expand="md" variant="dark">
      <Container>
        <Navbar.Brand href="/">
          <img src="/logo192.png" style={{ width: '45px' }} alt="logo" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="Navs ml-auto">
            <SessionContext.Consumer>
              {(value) => <AccountArea {...value} />}
            </SessionContext.Consumer>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}
export default Header
