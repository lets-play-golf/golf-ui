import React, { Fragment } from 'react'
import { Link } from '@reach/router'

const Crumbs = ({ crumbs, isForward = true }) => {
  const breadcrumbs = isForward ? crumbs : crumbs.reverse()
  const separator = isForward ? ' > ' : ' < '
  return <BreadcrumbsUI crumbs={breadcrumbs} separator={separator} />
}

const BreadcrumbsUI = ({ crumbs, separator }) => (
  <>
    {crumbs.map(({ title, link }, index) => (
      <Fragment key={index}>
        {index > 0 && <>&nbsp;&nbsp;&nbsp;{`${separator}`}&nbsp;&nbsp;&nbsp;</>}
        <Link to={link}>{title}</Link>
      </Fragment>
    ))}
  </>
)

export default { Crumbs }
