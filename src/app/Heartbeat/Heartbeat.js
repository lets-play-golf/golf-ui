import React from 'react'
import gql from 'graphql-tag'
import { useSubscription } from '@apollo/react-hooks'

const CONNECTIONS_SUBSCRIPTION = gql`
  subscription {
    heartbeat {
      time
    }
  }
`

const HeartBeat = () => {
  const { loading, error, data } = useSubscription(CONNECTIONS_SUBSCRIPTION)
  return (
    <>
      <h2>Heartbeat</h2>
      {loading && <p>listening (wait for 10 seconds max)</p>}
      {error && console.log(error) && <p>error: {error}</p>}
      {data && <p>Server time {data.heartbeat.time}</p>}
    </>
  )
}
export default HeartBeat
