import React from 'react'
import Header from './Header'
import Footer from './Footer'
import Container from 'react-bootstrap/Container'
import { useLocation } from '@reach/router'
import Stack from 'shared/content-control/Stack'
import { useSetAndTrackTitle } from 'app/pages'

import {
  HOME_PAGE,
  CREATE_USER_PAGE,
  CREATE_SESSION_PAGE,
  PLAY_PAGE,
  JOIN_GAME_PAGE,
  CREATE_GAME_PAGE,
} from '../dataTestIds'

const dataTestId = {
  '/': HOME_PAGE,
  '/register': CREATE_USER_PAGE,
  '/login': CREATE_SESSION_PAGE,
  '/play': PLAY_PAGE,
  '/join': JOIN_GAME_PAGE,
  '/create': CREATE_GAME_PAGE,
}

const MainLayout = ({ children }) => {
  const { pathname } = useLocation()
  useSetAndTrackTitle()
  return (
    <Stack space={'3'}>
      <Header />
      <Container data-testid={dataTestId[pathname]}>{children}</Container>
      <Footer />
    </Stack>
  )
}

export default MainLayout
