import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import {
  InMemoryCache,
  IntrospectionFragmentMatcher,
} from 'apollo-cache-inmemory'
import { split } from 'apollo-link'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'
import { GATEWAY_URI, GQL_CONNECTION_SECURE } from 'config'

const secure = GQL_CONNECTION_SECURE ? 's' : ''

async function initializeClient() {
  // Fragment matcher
  const introspectionQueryResultData = await getSchemas()
  const fragmentMatcher = new IntrospectionFragmentMatcher({
    introspectionQueryResultData,
  })

  // Links
  const httpLink = new HttpLink({
    credentials: 'include',
    uri: `http${secure}://${GATEWAY_URI}`,
  })

  const wsLink = new WebSocketLink({
    uri: `ws${secure}://${GATEWAY_URI}`,
    fetchOptions: {
      mode: 'no-cors',
    },
    options: {
      reconnect: true,
    },
  })

  const link = split(
    // split based on operation type
    ({ query }) => {
      const { kind, operation } = getMainDefinition(query)
      return kind === 'OperationDefinition' && operation === 'subscription'
    },
    wsLink,
    httpLink
  )

  return new ApolloClient({
    cache: new InMemoryCache({
      fragmentMatcher,
    }),
    link,
  })
}

export default initializeClient

async function getSchemas() {
  const result = await fetch(`http${secure}://${GATEWAY_URI}`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    credentials: 'include',
    body: JSON.stringify({
      variables: {},
      query: `
        {
          __schema {
            types {
              kind
              name
              possibleTypes {
                name
              }
            }
          }
        }
      `,
    }),
  })
  if (result.status === 401) console.log('Fragment Matcher Error: unauthorized')
  const res = await result.json()
  const filteredData = res.data.__schema.types.filter(
    (type) => type.possibleTypes !== null
  )
  res.data.__schema.types = filteredData
  return res.data
}
