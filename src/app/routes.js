import React from 'react'
import { Router as ReachRouter } from '@reach/router'

import Home from '../pages/Home'
import CreateUser from 'components/users/CreateUser'
import PlayerDisplay from 'components/players/PlayerDisplayPage'
import CreateSession from 'components/sessions/CreateSession'
import RequestPasswordReset from 'components/users/RequestPasswordReset'
import ResetPassword from 'components/users/ResetPassword'

import PlayGame from 'components/play/PlayGame/PlayGame'
import JoinGame from 'components/games/JoinGame'
import CreateGame from 'components/games/CreateGame'
import FindAPartner from '../pages/FindAPartner'
import LeaderboardPage from 'components/players/LeaderboardPage'
import UpdateUser from 'components/users/UpdateUser'
import DeleteUser from 'components/users/DeleteUser'
import ConsumeMagicLink from 'components/sessions/ConsumeMagicLink'

import Rules from 'pages/Rules'
import Docs from 'docs'
import { PAGES } from './pages'

const Router = () => (
  <ReachRouter id="router">
    {/* Generic model pages */}
    <Home path="/" />
    <CreateUser path={PAGES.CreateUser.path} />
    <PlayerDisplay path={PAGES.PlayerDisplay.path} />
    <CreateSession path={PAGES.CreateSession.path} />
    <RequestPasswordReset path={PAGES.RequestPasswordReset.path} />
    <ResetPassword path={PAGES.ResetPassword.path} />
    <ConsumeMagicLink path={PAGES.ConsumeMagicLink.path} />

    {/* Golf pages  */}
    <PlayGame path={PAGES.PlayGame.path} />
    <JoinGame path={PAGES.JoinGame.path} />
    <CreateGame path={PAGES.CreateGame.path} />
    <FindAPartner path={PAGES.FindAPartner.path} />
    <UpdateUser path={PAGES.UpdateUser.path} />
    <DeleteUser path={PAGES.DeleteUser.path} />
    <LeaderboardPage path={PAGES.LeaderboardPage.path} />

    {/* Static pages */}
    <Rules path={PAGES.Rules.path} />
    <Docs path={PAGES.Docs.path} />
  </ReachRouter>
)

export default Router
