import { keyframes } from 'styled-components'

/**
 * Add pulse animation to any box
 * @param {number} options.minScale: the smallest size the box should transition to - defaults to .90
 */
export const pulse = ({ minScale = 0.9 }) => keyframes`
  0% {
    transform: scale(${minScale});
  }

  70% {
    transform: scale(1);
  }

  100% {
    transform: scale(${minScale});
  }
`

/**
 * Add glow animation to any box
 * @param {string} options.color: the hex color to add glowing effect - optional
 */
export const glow = ({ color = '#FFFFFF' } = {}) => keyframes`
  0% {
    box-shadow: 0 0 0 0 ${color}99;
  }

  70% {
    box-shadow: 0 0 0 10px ${color}00;
  }

  100% {
    box-shadow: 0 0 0 0 ${color}00;
  }
`
