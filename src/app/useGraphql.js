import { useQuery, useMutation } from '@apollo/react-hooks'
import { navigate } from '@reach/router'
import { useSessionContext } from 'components/sessions/sessionContext'

const isSessionExpired = (error) =>
  error?.networkError?.result?.errors[0]?.extensions?.code === 'UNAUTHENTICATED'

/**
 * Redirects the user to the login page if their session timed out.
 * @param {object} query: gql tagged query
 * @param {options} options: graphQL options
 */
export const useLoggedInQuery = (
  query,
  { variables, fetchPolicy = 'network-only' } = {}
) => {
  const { session, deleteSession } = useSessionContext()

  if (!session) navigate('/login')

  return useQuery(query, {
    variables,
    fetchPolicy,
    onError: (error) => {
      console.log(
        'useQuery onErroruseQuery onErroruseQuery onErroruseQuery onErroruseQuery onErroruseQuery onErroruseQuery onErroruseQuery onError'
      )
      if (isSessionExpired(error)) {
        deleteSession()
        return navigate('/login')
      }
      console.log('useLoggedInQuery error >>', error)
      return error
    },
  })
}

/**
 * Redirects the user to the login page if their session timed out.
 * @param {object} mutation: gql tagged query
 * @param {options} options: graphQL options
 */
export const useLoggedInMutation = (mutation, { variables } = {}) => {
  const { session, deleteSession } = useSessionContext()

  if (!session) navigate('/login')

  return useMutation(mutation, {
    variables,
    onError: (error) => {
      if (isSessionExpired(error)) {
        deleteSession()
        return navigate('/login')
      }
      console.log('useLoggedInMutation error >>', error)
      return error
    },
  })
}
