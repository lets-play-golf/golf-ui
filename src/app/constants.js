export const passwordRegex = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})/
export const usernameRegex = /^[a-zA-Z0-9-]{3,8}$/
export const emailRegex =
  /^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/
export const MAX_NUMBER_OF_PLAYERS = 6
