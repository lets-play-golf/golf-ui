import React from 'react'

/**
 * @typedef {object} ModalContextProps
 * @property {React.ReactElement} contentElement
 * @property {function(): void} onClose
 */

/**
 * @typedef {object} ModalContextValue
 * @property {boolean} modalOpened
 * @property {function(React.ReactElement, function(): void): void} openModal
 * @property {function(): void} closeModal
 * @property {ModalContextProps} modalProps
 */

/** @type {React.Context<ModalContextValue>} */
const MODAL_CONTEXT = React.createContext(null)

export function useModalContext() {
  return React.useContext(MODAL_CONTEXT)
}

/** @type {ModalContextProps} */
const INITIAL_STATE = {
  contentElement: null,
  onClose: null,
}

export function ModalProvider({ children }) {
  const [modalProps, setModalProps] = React.useState(INITIAL_STATE)

  const closeModal = React.useCallback(() => setModalProps(INITIAL_STATE), [])

  const openModal = React.useCallback(
    (contentElement, closeAfter, onClose) => {
      if (closeAfter) setTimeout(closeModal, closeAfter)
      return setModalProps({ contentElement, onClose })
    },
    [closeModal]
  )

  const value = React.useMemo(
    () => ({
      modalOpened: modalProps.contentElement != null,
      openModal,
      closeModal,
      modalProps,
    }),
    [modalProps, openModal, closeModal]
  )

  // eslint-disable-next-line react/jsx-pascal-case
  return (
    <MODAL_CONTEXT.Provider value={value}>{children}</MODAL_CONTEXT.Provider>
  )
}
