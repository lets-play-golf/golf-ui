import { useLocation } from '@reach/router'
import ReactGA from 'react-ga4'

/**
 * Matches components with path and titles.
 */
const SITENAME = `Let's play golf`

export const PAGES = {
  CreateUser: {
    path: 'register',
    title: `${SITENAME} - Create an account in one minute`,
  },
  PlayerDisplay: {
    path: 'account',
    title: `${SITENAME} - Account details`,
  },
  CreateSession: {
    path: 'login',
    title: `${SITENAME} - Login to your account`,
  },
  RequestPasswordReset: {
    path: 'password-reset',
    title: `${SITENAME} - Request a password reset`,
  },
  ResetPassword: {
    path: 'rpr/:resetHash',
    title: `${SITENAME} - Resetting your password`,
  },
  ConsumeMagicLink: {
    path: 'login/ml/:hash',
    title: `${SITENAME} - Resetting your password`,
  },
  PlayGame: {
    path: 'play/:id',
    title: `${SITENAME} - Play the game`,
  },
  JoinGame: {
    path: 'join/:id',
    title: `${SITENAME} - Join existing game`,
  },
  CreateGame: {
    path: 'create',
    title: `${SITENAME} - New game`,
  },
  FindAPartner: {
    path: 'find_a_partner',
    title: `${SITENAME} - find a parter`,
  },
  UpdateUser: {
    path: 'update-account',
    title: `${SITENAME} - Update your account details`,
  },
  DeleteUser: {
    path: 'delete-account',
    title: `${SITENAME} - Delete your account`,
  },
  LeaderboardPage: {
    path: 'leaderboard',
    title: `${SITENAME} - See how you rank against other players`,
  },
  Rules: {
    path: 'rules',
    title: `${SITENAME} - rules of the game`,
  },
  Contact: {
    path: 'docs/contact',
    title: `${SITENAME} - contact page`,
  },
  Technical: {
    path: 'docs/technical',
    title: `${SITENAME} - technical`,
  },
  About: {
    path: 'about',
    title: `${SITENAME} - about`,
  },
  Docs: {
    path: 'docs/*',
    getTitle: (pathname) =>
      `${SITENAME} - ${pathname.split('/')[pathname.split('/').length - 1]}`,
  },
  // At the end so that it is matched by the RexExp last in case there is a more accurate match
  Home: {
    path: '',
    title: `${SITENAME} - Home`,
  },
}

const getCurrentPage = (pathname) =>
  Object.values(PAGES).find(({ path }) =>
    pathname.match(new RegExp(`^/${path}`))
  ) || {}

export const useSetAndTrackTitle = () => {
  const { pathname } = useLocation()

  const page = getCurrentPage(pathname)
  const { getTitle } = page
  const title = getTitle ? getTitle(pathname) : page.title

  if (typeof title !== 'string') return

  // Set title
  document.title = title

  // Track pageview in GA
  ReactGA.send({ hitType: 'pageview', page: pathname, title })
}
