import React from 'react'
import styled from 'styled-components'
import { BsClipboardData, BsChat } from 'react-icons/bs'
import {
  GiClubs,
  GiHearts,
  GiSpades,
  GiDiamonds,
  GiShrug,
  GiChart,
  GiGolfFlag,
  GiGolfTee,
  GiThink,
} from 'react-icons/gi'
import { MdArrowBack, MdSportsKabaddi } from 'react-icons/md'
import { SiProbot } from 'react-icons/si'
import { IoMdCloseCircleOutline } from 'react-icons/io'
import { FaUserFriends } from 'react-icons/fa'
import { FiRefreshCcw } from 'react-icons/fi'
// Visual messaging
export const YourTurn = ({ children }) => (
  <span role="img" aria-label="your-turn">
    ☝️ {children}
  </span>
)
export const GolfHole = ({ children }) => (
  <span role="img" aria-label="golf-hole">
    ⛳ {children}
  </span>
)
export const TakeALook = ({ children }) => (
  <span role="img" aria-label="take-a-look">
    <GiThink /> {children}
  </span>
)
export const Score = ({ children }) => (
  <span role="img" aria-label="scores">
    📈 {children}
  </span>
)
export const GolfPlayer = ({ children }) => (
  <span role="img" aria-label="golf-player">
    🏌🏻‍♂️ {children}
  </span>
)
export const Lightning = ({ children }) => (
  <span role="img" aria-label="online">
    ⚡ {children}
  </span>
)
export const Trophy = ({ children }) => (
  <span role="img" aria-label="winner">
    🏆 {children}
  </span>
)
export const ThumbDown = ({ children }) => (
  <span role="img" aria-label="loser">
    👎 {children}
  </span>
)

// PlayGame menu
export const ScoreBoardIcon = () => (
  <span role="img" aria-label="scores" title="scores">
    <BsClipboardData />
  </span>
)

export const ChatIcon = () => (
  <span role="img" aria-label="chat" title="chat">
    <BsChat />
  </span>
)
export const ExitIcon = () => (
  <span role="img" aria-label="exit" title="exit">
    <MdArrowBack />
  </span>
)

export const ReloadIcon = () => (
  <span role="img" aria-label="reload" title="reload">
    <FiRefreshCcw />
  </span>
)

export const HelpIcon = () => (
  <span role="img" aria-label="help" title="help">
    <GiShrug />
  </span>
)

// Card suits
export const ClubsIcon = () => (
  <span role="img" aria-label="clubs">
    <GiClubs />
  </span>
)
export const HeartsIcon = () => (
  <span role="img" aria-label="hearts">
    <GiHearts />
  </span>
)
export const SpadesIcon = () => (
  <span role="img" aria-label="spades">
    <GiSpades />
  </span>
)
export const DiamondsIcon = () => (
  <span role="img" aria-label="diamonds">
    <GiDiamonds />
  </span>
)

// Nav
export const CloseModal = () => (
  <span role="img" aria-label="close">
    <IoMdCloseCircleOutline />
  </span>
)

// Badges
export const FirstPlaceIcon = () => (
  <span role="img" aria-label="first-place" title="first place">
    🥇
  </span>
)
export const SecondPlaceIcon = () => (
  <span role="img" aria-label="second-place" title="second place">
    🥈
  </span>
)
export const ThirdPlaceIcon = () => (
  <span role="img" aria-label="third-place" title="third place">
    🥉
  </span>
)
export const BabyIcon = () => (
  <span role="img" aria-label="newest-player" title="newest member">
    👶
  </span>
)
export const HeartIcon = () => (
  <span role="img" aria-label="most-passionate" title="most games completed">
    💖
  </span>
)

export const ChartIcon = (props) => (
  <IconWrapper>
    <GiChart
      style={{
        width: '100%',
        height: '100%',
      }}
    />
  </IconWrapper>
)

// Adds the glow effect on large icons
const IconWrapper = styled.div`
  filter: drop-shadow(0 0 10px ${(props) => props.theme.accent1});
  width: 100%;
  height: 100%;
`

export const GolfFlagIcon = () => (
  <IconWrapper>
    <GiGolfFlag
      style={{
        width: '100%',
        height: '100%',
      }}
    />
  </IconWrapper>
)

export const Robot = () => (
  <IconWrapper>
    <SiProbot
      style={{
        width: '100%',
        height: '100%',
      }}
    />
  </IconWrapper>
)

export const GolfBall = () => (
  <IconWrapper>
    <GiGolfTee
      style={{
        width: '100%',
        height: '100%',
      }}
    />
  </IconWrapper>
)

export const Friends = () => (
  <IconWrapper>
    {(
      <MdSportsKabaddi
        style={{
          width: '100%',
          height: '100%',
        }}
      />
    ) || <FaUserFriends />}
  </IconWrapper>
)

export const LargeIcon = styled.div`
  height: 8rem;
  width: 8rem;
`
