// eslint-disable-next-line no-undef
module.exports = {
  // PAGES
  HOME_PAGE: 'home-page',
  CREATE_USER_PAGE: 'create-user-page',
  CREATE_SESSION_PAGE: 'create-session-page',
  PLAY_PAGE: 'play-game-page',
  JOIN_GAME_PAGE: 'join-game-page',
  CREATE_GAME_PAGE: 'create-game-page',

  // LINKS
  CREATE_GAME_LINK: 'create-game-link',
}
