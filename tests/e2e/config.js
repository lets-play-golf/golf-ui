const __id__ = new Date().getTime().toString().slice(-6)

module.exports = {
  verbose: true,
  globalSetup: '<rootDir>/helpers/globalSetup.js',
  globals: {
    __id__,
  },
}
