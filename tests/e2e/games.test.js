const createSession = require('./pages/CreateSession')
const {
  users: { activeUser },
} = require('./data/sample')
const {
  CREATE_SESSION_LINK,
  CREATE_SESSION_PAGE,
  CREATE_GAME_PAGE,
} = require('./helpers/selectors')
const { resetDb } = require('./utilities/dbCommands')
const browserCommands = require('./browser-commands')

describe('Games', () => {
  beforeAll(async () => {
    await resetDb()
    await browserCommands.clearSession()
  })

  describe('Create and Join Game', () => {
    it('should allow a registered user to create a game', async () => {
      await page.goto(GOLF_UI_URL, { waitUntil: 'networkidle0' })
      // TODO: Make puppeteer clear session before executing the next test
      await page
        .click(CREATE_SESSION_LINK)
        .then(() => page.waitForSelector(CREATE_SESSION_PAGE))
      await createSession({
        page,
        email: activeUser.email,
        password: 'secret',
      })
      const loggedInHomePageH1 = await page.$eval('h1', (el) => el.textContent)
      expect(loggedInHomePageH1).toBe('Your Games')

      await page
        .click("button[title='New Game']")
        .then(() => page.waitForSelector(CREATE_GAME_PAGE))

      expect(await page.$eval('h1', (el) => el.textContent)).toBe('New Game')

      await page
        .click("button[title='Create new Game']")
        .then(() => page.waitForSelector())

      expect(await page.$eval('h1', (el) => el.textContent)).toBe('Game Open')
    }, 30000)
  }, 30000)
}, 30000)
