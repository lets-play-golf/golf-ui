const { dti } = require('../helpers/data')

module.exports = async ({ page, email, password, username }) => {
  await page.waitForSelector(`[data-testid="${dti.CREATE_USER_PAGE}"]`)
  await page.type('input[name="firstname"]', 'Alaska Thunderfuck')
  await page.type("input[name='username']", username)
  await page.type("input[name='email']", email)
  await page.type("input[name='password']", password)
  await page.type("input[name='password_confirm']", password)
  await page
    .click('form button')
    .then(() => page.waitForSelector(`[data-testid="${dti.HOME_PAGE}"]`))
}
