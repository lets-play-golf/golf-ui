const {
  CREATE_SESSION_PAGE,
  HOME_PAGE,
  EMAIL_INPUT,
  PASSWORD_INPUT,
} = require('../helpers/selectors')

module.exports = async ({ page, email, password }) => {
  await page.waitForSelector(CREATE_SESSION_PAGE)
  await page.type(EMAIL_INPUT, email)
  await page.type(PASSWORD_INPUT, password)
  return page.click('form button').then(() => page.waitForSelector(HOME_PAGE))
}
