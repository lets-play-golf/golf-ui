const puppeteer = require('puppeteer')

const options = {
  headless: false,
  devtools: true,
}

const startBrowser = async () => {
  const browser = await puppeteer.launch(options)
  const page = await browser.newPage()
  return { browser, page }
}

module.exports = {
  startBrowser,
}
