const dti = require('../../../src/dataTestIds')
module.exports = {
  alice: {
    username: `al${process.pid}`,
    email: `alice${process.pid}@gmail.com`,
    firstname: 'Alice',
    password: 'Secret@1',
  },
  bob: {
    username: `bo${process.pid}`,
    firstname: 'Benoit',
    email: `bob${process.pid}@gmail.com`,
    password: 'Secret@1',
  },
  dti,
  launchConfig: {
    headless: false,
    devtools: true,
  },
}
