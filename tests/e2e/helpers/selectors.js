const dataTestIds = require('../../../src/dataTestIds')

module.exports = {
  // links:
  CREATE_SESSION_LINK: 'a[href="/login"]',
  CREATE_USER_LINK: 'a[href*="/register"]',
  DELETE_SESSION_LINK: '#header_logout',

  // pages:
  HOME_PAGE: `[data-testid="${dataTestIds.HOME_PAGE}"]`,
  CREATE_SESSION_PAGE: `[data-testid="${dataTestIds.CREATE_SESSION_PAGE}"]`,
  CREATE_USER_PAGE: `[data-testid="${dataTestIds.CREATE_USER_PAGE}"]`,
  CREATE_GAME_PAGE: `[data-testid="${dataTestIds.CREATE_GAME_PAGE}"]`,

  // inputs
  EMAIL_INPUT: "input[name='email']",
  PASSWORD_INPUT: "input[name='password']",
}
