module.exports = {
  clearSession: async () => {
    // deleteCookie and localStorage.clear may fail if they are not executed on a page
    await page.goto(GOLF_UI_URL)
    return Promise.all([
      page.deleteCookie(),
      page.evaluate(() => {
        localStorage.clear()
      }),
    ])
  },
}
