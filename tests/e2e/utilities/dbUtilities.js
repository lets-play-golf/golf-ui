const { createComponent } = require('whales')

const users = require('../data/users.json')

let mongo = null

async function initMongo() {
  if (!mongo) {
    mongo = createComponent(require('../../src/components/mongo'))
    await mongo.init()
  }
}

async function resetDB() {
  try {
    await initMongo()
    await dropCollections()
    await createCollections()
  } catch (e) {
    console.log('resetDB e', e.stack)
  }
}

async function dropCollections() {
  return Promise.all([removeCollection('users')])
}

async function createCollections() {
  return Promise.all([insertCollection('users', users)])
}

async function mongoQuery({
  collection,
  query,
  limit = 10,
  sort = { _id: 1 },
}) {
  await initMongo()
  return Promise.all([
    mongo.db
      .collection(collection)
      .find(query)
      .limit(limit)
      .sort(sort)
      .toArray(),
  ])
}

async function insertCollection(collectionName, array) {
  return mongo.db.collection(collectionName).insertMany(array)
}

async function removeCollection(collectionName) {
  return mongo.db.collection(collectionName).deleteMany({})
}

async function resetUserActivity() {
  return removeCollection('user-activity')
}

const closeMongo = () => mongo.client.close()

module.exports = { resetDB, resetUserActivity, mongoQuery, closeMongo }
