const { execFile } = require('promisify-child-process')
const {
  MONGO: { test },
} = require('../../../app.env.json')

// DB config
const {
  database,
  hosts: [{ host: dbHost, port: dbPort }],
} = test
const args = [`${dbHost}:${dbPort}`, database]

const executeCommand = async (cmd) => execFile(cmd, args)

// Commands
const resetDb = async () => executeCommand(`${__dirname}/resetDB.sh`)

const resetUserActivity = async () =>
  executeCommand(`${__dirname}/resetUserActivity.sh`)

module.exports = {
  resetDb,
  resetUserActivity,
}
