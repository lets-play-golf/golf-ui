const users = require('./users.json')

module.exports = {
  users: {
    activeUser: users[0],
    inactiveUser: users[1],
  },
}
