const createUser = require('./pages/CreateUser')
const createSession = require('./pages/CreateSession')
const { alice } = require('./helpers/data')
const {
  CREATE_SESSION_LINK,
  CREATE_SESSION_PAGE,
  CREATE_USER_LINK,
  CREATE_USER_PAGE,
  HOME_PAGE,
  DELETE_SESSION_LINK,
} = require('./helpers/selectors')
const browserCommands = require('./browser-commands')
const { email, password, username } = alice

const home = require('./pages_new/home')

describe('Create user', () => {
  beforeEach(async () => browserCommands.clearSession())

  it('should allow a new user to register and log in', async () => {
    await page.goto(GOLF_UI_URL, { waitUntil: 'networkidle0' })
    await page
      .click(CREATE_SESSION_LINK)
      .then(() => page.waitForSelector(CREATE_SESSION_PAGE))
    await page
      .click(CREATE_USER_LINK)
      .then(() => page.waitForSelector(CREATE_USER_PAGE))
    await createUser({ page, email, password, username })
    await page
      .click(DELETE_SESSION_LINK)
      .then(() => page.waitForSelector(HOME_PAGE))
    await page
      .click(CREATE_SESSION_LINK)
      .then(() => page.waitForSelector(CREATE_SESSION_PAGE))
    await createSession({ page, email, password }).then(() =>
      page.waitForSelector('h1')
    )
    const loggedInHomePageH1 = await page.$eval('h1', (el) => el.textContent)
    expect(loggedInHomePageH1).toBe('Your Games')
  }, 30000)

  it('do shit', async () => {
    await home.load()
    await home.header.navigateTo.login()
    expect(true).toBe(true)
  }, 30000)
}, 30000)
