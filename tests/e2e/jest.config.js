module.exports = {
  preset: 'jest-puppeteer',
  globals: {
    GOLF_UI_URL: 'http://localhost:4000',
  },
  testMatch: ['**/tests/**/*.test.js'],
}
