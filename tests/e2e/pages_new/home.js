const header = require('./header')

module.exports = {
  header,
  load: async () => page.goto(GOLF_UI_URL, { waitUntil: 'networkidle0' }),
}
