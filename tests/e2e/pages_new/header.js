const selectors = {
  loginNav: 'a[href="/login"]',
}
module.exports = {
  navigateTo: {
    login: async () =>
      page
        .click(selectors.loginNav)
        .then(() =>
          page.waitForSelector('[data-testid="create-session-page"]')
        ),
  },
}
