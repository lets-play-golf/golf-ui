#!/bin/bash
api_port=4001
ui_port=4000

if ! nc -z localhost $api_port; then
  echo "Gateway is not started, run 'npm run start:test' in api project"
  exit 1
fi

if ! nc -z localhost $ui_port; then
  echo "UI is not started, run 'npm start' in ui project"
  exit 1
fi

#bash -c 'while [[ "$(curl --write-out ''%{http_code}'' localhost:4000)" != "200" ]]; do sleep 5; done' || false
yarn e2e:test
TEST_RESULT=$?
echo "Tests finished with exit code ${TEST_RESULT}, killing PID ${ui_PID} newPid ${test_PID}"

